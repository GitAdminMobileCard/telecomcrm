var app = angular.module('MyApp', ['ngRoute','ngCookies', 'ngMaterial', 'datatables',
    'ui.bootstrap', 'datatables.buttons', 'ngFileSaver', 'ngResource', 'ngSanitize']);


app.config(function($routeProvider, $httpProvider, $compileProvider) {

    $routeProvider.when('/', {
        templateUrl : '/TelecomCRM/admin/home/'
    }).when('/clientes/:action', {
        templateUrl : '/TelecomCRM/admin/cliente/'           
    }).when('/usuarios', {
        templateUrl : '/TelecomCRM/admin/usuario/'           
    }).when('/conciliacion', {
        templateUrl : '/TelecomCRM/admin/conciliacion/'           
    }).when('/transacciones', {
        templateUrl : '/TelecomCRM/admin/transaccion/'           
    }).when('/reversos', {
        templateUrl : '/TelecomCRM/admin/reverso/'           
    }).when('/bloqueos', {
        templateUrl : '/TelecomCRM/admin/bloqueo/'           
    }).when('/tags', {
        templateUrl : '/TelecomCRM/admin/tag/'           
    }).when('/proveedores', {
        templateUrl : '/TelecomCRM/admin/provider/'           
    }).when('/connectionLost', {
        templateUrl : 'connectionLost'
    }).otherwise('/');

    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
        
});

app.filter('lockStatusFormat', function() {
    return function(x) {
        var txt = "";
        if (x == 0){
            txt = "Activo";
        }else if (x == 1) {
            txt = "Bloqueado";
        }
        return txt;
    };
});

app.filter('to_trusted', ['$sce', function($sce){
    return function(text) {
        return $sce.trustAsHtml(text);
    };
}]);

app.directive('compileHtml', compileHtml);

function compileHtml($compile) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            scope.$watch(function () {
                return scope.$eval(attrs.compileHtml);
            }, function (value) {
                element.html(value);
                $compile(element.contents())(scope);
            });
        }
    };
}

