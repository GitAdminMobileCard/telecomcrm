/* global angular */

app.controller('UserController', ['$rootScope', '$scope', '$location', 
    '$window', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'UserService', 'RoleService',
    function($rootScope, $scope, $location, $window, DTOptionsBuilder, DTColumnDefBuilder, UserService, RoleService) {            
    
    var self = this; 
    self.users = [];
    self.roles = [];
    self.wait = false;
    self.detailUser = false;
    self.editingUser = false; 
    self.creatingUser = false; 
    self.editingRolesUser = false;

    self.user = {
        id: "",    
        version: "",
        account_expired: "",
        account_locked: "",
        enabled: "",
        password_expired: "",
        password: "",
        username: "",
        roles: []
    };
    
    self.reset = function (){     
        self.user = {
            id: "",    
            version: "",
            account_expired: "",
            account_locked: "",
            enabled: "",
            password_expired: "",
            password: "",
            username: "",
            roles: []
        };
    };
    
    self.getUserList = function(){
        self.wait = true;
        UserService.getUserList()        
        .then(
            function(d) {                
                self.users = d.data;
                self.wait = false;                
            },
            function(errResponse){
                console.error('Error while fetching users For List');
                self.showMessageError('Error while fetching users',errResponse);
            }
        );
    };
    
    self.getRoleList = function(){
        self.wait = true;
        RoleService.getRoleList()        
        .then(
            function(d) {                
                self.roles = d.data;
                self.wait = false;                
            },
            function(errResponse){
                console.error('Error while fetching roles For List');
                //self.showMessageError('Error while fetching roles',errResponse);
            }
        );
    };
    
    self.editUser = function () {
        self.wait = true;        
        UserService.editUser(self.user)        
        .then(
            function(result) {                
                console.log("User updated successful",result);
                self.users = result.data;
                self.wait = false;
                self.editingUser = false;               
            },
            function(errResponse){
                console.error('Error while fetching user');
                //self.showMessageError('Error while fetching User',errResponse);
                self.wait = false;
                self.editingUser = false;
            }
        );
    };
    
    self.addUser = function () {
        self.wait = true;        
        UserService.addUser(self.user)        
        .then(
            function(result) {                
                console.log("User added successful",result);
                self.users = result.data;
                self.wait = false;
                self.creatingUser = false;
            },
            function(errResponse){
                console.error('Error while add user');
//                self.showMessageError('Error while fetching User',errResponse);
                self.wait = false;
                self.creatingUser = false;
            }
        );
    };           
    
    self.getUserList();
    self.getRoleList();
    
    self.dtOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('full_numbers')
            .withDisplayLength(10)            
            .withOption('order', [0, 'desc'])
            .withOption('stateSave', true);
            
    self.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5),
        DTColumnDefBuilder.newColumnDef(6).notSortable()
    ];
    
    self.dtInstance = {};  
    
    self.initUserModalData = function (user, type) {
        switch (type){
            case "detail":
                self.user = angular.copy(user);
                self.detailUser = true; 
                self.editingUser = false; 
                self.creatingUser = false; 
                self.editingRolesUser = false;
                break;
            case "edit":
                self.user = angular.copy(user);
                self.detailUser = false;
                self.editingUser = true; 
                self.creatingUser = false; 
                self.editingRolesUser = false;                    
                break;            
            case "create":                    
                self.detailUser = false;
                self.editingUser = false; 
                self.creatingUser = true; 
                self.editingRolesUser = false;                                        
                break;
        };        
    };    

    self.showRole = function (role){
        for (var i = 0; i < self.user.roles.length; i++){
            if (self.user.roles[i].id_role === role.id_role){
                return false;
            }
        }
        return true;
    };
    
    self.addUserRole = function (role){
        if (!self.detailUser){
            self.user.roles.push(role);
        }        
    };
    
    self.removeUserRole = function (role){
        if (!self.detailUser){
            for (var i = 0; i < self.user.roles.length; i++){
                if (self.user.roles[i].id_role === role.id_role){
                    self.user.roles.splice(i,1);
                    break;
                }
            }
        }           
    };
    
}]);