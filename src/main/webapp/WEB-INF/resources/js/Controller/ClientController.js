/* global angular */

app.controller('ClientController', ['$rootScope', '$scope', '$routeParams', 'ClientService', 'CountryService',
    'ProviderService', 'BankService', 'CardService', 'ClientStatusService', 'FranchiseService','$location', 
    '$window', 'DTOptionsBuilder', 'DTColumnDefBuilder', 
    function($rootScope, $scope, $routeParams, ClientService, CountryService, ProviderService, BankService, CardService,
        ClientStatusService, FranchiseService, $location, $window, DTOptionsBuilder, DTColumnDefBuilder) {            
    
    var self = this;  
    self.totalClients = "";
    self.clientsToShow = 500;
    self.lastPageLoaded = 0;
    self.pageDT = 0;
    self.clients = [];
    self.providers = [];
    self.banks = [];
    self.typeCards = [];
    self.clientStatus = [];
    self.detailClient = false;
    self.editingClient = false; 
    self.creatingClient = false; 
    self.editingStatusClient = false; 
    self.finishedSearch = false;        
    self.search = true;
    self.registrationDateIsOpen = false;
    self.bornDateIsOpen = false;
    self.action = $routeParams.action;
    self.showWallet = false;
    
    self.dateFormat = 'yyyy-MM-dd';        
    
    self.initialDate = {
        opened: false
    };
    
    self.finalDate = {
        opened: false
    };
    
    self.registrationDate = {
        opened: false
    };
    
    self.bornDate = {
        opened: false
    };              
    
    self.client = {
        cedula: "",
        email: "",
        gemalto: 0,
        id_banco: "",
        id_ingo: "",
        id_proveedor: 0,
        id_tipo_tarjeta: 0,
        id_usr_status: 0,
        id_usuario: "",
        idpais: "",    
        imei: "",
        modelo: "",
        num_ext_STR: "",
        operador: "",
        recibirSMS: "",
        software: "",
        telefono_original: "",
        tipo: "",
        tipocedula: 0,
        usr_Colonia: "",
        usr_apellido: "",
        usr_calle: "",
        usr_ciudad: "",
        usr_cp: "",
        usr_direccion: "",
        usr_dom_amex: "",
        usr_fecha_nac: "",
        usr_fecha_registro: "",
        usr_id_estado: "",
        usr_login: "",
        usr_materno: "",
        usr_nombre: "",
        usr_nss: "",
        usr_num_ext: "",
        usr_num_interior: "",
        usr_pwd: "",
        usr_sexo: "2",
        usr_tdc_numero: "",
        usr_tdc_vigencia: "",
        usr_tel_casa: "",
        usr_tel_oficina: "",
        usr_telefono: "",
        usr_terminos: "",
        wkey: ""
    };
    
    self.pwd_aux = "";
    
    self.clientCard = {                    
        idtarjetasusuario: "", 
        idusuario: "", 
        numerotarjeta: "", 
        vigencia: "", 
        estado: "", 
        idbanco: "",
        idfranquicia: "",
        idtarjetas_tipo: "", 
        fecharegistro: "",
        ct: "",
        nombre_tarjeta: "",
        usrDomAmex: "",
        usrCpAmex: "",
        mobilecard: ""
    };
    
    self.addingClientCard = false;
    
    self.reset = function () {
        self.client = {
            cedula: "",
            email: "",
            gemalto: 0,
            id_banco: "",
            id_ingo: "",
            id_proveedor: 0,
            id_tipo_tarjeta: 0,
            id_usr_status: 0,
            id_usuario: "",
            idpais: "",    
            imei: "",
            modelo: "",
            num_ext_STR: "",
            operador: "",
            recibirSMS: "",
            software: "",
            telefono_original: "",
            tipo: "",
            tipocedula: 0,
            usr_Colonia: "",
            usr_apellido: "",
            usr_calle: "",
            usr_ciudad: "",
            usr_cp: "",
            usr_direccion: "",
            usr_dom_amex: "",
            usr_fecha_nac: "",
            usr_fecha_registro: "",
            usr_id_estado: "",
            usr_login: "",
            usr_materno: "",
            usr_nombre: "",
            usr_nss: "",
            usr_num_ext: "",
            usr_num_interior: "",
            usr_pwd: "",
            usr_sexo: "2",
            usr_tdc_numero: "",
            usr_tdc_vigencia: "",
            usr_tel_casa: "",
            usr_tel_oficina: "",
            usr_telefono: "",
            usr_terminos: "",
            wkey: ""
        };
        
        self.pwd_aux = "";
        
        self.clientSearch = {
            id_usuario: "",
            usr_login: "",
            usr_fecha_registro_inicial: "",
            usr_fecha_registro_final: "",
            usr_nombre: "",
            usr_telefono: "",
            usr_apellido: ""        
        };
         
        self.clientCard = {                    
            idtarjetasusuario: "", 
            idusuario: "", 
            numerotarjeta: "", 
            vigencia: "", 
            estado: "", 
            idbanco: "",
            idfranquicia: "",
            idtarjetas_tipo: "", 
            fecharegistro: "",
            ct: "",
            nombre_tarjeta: "",
            usrDomAmex: "",
            usrCpAmex: "",
            mobilecard: ""
        };
    };
          
    self.validateDataClient = function () {
        if (self.client.usr_login === "" || self.client.email === ""
            || self.client.usr_pwd === "" || self.client.id_usr_status === ""
            || self.client.usr_nombre === "" || self.client.usr_apellido === ""
            || self.client.usr_telefono === "" || self.client.usr_direccion === ""
            || self.client.idpais === "" || self.client.usr_fecha_nac === ""
            || self.client.id_tipo_tarjeta === "" || self.client.usr_tdc_numero === ""
            || self.client.usr_tdc_vigencia === "" || self.client.usr_tdc_cvv === ""
            || self.client.usr_tdc_tarjeta_habiente === ""){                                                                        
            
            return true;
        }else {
            var date = new Date();            
            date.setFullYear(date.getFullYear() - 18);            
            if (self.client.usr_fecha_nac > date){
                return true;
            }else if (self.client.usr_pwd !== self.pwd_aux && self.action === "register"){
                return true;
            }else{
                return false;
            }
        }
    };
        
    self.getClientList = function(){
        self.wait = true;
        ClientService.getClientList()        
        .then(
            function(d) {                
                self.clients = d.data;
                self.wait = false;                
            },
            function(errResponse){
                console.error('Error while fetching clients For List');
                self.showMessageError('Error while fetching clients',errResponse);
            }
        );
    };

    self.getClientList = function(){
        self.wait = true;
        ClientService.getClientList()        
        .then(
            function(d) {                
                self.clients = d.data;
                self.wait = false;                
            },
            function(errResponse){
                console.error('Error while fetching clients For List');
                self.showMessageError('Error while fetching clients',errResponse);
            }
        );
    };
    
    self.clientSearch = {
        id_usuario: "",
        usr_login: "",
        usr_fecha_registro_inicial: "",
        usr_fecha_registro_final: "",
        usr_nombre: "",
        usr_telefono: "",
        usr_apellido: ""        
    };
    
    self.searchClientByParameters = function(){
        self.wait = true;
        self.finishedSearch = false;
        self.search = false;
        if (self.clientSearch.id_usuario === ""){
            self.clientSearch.id_usuario = -1;
        }        
        ClientService.searchClients(self.clientSearch)        
        .then(
            function(d) {
                console.log(d);
                self.clients = d.data;
                for (var i = 0; i < self.clients.length; i++){
                    var registrationDate = self.clients[i].usr_fecha_registro;
                    var bornDate = self.clients[i].usr_fecha_nac;
                    self.clients[i].usr_fecha_registro = new Date(registrationDate);
                    self.clients[i].usr_fecha_registro.setMinutes(self.clients[i].usr_fecha_registro.getMinutes() + self.clients[i].usr_fecha_registro.getTimezoneOffset());
                    self.clients[i].usr_fecha_nac = new Date(bornDate);
                    self.clients[i].usr_fecha_nac.setMinutes(self.clients[i].usr_fecha_nac.getMinutes() + self.clients[i].usr_fecha_nac.getTimezoneOffset());                    
                }
                self.finishedSearch = true;
                self.search = false;
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching clients For List');
                self.showMessageError('Error while fetching clients',errResponse);
            }
        );
    };
    
    self.validateClientSearch = function () {
        if (self.clientSearch.usr_fecha_registro_inicial === "" && self.clientSearch.usr_fecha_registro_final !== ""){
            return true;
        }else if (self.clientSearch.usr_fecha_registro_inicial !== "" && self.clientSearch.usr_fecha_registro_final === ""){
            return true;
        }
        if (self.clientSearch.id_usuario !== ""){
            return false;
        }else if (self.clientSearch.usr_login !== ""){
            return false;
        }else if (self.clientSearch.usr_nombre !== ""){
            return false;
        }else if (self.clientSearch.usr_apellido !== ""){
            return false;
        }else if (self.clientSearch.usr_telefono !== ""){
            return false;
        }else if (self.clientSearch.usr_fecha_registro_inicial !== "" && self.clientSearch.usr_fecha_registro_final !== ""){
            return false;
        }
        
        return true;
    };
       
    self.getNumberOfClients = function(){
        self.wait = true;
        ClientService.getNumberOfClients()        
        .then(
            function(numberOfClients) {                
                self.totalClients = numberOfClients.data;
                if (self.totalClients <= 500){
                    self.getClientList();
                }else {                    
                    self.getClientsByPage(self.lastPageLoaded, self.clientsToShow);
                }                
            },
            function(errResponse){
                console.error('Error while fetching clients For List');
                self.showMessageError('Error while fetching clients',errResponse);
            }
        );
    };
    
    self.editClient = function () {
        self.wait = true;
        var registrationDate = self.client.usr_fecha_registro;
        var bornDate = self.client.usr_fecha_nac;
        self.client.usr_fecha_registro = new Date(registrationDate);
        self.client.usr_fecha_registro.setMinutes(self.client.usr_fecha_registro.getMinutes() + self.client.usr_fecha_registro.getTimezoneOffset());
        self.client.usr_fecha_nac = new Date(bornDate);
        self.client.usr_fecha_nac.setMinutes(self.client.usr_fecha_nac.getMinutes() + self.client.usr_fecha_nac.getTimezoneOffset());
        
        
            
        self.clientCard.idusuario = self.client.id_usuario;
        self.clientCard.numerotarjeta = self.client.usr_tdc_numero;
        self.clientCard.vigencia = self.client.usr_tdc_vigencia;
        self.clientCard.idtarjetas_tipo = self.client.id_tipo_tarjeta;
        self.clientCard.fecharegistro = new Date();
        
        ClientService.editClient(self.client)        
        .then(
            function(result) {                
                console.log("Client updated successful",result);
                var clientEdited = result.data;
                registrationDate = clientEdited.usr_fecha_registro;
                bornDate = clientEdited.usr_fecha_nac;
                clientEdited.usr_fecha_registro = new Date(registrationDate);
                clientEdited.usr_fecha_registro.setMinutes(clientEdited.usr_fecha_registro.getMinutes() + clientEdited.usr_fecha_registro.getTimezoneOffset());
                clientEdited.usr_fecha_nac = new Date(bornDate);
                clientEdited.usr_fecha_nac.setMinutes(clientEdited.usr_fecha_nac.getMinutes() + clientEdited.usr_fecha_nac.getTimezoneOffset());                
                for (var i  = 0; i < self.clients.length; i++){                    
                    if (self.clients[i].id_usuario === clientEdited.id_usuario){
                        self.clients[i] = angular.copy(clientEdited);                                                
                    }
                }
                self.wait = false;
                //self.addOrUpdateClientCard(self.clientCard);
                self.editingClient = false;
                self.detailClient = true;
            },
            function(errResponse){
                console.error('Error while fetching client');
//                self.showMessageError('Error while fetching Client',errResponse);
                self.wait = false;
                self.editingClient = false;
                self.detailClient = true;
            }
        );
    };
    
    self.addClient = function () {
        self.wait = true;
        var registrationDate = self.client.usr_fecha_registro;
        var bornDate = self.client.usr_fecha_nac;
        self.client.usr_fecha_registro = new Date(registrationDate);
        self.client.usr_fecha_registro.setMinutes(self.client.usr_fecha_registro.getMinutes() + self.client.usr_fecha_registro.getTimezoneOffset());
        self.client.usr_fecha_nac = new Date(bornDate);
        self.client.usr_fecha_nac.setMinutes(self.client.usr_fecha_nac.getMinutes() + self.client.usr_fecha_nac.getTimezoneOffset());
        
        self.clientCard.idusuario = self.client.id_usuario;
        self.clientCard.numerotarjeta = self.client.usr_tdc_numero;
        self.clientCard.vigencia = self.client.usr_tdc_vigencia;
        self.clientCard.idtarjetas_tipo = self.client.id_tipo_tarjeta;
        self.clientCard.fecharegistro = new Date();       
        
        ClientService.addClient(self.client)        
        .then(
            function(result) {                
                console.log("Client added successful",result);                
                self.wait = false;
                //self.addOrUpdateClientCard(self.clientCard);
                self.creatingClient = false;
                self.detailClient = true;
            },
            function(errResponse){
                console.error('Error while add client');
//                self.showMessageError('Error while fetching Client',errResponse);
                self.wait = false;
                self.creatingClient = false;
                self.detailClient = true;
            }
        );
    };            
    
    self.initSearch = function () {
        self.finishedSearch = false;        
        self.search = true;
        self.clients = [];
        self.reset();
        $scope.$apply();
    };
    
//    self.getNumberOfClients();    
    
    self.dtOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('full_numbers')
            .withDisplayLength(10)
            .withDOM('lBfrtip')
            .withOption('order', [0, 'desc'])
            .withOption('stateSave', true)
            .withButtons([                                
                {
                    text: 'Nueva Busqueda',
                    key: '3',
                    action: function (e, dt, node, config) {
                        self.initSearch();
                    }
                }
            ]);
            
    self.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5),
        DTColumnDefBuilder.newColumnDef(6),
        DTColumnDefBuilder.newColumnDef(7),
        DTColumnDefBuilder.newColumnDef(8).notSortable()
    ];
    
    self.dtInstance = {};    

    self.showPageInfo = function () {
        console.log(self.dtInstance.DataTable.page.info());
    };
    
    self.getCountryList = function(){
        self.wait = true;
        CountryService.getCountryList()        
        .then(
            function(d) {                
                self.countries = self.formatCountries(d.data);
                self.getProviderList();                
            },
            function(errResponse){
                console.error('Error while fetching countries List');
                //self.showMessageError('Error while fetching countries',errResponse);
            }
        );
    };
    
    self.getCountryList();
    
    self.formatCountries = function (data){
        var countries = {};
        for (var i = 0; i < data.length; i++){
            countries[data[i].idpaises] = data[i].nombre;
        }
        return countries;
    };
    
    self.getProviderList = function(){        
        ProviderService.getProviderList()        
        .then(
            function(d) {                
                self.providers = d.data;
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching providers For List');
                //self.showMessageError('Error while fetching providers',errResponse);
            }
        );
    };
    
    self.getBankList = function(){
        self.wait = true;
        BankService.getBankList()        
        .then(
            function(d) {                
                self.banks = d.data;
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching banks For List');
                //self.showMessageError('Error while fetching banks',errResponse);
            }
        );
    };
    
    self.getFranchiseList = function(){
        self.wait = true;
        FranchiseService.getFranchiseList()        
        .then(
            function(d) {                
                self.franchises = d.data;
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching franchises For List');
                //self.showMessageError('Error while fetching franchises',errResponse);
            }
        );
    };
    
    self.getTypeCardList = function(){
        self.wait = true;
        CardService.getTypeCardList()        
        .then(
            function(d) {                
                self.typeCards = d.data;
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching typeCards For List');
                //self.showMessageError('Error while fetching typeCards',errResponse);
            }
        );
    };
    
    self.getClientStatusList = function(){
        self.wait = true;
        ClientStatusService.getClientStatusList()        
        .then(
            function(d) {                
                self.clientStatus = d.data;
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching clientStatus For List');
                //self.showMessageError('Error while fetching clientStatus',errResponse);
            }
        );
    };
    
    self.getClientCards = function(id_usuario){
        self.wait = true;
        ClientService.getClientCards(id_usuario)        
        .then(
            function(d) {                
                self.clientCards = d.data;
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching clientStatus For List');
                //self.showMessageError('Error while fetching clientStatus',errResponse);
            }
        );
    };
    
    self.addOrUpdateClientCard = function(){
        self.wait = true;
        ClientService.addOrUpdateClientCard(self.clientCard)        
        .then(
            function(d) {                
                self.clientCards = d.data;
                self.resetClientCard();
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching clientCards For List');
                //self.showMessageError('Error while fetching clientCards',errResponse);
            }
        );
    };
    
    self.initClientModalData = function (client, type) {        
        switch (type){
            case "detail":
                self.client = client;
                if (client.usr_tdc_numero === ""){
                    self.showWallet = false;
                }else{
                    self.getClientCards(self.client.id_usuario);                
                    self.showWallet = true;
                }                
                self.detailClient = true; 
                self.editingClient = false; 
                self.creatingClient = false;                                 
                break;
            case "edit":
                self.client = client;
                self.client = client;
                if (client.usr_tdc_numero === ""){
                    self.showWallet = false;
                }else{
                    self.getClientCards(self.client.id_usuario);                
                    self.showWallet = true;
                }                
                self.detailClient = false;
                self.editingClient = true; 
                self.creatingClient = false; 
                self.editingStatusClient = false;                    
                break;
            case "status":
                self.client = client;
                self.detailClient = false;
                self.editingClient = false; 
                self.creatingClient = false; 
                self.editingStatusClient = true;                    
                break;
            case "create":                    
                self.detailClient = false;
                self.showWallet = false;
                self.editingClient = false; 
                self.creatingClient = true; 
                self.editingStatusClient = false;                                        
                break;
        };        
    };    

    self.openInitialDate = function() {
        self.initialDate.opened  = true;
    };
    
    self.openFinalDate = function() {
        self.finalDate.opened  = true;
    };
    
    self.openRegistrationDate = function() {
        self.registrationDate.opened  = true;
    };
    
    self.openBornDate = function() {
        self.bornDate.opened  = true;
    };
    
    self.getBankList();
    self.getFranchiseList();
    self.getTypeCardList();
    self.getClientStatusList();   


    if (self.action === "register"){
        self.search = false;
        self.reset();
        self.initClientModalData(self.client,'create');
    }
    
    self.getClientCardType = function (id_card_type){
        for (var i = 0; i < self.typeCards.length; i++){
            if (self.typeCards[i].id_tipo_tarjeta === id_card_type){
                return self.typeCards[i].desc_tipo_tarjeta;
            }
        }
        return "";
    };
    
    self.initClientCardData = function (card){
        
        self.clientCard = angular.copy(card);
        
        self.addingClientCard = true;
    };
    
    self.validateDataClientCard = function (){
        if (self.clientCard.idusuario === "" || 
            self.clientCard.numerotarjeta === "" || 
            self.clientCard.vigencia === "" || 
            self.clientCard.idtarjetas_tipo === "" || 
            //self.clientCard.fecharegistro === "" ||
            self.clientCard.ct === "" ||
            self.clientCard.idbanco === "" ||
            self.clientCard.idfranquicia === ""){
            return true;
        }else{
            return false;
        }
    };
    
    self.resetClientCard = function (){
        self.clientCard = {                    
            idtarjetasusuario: "", 
            idusuario: "", 
            numerotarjeta: "", 
            vigencia: "", 
            estado: "", 
            idbanco: "",
            idfranquicia: "",
            idtarjetas_tipo: "", 
            fecharegistro: "",
            ct: "",
            nombre_tarjeta: "",
            usrDomAmex: "",
            usrCpAmex: "",
            mobilecard: ""
        };

        self.addingClientCard = false;
    };
    
    self.initAddClientCard = function () {
        self.clientCard.idusuario = self.client.id_usuario;        
        self.clientCard.fecharegistro = new Date();
        self.addingClientCard = true;
    };
    
    self.dtWalletOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('full_numbers')
            .withDisplayLength(5)
            .withDOM('lfrtip')
            .withOption('order', [0, 'desc'])
            .withOption('stateSave', true);
            
    self.dtWalletColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5).notSortable()
    ];
    
    self.dtWalletInstance = {};
        
}]);