app.controller('LoginController', ['$scope', '$rootScope', 'LoginService', '$http', '$location', 
    function($scope, $rootScope, LoginService, $http, $location) {            
    
    var self = this;            
    
    self.userDelay = new Date().getTimezoneOffset();
    
    console.log("Delay", self.userDelay);
    
    var authenticate = function(credentials, callback) {
        
        var headers = credentials ? {authorization : "Basic "
            + btoa(credentials.login + ":" + credentials.password + ":" + credentials.delay)
        } : {};

        $http.get('user/', {headers : headers}).success(function(data) {
            console.log("data: ", data);
            if (data.name) {
              $rootScope.authenticated = true;              
            } else {
              $rootScope.authenticated = false;
            }
            callback && callback();
        }).error(function() {
            $rootScope.authenticated = false;
            callback && callback();
        });
    };
    
    
    self.credentials = {};
    self.login = function() {
        authenticate(self.credentials, function() {
            if ($rootScope.authenticated) {
                $location.path("/home");
                self.error = false;
            } else {
                $location.path("/");
                self.error = true;
            }
        });
    };
    
    
    self.user = {
        login: '',
        password: ''
    };        
    
    self.error = {
        title: "",
        message: ""
    };
    
    self.loginUser = function(user){
        LoginService.loginUser(user)
        .then(
            function(d) {
                 self.user = d;                
                 $rootScope.authenticated = true;
            },
             function(errResponse){
                 console.error('Error while fetching user');
                 self.showMessageError('Error while login',errResponse);
             }
        );
    };
    
    self.logoutUser = function(){
        LoginService.logoutUser()
        .then(
            function(d) {
                 self.user.login = "";
                 self.user.password = "";                
                 $rootScope.authenticated = false;                 
            },
             function(errResponse){
                 console.error('Error while fetching user');
                 self.showMessageError('Error while logout',errResponse);
             }
        );
    };
    
    self.showMessageError = function (title, message){
        self.error.title = title;
        self.error.message = message;
        document.getElementById('errorMessage').style.display='block';
    };
    
    self.closeMessageError = function (){
        self.error.title = "";
        self.error.message = "";
        document.getElementById('errorMessage').style.display='none';
        if (self.wait){
            self.wait = false;
        }
    };
    
}]);

