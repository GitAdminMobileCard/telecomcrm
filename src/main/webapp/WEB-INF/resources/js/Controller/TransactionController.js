/* global angular */

app.controller('TransactionController', ['$rootScope', '$scope', '$filter', 'TransactionService', 'ClientService', 
    'ProviderService', 'ProductService', '$location', '$window', 'DTOptionsBuilder', 'DTColumnDefBuilder', 
    function($rootScope, $scope, $filter, TransactionService, ClientService, ProviderService, ProductService, 
            $location, $window, DTOptionsBuilder, DTColumnDefBuilder) {            
    
    var self = this;  
    self.transactions = [];
    self.transactionsToShow = 500;
    self.lastPageLoaded = 0;
    self.pageDT = 0;
    self.wait = false;
    self.search = true;
    self.select = "";
    self.availableOptions = [];
    self.products = [];
    self.blockingClient = false;
    
    self.initSearch = function () {
        self.finishedSearch = false;        
        self.search = true;
        self.transactions = [];
        self.transactionSearch = {
            id_usuario: "",
            usr_login: "",
            usr_fecha_registro_inicial: "",
            usr_fecha_registro_final: "",
            bit_no_autorizacion: "",
            status: ""
        };
        self.reset();
        self.getProviderList();
        $scope.$apply();
    };
    
    self.initialDate = {
        opened: false
    };
    
    self.finalDate = {
        opened: false
    };
    
    self.transactionSearch = {
        id_usuario: "",
        usr_login: "",
        usr_fecha_registro_inicial: "",
        usr_fecha_registro_final: "",
        bit_no_autorizacion: "",
        status: ""
    };
    
    
    self.openInitialDate = function() {
        self.initialDate.opened  = true;
    };
    
    self.openFinalDate = function() {
        self.finalDate.opened  = true;
    };
    
    self.reset = function () {
        self.transactionSearch = {
            id_usuario: "",
            usr_login: "",
            usr_fecha_registro_inicial: "",
            usr_fecha_registro_final: "",
            bit_no_autorizacion: "",
            status: ""
        };
    };
    

    
    self.getAvailableOptions = function(){
        self.wait = true;
        TransactionService.getAvailableOptions()  
        .then(
            function(d) {
              self.wait =  true;
              self.availableOptions = [
                {id: '1', name: 'Option A'},
                {id: '2', name: 'Option B'},
                {id: '3', name: 'Option C'}
              ];
              console.log(d);
            },
            function(errResponse){
                console.error('Error while fetching products For List');
//                self.showMessageError('Error while fetching transactions',errResponse);
            }
        );
    }; 
            
    self.getTransactionList = function(){
        self.wait = true;
        TransactionService.getTransactionList()        
        .then(
            function(d) {                
                self.transactions = d.data;
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching transactions For List');
                self.showMessageError('Error while fetching transactions',errResponse);
            }
        );
    }; 
    
    self.getProductList = function(){
        self.wait = true;
        ProductService.getProductList()        
        .then(
            function(d) {                
                self.products = d.data;
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching products For List');
                self.showMessageError('Error while fetching products',errResponse);
            }
        );
    };
    
    self.getTransactionsByPage = function(numberPage, maxResults){
        self.wait = true;
        TransactionService.getTransactionsByPage(numberPage, maxResults)        
        .then(
            function(d) {                
                self.transactions = self.transactions.concat(d.data);
                self.lastPageLoaded = self.transactions.length;
                self.wait = false;
                self.pageDT = 0;
            },
            function(errResponse){
                console.error('Error while fetching transactions For List');
                self.showMessageError('Error while fetching transactions',errResponse);
            }
        );
    };
       
    self.getNumberOfTransactions = function(){
        self.wait = true;
        TransactionService.getNumberOfTransactions()        
        .then(
            function(numberOfTransactions) {                
                self.totalTransactions = numberOfTransactions.data;
                if (self.totalTransactions <= 500){
                    self.getTransactionList();
                }else {                    
                    self.getTransactionsByPage(self.lastPageLoaded, self.transactionsToShow);
                }                
            },
            function(errResponse){
                console.error('Error while fetching transactions For List');
                self.showMessageError('Error while fetching transactions',errResponse);
            }
        );
    };
    
    self.reset = function(){                                                            
        
    };           
    
//    self.getNumberOfTransactions();
//    self.getAvailableOptions();
    
    self.getProductList();    
    
    self.dtOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('full_numbers')
            .withDisplayLength(10)
            .withDOM('lBfrtip')
            .withOption('order', [1, 'desc'])
            .withOption('stateSave', true)
            .withButtons([                                
                        {
                            text: 'Nueva Busqueda',
                            key: '3',
                            action: function (e, dt, node, config) {
                                self.initSearch();
                            }
                        }
                    ]);            
            
    self.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5)
    ];
    
    self.dtInstance = {};            
    
    self.searchTransactionByParameters = function(){
        self.wait = true;
        self.finishedSearch = false;
        self.search = false;
        if (self.transactionSearch.id_usuario === ""){
            self.transactionSearch.id_usuario = -1;
        }        
        TransactionService.searchTransactions(self.transactionSearch)        
        .then(
            function(d) {
                //console.log(d);
                self.transactions = d.data;
                //console.log(self.transactions)
                for (var i = 0; i < self.transactions.length; i++){
                    var bit_fecha = self.transactions[i].bit_fecha;
                    var bit_hora = self.transactions[i].bit_hora;
                    var status = self.transactions[i].bit_status;
                    self.transactions[i].statusMessages = self.getTransactionStatus(self.transactions[i]);
                    //self.transactions[i].bit_status = status == 1 ? 'Aprobada' : 'Fallida';
                    self.transactions[i].bit_fecha = new Date(bit_fecha);
                    //console.log("destino",self.transactions[i].destino);
                    if (self.transactions[i].destino != undefined){                                            
                        if (self.transactions[i].destino.toLowerCase().search("referencia") !== -1){
                            //console.log("Split", self.transactions[i].destino.toLowerCase().split(" "));
                            var destino_split = self.transactions[i].destino.toLowerCase().split(" ");
                            self.transactions[i].referencia = destino_split[1].replace(",", "");
                            self.transactions[i].importe = destino_split[3].split(",")[0];
                        } else if (self.transactions[i].destino.toLowerCase().search("lineacap") !== -1) {                        
                            //console.log("Split", self.transactions[i].destino.toLowerCase().split(" "));
                            var destino_split = self.transactions[i].destino.toLowerCase().split(" ");
                            self.transactions[i].referencia = destino_split[1].replace(",", "");
                            self.transactions[i].importe = destino_split[3].split(",")[0];
                        }else {
                            self.transactions[i].referencia = self.transactions[i].destino;                        
                        }
                        /*
                        self.transactions[i].bit_hora = new Date(bit_hora);
                        self.transactions[i].bit_hora.setMinutes(self.transactions[i].bit_hora.getMinutes() + self.transactions[i].bit_hora.getTimezoneOffset());
                        */
                   }
                   self.transactions[i].prv_nombre_comercio = self.getProviderName(self.transactions[i].id_proveedor);
                }
                self.finishedSearch = true;
                self.search = false;
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching transactions For List');
//                self.showMessageError('Error while fetching clients',errResponse);
            }
        );
    };
    
    self.getProviderList = function(){        
        ProviderService.getProviderList()        
        .then(
            function(d) {                
                self.providers = d.data;                
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching providers For List');
                self.showMessageError('Error while fetching providers',errResponse);
            }
        );
    };
    
    self.getProviderList();
    
    self.getClientById = function(){        
        ClientService.getClientById(self.transaction.id_usuario)        
        .then(
            function(d) {                
                self.client = d.data;                
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching providers For List');
                self.showMessageError('Error while fetching providers',errResponse);
            }
        );
    };        
    
    self.initTransactionModalData = function (transaction, type) {            
        switch (type){
            case "detail":
                self.transaction = transaction;
                self.detailClient = true; 
                self.editingClient = false; 
                self.creatingClient = false; 
                self.editingStatusClient = false;
                break;
        };        
        self.getProviderList();
        self.getClientById();        
    };    

    self.validateTransactionSearch = function () {
        
        if (self.transactionSearch.id_usuario !== ""){
            return false;
        }else if (self.transactionSearch.usr_login !== ""){
            return false;
        }else if (self.transactionSearch.usr_fecha_registro_inicial !== "" && self.transactionSearch.usr_fecha_registro_final !== ""){
            return false;
        }else if (self.transactionSearch.bit_no_autorizacion !== "" ){
            return false;
        }else if (self.transactionSearch.status !== "" ){
            return false;
        }
        
        return true;
    };
    
    self.getProductName = function (product_id){
        for (var i = 0; i < self.products.length; i++){
            if (self.products[i].id_producto === product_id){
                if (self.products[i].nombre === null){
                    return self.products[i].id_producto;
                }else{
                    return self.products[i].nombre;
                }
            }            
        }
        return product_id;
    };
    
    self.getProviderName = function (provider_id){
        for (var i = 0; i < self.providers.length; i++){
            if (self.providers[i].id_proveedor === provider_id){
                if (self.providers[i].prv_nombre_comercio === null){
                    return self.providers[i].id_proveedor;
                }else{
                    return self.providers[i].prv_nombre_comercio;
                }
            }            
        }
        return provider_id;
    };
    
    $scope.$watch('ctrl.transaction.bit_fecha', function (newValue) {
        if (self.transaction !== undefined){
            self.transaction.bit_fecha = $filter('date')(newValue, 'yyyy/MM/dd'); 
        }        
    });  
    
    self.blockClient = function () {
        self.modalWait = true;
        ClientService.blockClient(self.client)        
        .then(
            function(d) {                
                self.client = d.data;
                self.blockingClient = false;
                self.modalWait = false;
            },
            function(errResponse){
                console.error('Error while fetching providers For List');
                self.showMessageError('Error while fetching providers',errResponse);
            }
        );
    };
    
    self.getTransactionStatus = function (transaction) {        
        var transactionStatus = $rootScope.transactionStatus[transaction.bit_status];
        
        if (transactionStatus === undefined){
            if (transaction.bit_status === 1){
                transactionStatus = {
                    longMessage: "Exitosa",
                    shortMessage: "Exitosa"
                };
            }else{
                transactionStatus = {
                    longMessage: "Fallida",
                    shortMessage: "Fallida"
                };
            }
        }
        return transactionStatus;
    };
    
}]);

