/* global angular */

app.controller('LocksController', ['$rootScope', '$scope', 'LocksService', 'ClientService', 
    'CountryService', 'ProviderService', 'BankService', 'CardService', 'ClientStatusService', '$location',
    '$window', 'DTOptionsBuilder', 'DTColumnDefBuilder',
    function($rootScope, $scope, LocksService, ClientService, CountryService, ProviderService, BankService, CardService,
        ClientStatusService, $location, $window, DTOptionsBuilder, DTColumnDefBuilder) {

    var self = this;
    self.totalLockedCards = "";
    self.lockedCardsToShow = 500;
    self.lastPageLoaded = 0;
    self.pageDT = 0;
    self.lockedCards = [];
    self.search = true;
    self.numcard = "";
    self.numImei = "";
    self.listLockedCard = [];
    self.listLockedImei = [];
    self.providers = [];
    self.banks = [];
    self.typeCards = [];
    self.clientStatus = [];
    self.showLockedCardTable=false;
    self.showLockedImeiTable=false;
    self.modalWait = false;
    self.editingClient = false;
    self.showErrorImei="error en el imei";
    self.detailClient = false;
    self.editingClient = false; 
    self.creatingClient = false; 
    self.editingStatusClient = false; 

    self.lockedCard = {
        id: "",
        activo: "",
        tarjeta: ""
    };

    self.lockedImei = {
        id: "",
        activo: "",
        tarjeta: ""
    };
    
    self.lockingCard = false;

    self.validateDataLockedCard = function () {
        if (self.lockedCard.activo == "" || self.lockedCard.tarjeta == ""){
            return true;
        }else{
            return false;
        }
    };
    
    self.validateDataLockedImei = function () {
        if (self.lockedImei.activo == "" || self.lockedImei.imei == ""){
            return true;
        }else{
            return false;
        }
    };

    self.getLockedCardList = function(){
        self.wait = true;
        LocksService.getLockedCardList()
        .then(
            function(d) {
                self.listLockedCard = d.data;
                self.showLockedCardTable=true;
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching lockedCards For List');
                self.showMessageError('Error while fetching lockedCards',errResponse);
            }
        );
    };

    self.getLockedCardsByPage = function(numberPage, maxResults){
        self.wait = true;
        LocksService.getLockedCardsByPage(numberPage, maxResults)
        .then(
            function(d) {
                self.lockedCards = self.lockedCards.concat(d.data);
                self.lastPageLoaded = self.lockedCards.length;
                self.wait = false;
                self.pageDT = 0;
            },
            function(errResponse){
                console.error('Error while fetching lockedCards For List');
                self.showMessageError('Error while fetching lockedCards',errResponse);
            }
        );
    };

    self.getNumberOfLockedCards = function(){
        self.wait = true;
        LocksService.getNumberOfLockedCards()
        .then(
            function(numberOfLockedCards) {
                self.totalLockedCards = numberOfLockedCards.data;
                if (self.totalLockedCards <= 500){
                    self.getLockedCardList();
                }else {
                    self.getLockedCardsByPage(self.lastPageLoaded, self.lockedCardsToShow);
                }
            },
            function(errResponse){
                console.error('Error while fetching lockedCards For List');
                self.showMessageError('Error while fetching lockedCards',errResponse);
            }
        );
    };

    //self.getNumberOfLockedCards();

    self.dtLockedCardsOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('full_numbers')
            .withDisplayLength(10)
            .withDOM('lBfrtip')
            .withOption('order', [0, 'desc'])
            .withOption('stateSave', true)
            .withButtons([                                
                {
                    text: 'Nueva Busqueda',
                    key: '3',
                    action: function (e, dt, node, config) {
                        self.initSearch();
                    }
                }
            ]);

    self.dtLockedCardsColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2).notSortable()
    ];

    self.dtLockedCardsInstance = {};

    self.getCountries = function (){
        var countries = {};
        for (var i = 0; i < $rootScope.countries.length; i++){
            countries[$rootScope.countries[i].idpaises] = $rootScope.countries[i].nombre;
        }
        return countries;
    };

    self.countries = self.getCountries();

    self.totalLockedImeis = "";
    self.lockedImeisToShow = 500;
    self.lastPageLoaded = 0;
    self.pageDT = 0;
    self.lockedImeis = [];

    self.getLockedImeiList = function(){
        self.wait = true;
        LocksService.getLockedImeiList()
        .then(
            function(d) {
                self.lockedImeis = d.data;
                self.showLockedImeiTable = true;
                self.wait = false;
            },
            function(errResponse){
                console.error('Error while fetching lockedImeis For List');
                self.showMessageError('Error while fetching lockedImeis',errResponse);
            }
        );
    };

    self.getLockedImeisByPage = function(numberPage, maxResults){
        self.wait = true;
        LocksService.getLockedImeisByPage(numberPage, maxResults)
        .then(
            function(d) {
                self.lockedImeis = self.lockedImeis.concat(d.data);
                self.lastPageLoaded = Math.round(self.lockedImeis.length / 10);
                self.wait = false;
               /* self.showPageInfo();*/
                self.pageDT = 0;
            },
            function(errResponse){
                console.error('Error while fetching lockedImeis For List');
                self.showMessageError('Error while fetching lockedImeis',errResponse);
            }
        );
    };

    self.getNumberOfLockedImeis = function(){
        self.wait = true;
        LocksService.getNumberOfLockedImeis()
        .then(
            function(numberOfLockedImeis) {
                self.totalLockedImeis = numberOfLockedImeis.data;
                if (self.totalLockedImeis <= 500){
                    self.getLockedImeiList();
                }else {
                    self.getLockedImeisByPage(self.lastPageLoaded, self.lockedImeisToShow);
                }
            },
            function(errResponse){
                console.error('Error while fetching lockedImeis For List');
                self.showMessageError('Error while fetching lockedImeis',errResponse);
            }
        );
    };
    /* search card*/
    self.searchLockedCard = function(numcard){
       console.log("numcard...",numcard);

       self.wait = true;
       LocksService.getLockedCard(numcard)
       .then(

           function(result) {        	   
               self.listLockedCard = result.data;
               self.showLockedCardTable=true;
               self.wait=false;
           },
           function(errResponse){
               console.error('Error while fetching listLockedCard For List');
            /*   self.showMessageError('Error while fetching listLockedCard',errResponse);*/
           }
       );



    };

    self.validateCard = function(){
         if(self.numcard == ""){
        	 return true;
         }else{
        	 return false;
         }

     };
     

    /**/
     /* search Imei*/

     /*self.validateNumber = function isNumber(numImei) {return typeof numImei === 'number';}*/
    self.searchLockedImei = function(numImei){

        console.log("numImei...",numImei);

        LocksService.getLockedImei(numImei)
        .then(
            function(result) {
         	   console.log(result);
                self.lockedImeis = result.data;
                self.listLockedImei = result.data;
                console.log(self.showLockedImeiTable);
                self.showLockedImeiTable=true;
                console.log(self.showLockedImeiTable);
                
                self.wait=false;


            },
            function(errResponse){
                console.error('Error while fetching listLockedImei For List');
             /*   self.showMessageError('Error while fetching listLockedCard',errResponse);*/
            }
        );
     };
     
     self.getClientByImei = function(lockedImei){
        console.log("numImei...",lockedImei);
        self.modalWait = true;
        self.editingClient = false;
        ClientService.getClientByImei(lockedImei.imei)
        .then(
            function(result) {
                if (result.data !== ""){
                    self.client = result.data;                
                    var registrationDate = self.client.usr_fecha_registro;
                    var bornDate = self.client.usr_fecha_nac;
                    self.client.usr_fecha_registro = new Date(registrationDate);
                    self.client.usr_fecha_registro.setMinutes(self.client.usr_fecha_registro.getMinutes() + self.client.usr_fecha_registro.getTimezoneOffset());
                    self.client.usr_fecha_nac = new Date(bornDate);
                    self.client.usr_fecha_nac.setMinutes(self.client.usr_fecha_nac.getMinutes() + self.client.usr_fecha_nac.getTimezoneOffset());
                    self.clientEmpty = false;               
                    self.showLockedImeiTable = true;                
                    self.wait = false;
                    self.modalWait = false;
                }else {
                    self.clientEmpty = true;
                    self.modalWait = false;
                    self.wait = false;
                    self.showLockedImeiTable = true;
                }
            },
            function(errResponse){
                console.error('Error while fetching listLockedImei For List');
             /*   self.showMessageError('Error while fetching listLockedCard',errResponse);*/
             self.wait=false;
             self.modalWait = false;
            }
        );
     };

     self.validateImei = function(){
          if(self.numImei == ""){
         	 return true;
          }else{
         	 return false;
          }

      };

     /**/

    self.reset = function(){
        self.totalLockedCards = "";
        self.lockedCardsToShow = 500;
        self.lastPageLoaded = 0;
        self.pageDT = 0;
        self.lockedCards = [];
        self.search = true;
        self.numcard = "";
        self.numImei = "";        
        self.providers = [];
        self.banks = [];
        self.typeCards = [];
        self.clientStatus = [];        
        self.modalWait = false;
        self.editingClient = false;
        self.showErrorImei="error en el imei";
        self.detailClient = false;
        self.editingClient = false; 
        self.creatingClient = false; 
        self.editingStatusClient = false; 

        self.lockedCard = {
            id: "",
            activo: "",
            tarjeta: ""
        };

        self.lockedImei = {
            id: "",
            activo: "",
            tarjeta: ""
        };

        self.lockingCard = false;
        self.lockingImei = false;
    };

    self.getClientByCard = function(LockedCard){
        self.wait = true;
        self.modalWait = true;
        self.editingClient = false;
        console.log(LockedCard.tarjeta);
        ClientService.getClientByCard(LockedCard.tarjeta)     
        .then(
            function(result) {
                if (result.data !== ""){
                    self.client = result.data;                
                    var registrationDate = self.client.usr_fecha_registro;
                    var bornDate = self.client.usr_fecha_nac;
                    self.client.usr_fecha_registro = new Date(registrationDate);
                    self.client.usr_fecha_registro.setMinutes(self.client.usr_fecha_registro.getMinutes() + self.client.usr_fecha_registro.getTimezoneOffset());
                    self.client.usr_fecha_nac = new Date(bornDate);
                    self.client.usr_fecha_nac.setMinutes(self.client.usr_fecha_nac.getMinutes() + self.client.usr_fecha_nac.getTimezoneOffset());
                    self.clientEmpty = false;
                    self.modalWait = false;
                    self.wait = false;
                    self.showLockedCardTable = true;
                }else{
                    self.clientEmpty = true;
                    self.modalWait = false;
                    self.wait = false;
                    self.showLockedCardTable = true;
                }                                
            },
            function(errResponse){
                console.error('Error while fetching client');
//                self.showMessageError('Error while fetching Client',errResponse);
                self.wait = false;
            }
        );
    };

    // self.getNumberOfLockedImeis();

    self.editClient = function () {
        self.wait = true;
        ClientService.editClient(self.client)        
        .then(
            function(result) {                
                console.log("Client updated successful",result);
                self.wait = false;
                self.showLockedImeiTable = false;         
            },
            function(errResponse){
                console.error('Error while fetching client');
//                self.showMessageError('Error while fetching Client',errResponse);
                self.wait = false;
            }
        );
    };

    self.dtLockedImeisOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('full_numbers')
            .withDisplayLength(10)
            .withDOM('lBfrtip')
            .withOption('order', [0, 'desc'])
            .withOption('stateSave', true)
            .withButtons([                                
                {
                    text: 'Nueva Busqueda',
                    key: '3',
                    action: function (e, dt, node, config) {
                        self.initSearch();
                    }
                }
            ]);

    self.dtLockedImeisColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2).notSortable()
    ];

    self.initSearch = function () {        
        self.reset();
        self.showLockedCardTable = false;
        self.showLockedImeiTable = false;
        self.listLockedCard = [];
        self.listLockedImei = [];
        $scope.$apply();
    };
    
    self.getCountryList = function(){        
        CountryService.getCountryList()        
        .then(
            function(d) {                
                self.countries = self.formatCountries(d.data);
                self.getProviderList();                
            },
            function(errResponse){
                console.error('Error while fetching countries List');
                self.showMessageError('Error while fetching countries',errResponse);
            }
        );
    };        
    
    self.formatCountries = function (data){
        var countries = {};
        for (var i = 0; i < data.length; i++){
            countries[data[i].idpaises] = data[i].nombre;
        }
        return countries;
    };
    
    self.getProviderList = function(){        
        ProviderService.getProviderList()        
        .then(
            function(d) {                
                self.providers = d.data;
                self.getBankList();                
            },
            function(errResponse){
                console.error('Error while fetching providers For List');
                self.showMessageError('Error while fetching providers',errResponse);
            }
        );
    };
    
    self.getBankList = function(){
        self.wait = true;
        BankService.getBankList()        
        .then(
            function(d) {                
                self.banks = d.data;
                self.getTypeCardList();                
            },
            function(errResponse){
                console.error('Error while fetching banks For List');
                self.showMessageError('Error while fetching banks',errResponse);
            }
        );
    };
    
    self.getTypeCardList = function(){
        self.wait = true;
        CardService.getTypeCardList()        
        .then(
            function(d) {                
                self.typeCards = d.data;
                self.getClientStatusList();                                
            },
            function(errResponse){
                console.error('Error while fetching typeCards For List');
                self.showMessageError('Error while fetching typeCards',errResponse);
            }
        );
    };
    
    self.getClientStatusList = function(){
        self.wait = true;
        ClientStatusService.getClientStatusList()        
        .then(
            function(d) {                
                self.clientStatus = d.data;
                self.wait = false;                
            },
            function(errResponse){
                console.error('Error while fetching clientStatus For List');
                //self.showMessageError('Error while fetching clientStatus',errResponse);
            }
        );
    };        
    
    self.getCountryList();
    
    self.addOrUpdateLockedCard = function () {
        self.wait = true;
        LocksService.updateLockedCard(self.lockedCard)        
        .then(
            function(d) {                
                self.getLockedCardList();
            },
            function(errResponse){
                console.error('Error while updating locecked card');
                //self.showMessageError('Error while fetching clientStatus',errResponse);
            }
        );
    };
    
    self.addOrUpdateLockedImei = function () {
        self.wait = true;
        LocksService.updateLockedImei(self.lockedImei)        
        .then(
            function(d) {                
                self.getLockedImeiList();
            },
            function(errResponse){
                console.error('Error while updating locecked imei');
                //self.showMessageError('Error while fetching clientStatus',errResponse);
            }
        );
    };
}]);
