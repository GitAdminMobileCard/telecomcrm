'use strict';
 
app.factory('RoleService', ['$http', '$q','$location', function($http, $q, $location){
 
    return {                                      
        
        getRoleList: function(){
            return  $http({
                url: "admin/role/list",
                method: 'GET'                
            });            
        },                
        
        editRole: function (role) {
            return $http.post('admin/role/edit', role );                       
        },
        
        addRole: function (role) {
            return $http.post('admin/role/add', role );                       
        }
    };
}]);
