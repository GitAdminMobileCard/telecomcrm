'use strict';
 
app.factory('ClientStatusService', ['$http', '$q','$location', function($http, $q, $location){
 
    return {                                      
        
        getClientStatusList: function(){
            return  $http({
                url: "admin/cliente/status/list",
                method: 'GET'                
            });            
        },
        
        getClientStatussByPage: function(pageNumber, maxResults){
            return $http.post('admin/cliente/status/list', [pageNumber, maxResults] );                       
        },
        
        getNumberOfClientStatus: function(){
            return  $http({
                url: "admin/cliente/status/total",
                method: 'POST'                
            });            
        }
    };
}]);
