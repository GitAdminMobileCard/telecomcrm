'use strict';
 
app.factory('ReverseTransactionService', ['$http', '$q','$location', function($http, $q, $location){
 
    return {                                      
        
        getReverseTransactionList: function(){
            return  $http({
                url: "admin/reverso/list",
                method: 'GET'                
            });            
        },
        
        getReverseTransactionsByPage: function(pageNumber, maxResults){
            return $http.post('admin/reverso/list', [pageNumber, maxResults] );                       
        },
        
        getNumberOfReverseTransactions: function(){
            return  $http({
                url: "admin/reverso/total",
                method: 'POST'                
            });            
        },
        
        searchReverseTransactions: function(reverseTransactionSearch){
            return $http.post('admin/reverso/searchReverse', reverseTransactionSearch );                       
        }
    };
}]);
