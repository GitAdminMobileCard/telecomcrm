'use strict';
 
app.factory('CardService', ['$http', '$q','$location', function($http, $q, $location){
 
    return {                                      
        
        getTypeCardList: function(){
            return  $http({
                url: "admin/tipoTarjeta/list",
                method: 'GET'                
            });            
        },
        
        getTypeCardsByPage: function(pageNumber, maxResults){
            return $http.post('admin/tipoTarjeta/list', [pageNumber, maxResults] );                       
        },
        
        getNumberOfTypeCards: function(){
            return  $http({
                url: "admin/tipoTarjeta/total",
                method: 'POST'                
            });            
        }
    };
}]);


