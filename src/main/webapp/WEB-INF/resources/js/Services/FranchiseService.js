'use strict';
 
app.factory('FranchiseService', ['$http', '$q','$location', function($http, $q, $location){
 
    return {                                      
        
        getFranchiseList: function(){
            return  $http({
                url: "admin/franquicia/list",
                method: 'GET'                
            });            
        }                
    };
}]);
