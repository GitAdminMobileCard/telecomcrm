'use strict';
 
app.factory('LoginService', ['$http', '$q','$location', function($http, $q, $location){
 
    return {                                

        loginUser: function(user){
            return  $http({
                url: "login",
                method: 'POST',
                params: {
                    user: user                    
                }
            });            
        },
        
        logoutUser: function(){  
            return  $http({
                url: "logout",
                method: 'POST'                
            });            
        }
         
    };
 
}]);
