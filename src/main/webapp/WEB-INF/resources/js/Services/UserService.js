'use strict';
 
app.factory('UserService', ['$http', '$q','$location', function($http, $q, $location){
 
    return {                                      
        
        getUserList: function(){
            return  $http({
                url: "admin/usuario/list",
                method: 'GET'                
            });            
        },                
        
        editUser: function (user) {
            return $http.post('admin/usuario/edit', user );                       
        },
        
        addUser: function (user) {
            return $http.post('admin/usuario/add', user );                       
        }
    };
}]);


