'use strict';
 
app.factory('ConciliationService', ['$http', '$q','$location', '$resource', function($http, $q, $location, $resource){
 
    return {                                                      
        
        getAccountStatus: function(initDate, endDate){
            return $http.post('admin/conciliacion/accountStatus', [initDate, endDate] );                       
        },
        
        createFile: function(date, type){
            return $http.post('admin/conciliacion/createFile', [date, type] );                       
        },
        
        getAmexConciliation: function(date){
            return $http.post(
                'admin/conciliacion/amexConciliation', 
                date, 
                {                    
                    transformResponse:  [function (data) {
                        // Do whatever you want!
                        return data;
                    }]
                }
            );                       
        }                        
    };
}]);
