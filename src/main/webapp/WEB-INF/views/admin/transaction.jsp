<%-- 
    Document   : transaction
    Created on : 24-mar-2017, 8:31:48
    Author     : wsolano
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div ng-controller="TransactionController as ctrl">
    <header id="mainHeader" class="w3-center w3-text-white">                
        <h4 class="w3-padding-8" >Módulo Administrador - Transacciones</h4>                
    </header>
    <div class="w3-center" ng-show="ctrl.wait">
        <i class="fa fa-spinner w3-xxxlarge w3-spin" aria-hidden="true"></i>
    </div>
    <div class="w3-margin w3-small " ng-if="ctrl.search && !ctrl.wait">        
        <form class="form-group col-lg-12 col-md-12 col-sm-12 test ">
            <div>
                <!-- busqueda por id usuario -->
                <div class="w3-margin ">
                    <div class="form-group">
                        <label class="w3-label">Busqueda por ID de Usuario:</label> 
                        <input class="form-control" type="text" name="id_usuario" id="id_usuario" value="" placeholder="Id del Usuario"
                                ng-model="ctrl.transactionSearch.id_usuario" />
                    </div>
                </div>

                <!-- busqueda por nombre usuario -->
                <div class="w3-margin ">
                    <div class="form-group">
                        <label class="w3-label">Busqueda por nombre de usuario:</label> 
                        <input class="form-control" type="text" name="usr_login" id="usr_login" value=""
                                placeholder="usuario" ng-model="ctrl.transactionSearch.usr_login" />
                    </div>
                </div>
                
                <!-- busqueda por id producto -->
<!--                <div class="w3-margin ">
                    <div class="form-group">
                        <label class="w3-label">Productos</label> 
                            <select class="form-control" name="repeatSelect" id="repeatSelect" ng-model="ctrl.select">
                                <option ng-repeat="option in ctrl.availableOptions" value="{{option.id}}">{{option.name}}</option>
                            </select>
                    </div>
                </div>-->
                
                <!-- busqueda por status de la transaccion -->
                <div class="w3-margin ">                    
                    <div class="form-group">
                        <label class="w3-label">Estado de la transacción</label> 
                        <select class="form-control" id="inputStatusTransaction"                                                    
                                ng-model="ctrl.transactionSearch.status">
                            <option value="1">
                                Exitosas
                            </option>
                            <option value="0">
                                No Exitosas
                            </option>
                        </select>
                    </div>
                </div>

                <!-- busqueda por numero de autorización-->
                <div class="w3-margin ">
                    <div class="form-group">
                        <label class="w3-label">Busqueda por numero de autorización</label>                                               
                        <input class="form-control" type="text" name="bit_no_autorizacion" id="bit_no_autorizacion" value=""
                                        placeholder="Numero de autorizacion" ng-model="ctrl.transactionSearch.bit_no_autorizacion" />                                                                           
                    </div>
                </div>
                <div class="w3-margin ">
                    <div class="form-group">
                        <label class="w3-label">Búsqueda por fecha:</label>                                                
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label class="">Desde:</label>
                                <p class="input-group">                                    
                                    <input type="text" class="form-control" uib-datepicker-popup
                                           ng-model="ctrl.transactionSearch.usr_fecha_registro_inicial" 
                                           is-open="ctrl.initialDate.opened"  
                                           ng-required="false" close-text="Cerrar" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" 
                                                ng-click="ctrl.openInitialDate()">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </button>
                                    </span>                                     
                                </p>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label class="">Hasta:</label>
                                <p class="input-group">                                    
                                    <input type="text" class="form-control" uib-datepicker-popup 
                                           ng-model="ctrl.transactionSearch.usr_fecha_registro_final" 
                                           is-open="ctrl.finalDate.opened"
                                           ng-required="false" close-text="Cerrar" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" 
                                                ng-click="ctrl.openFinalDate()">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </button>
                                    </span>
                                </p>
                            </div>
                        </div>                        
                    </div>
                </div>                
                <div class="w3-margin ">
                    <div class="form-group">
                        <button type="button" class="btn btn-raised btn-secondary" value="Buscar" 
                                ng-click="ctrl.searchTransactionByParameters()"
                        ng-disabled="ctrl.validateTransactionSearch()">Buscar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="w3-margin" ng-if="ctrl.finishedSearch && !ctrl.wait">                        
        <table id="transactionsTable" datatable="ng" 
               dt-options="ctrl.dtOptions" dt-column-defs="ctrl.dtColumnDefs" dt-instance="ctrl.dtInstance"
               class="table-responsive table-bordered w3-tiny">
            <thead>
                <tr class="table-header w3-padding-0">            
                    <th class="w3-text-white w3-padding-2 w3-center">ID Transacción</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Fecha</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Hora</th>
                    <th class="w3-text-white w3-padding-2 w3-center">ID Usuario</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Proveedor</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Tarjeta Compra</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Destino</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Importe</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Producto</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Status</th>
                    <!--<th class="w3-text-white w3-padding-2 w3-center">País</th>--> 
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="transaction in ctrl.transactions track by transaction.id_bitacora" 
                    class="w3-hover-orange w3-padding-0 w3-center">
                    <td class="w3-padding-2 w3-center">{{ transaction.id_bitacora }}</td>
                    <td class="w3-padding-2 w3-center">{{ transaction.bit_fecha | date :  "yyyy-MM-dd" }}</td>
                    <td class="w3-padding-2 w3-center">{{ transaction.bit_hora | date :  "HH:mm:ss" }}</td>
                    <td class="w3-padding-2 w3-center">{{ transaction.id_usuario }}</td>
                    <td class="w3-padding-2 w3-center">{{ transaction.prv_nombre_comercio }}</td>
                    <td class="w3-padding-2 w3-center">{{ transaction.tarjeta_compra }}</td>
                    <td class="w3-padding-2 w3-center">{{ transaction.referencia }}</td>
                    <td class="w3-padding-2 w3-center">{{ transaction.importe }}</td>
                    <td class="w3-padding-2 w3-center">{{ ctrl.getProductName(transaction.id_producto) }}</td>
                    <td class="w3-padding-2 w3-center">{{ transaction.statusMessages.shortMessage}}</td>
                    
                    <!--<td class="w3-padding-2 w3-center">{{ ctrl.countries[client.idpais] }}</td>-->
                    <td class="w3-padding-2 w3-center">
                        <a href="" class="w3-hover-opacity w3-padding-2 w3-tiny" 
                           uib-tooltip="Detalles" data-toggle="modal" data-target="#transactionModal"
                           ng-click="ctrl.initTransactionModalData(transaction,'detail')">
                            <i class="material-icons w3-text-blue">                            
                                search
                            </i>                            
                        </a>                                              
                    </td>         
                </tr>
            </tbody>
        </table>                
    </div>
    <div id="transactionModal" class="modal fade" role="dialog">
        <div ng-include="'/TelecomCRM/admin/transaccion/detail'"></div>
    </div>
</div>