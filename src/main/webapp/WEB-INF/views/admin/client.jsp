<%-- 
    Document   : tusuario
    Created on : 20-mar-2017, 10:00:35
    Author     : wsolano
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div ng-controller="ClientController as ctrl">
    <header id="mainHeader" class="w3-center w3-text-white">                
        <h4 class="w3-padding-8" >Módulo Administrador - Clientes</h4>                
    </header>
    <div class="w3-center" ng-if="ctrl.wait">
        <i class="fa fa-spinner w3-xxxlarge w3-spin" aria-hidden="true"></i>
    </div>
    <div class="w3-margin w3-small" ng-if="ctrl.search && !ctrl.wait && ctrl.action == 'search'">        
        <form class="form-group col-lg-12 col-md-12 col-sm-12 test">
            <div>
                <!--
                <div class="w3-margin">
                    <div class="form-group">
                        <button type="button" class="btn btn-sm btn-block w3-grey w3-hover-orange" 
                                id="addUserButton"
                                value="<spring:message code="client.table.option.add"/>" 
                                data-toggle="modal" data-target="#clientModal"
                                ng-click=" ctrl.reset(); ctrl.initClientModalData(ctrl.client,'create')">
                            <strong class="w3-text-white">
                                <spring:message code="client.table.option.add"/>
                            </strong>
                        </button>
                    </div>
                </div>
                -->
                <div class="w3-margin ">
                    <div class="form-group">
                        <label class="w3-label">Busqueda por Id:</label> 
                        <input class="form-control" type="text" name="id_usuario" id="id_usuario" value="" placeholder="Id del Usuario"
                                ng-model="ctrl.clientSearch.id_usuario" />
                    </div>
                </div>

                <!-- busqueda por nombre usuario -->
                <div class="w3-margin ">
                    <div class="form-group">
                        <label class="w3-label">Busqueda por nombre de usuario:</label> 
                        <input class="form-control" type="text" name="usr_login" id="usr_login" value=""
                                placeholder="usuario" ng-model="ctrl.clientSearch.usr_login" />
                    </div>
                </div>

                <!-- busqueda por nombre completo-->
                <div class="w3-margin ">
                    <div class="form-group">
                        <label class="w3-label">Busqueda por nombre y/o apellido</label>                                               
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label class="">Nombre:</label>
                                <input class="form-control" type="text" name="usr_nombre" id="usr_nombre" value=""
                                        placeholder="Nombre" ng-model="ctrl.clientSearch.usr_nombre" />                                
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label class="">Apellido:</label>                                
                                <input class="form-control" type="text" name="usr_apellido" id="usr_apellido" value=""
                                        placeholder="Apellido" ng-model="ctrl.clientSearch.usr_apellido" />
                            </div>                                                 
                        </div>
                    </div>
                </div>
                <!-- busqueda por numero telefonico-->
                <div class="w3-margin ">
                    <div class="form-group">
                        <label class="w3-label">Búsqueda por número telefónico:</label> 
                        <input class="form-control" type="text" name="usr_telefono" id="usr_telefono" value=""
                            placeholder="Telefóno" ng-model="ctrl.clientSearch.usr_telefono" />
                    </div>
                </div>
                <!-- busqueda por rango de fecha-->
                <div class="w3-margin ">
                    <div class="form-group">
                        <label class="w3-label">Búsqueda por fecha:</label>                                                
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label class="">Desde:</label>
                                <p class="input-group">                                    
                                    <input type="text" class="form-control" uib-datepicker-popup
                                           ng-model="ctrl.clientSearch.usr_fecha_registro_inicial" 
                                           is-open="ctrl.initialDate.opened"  
                                           ng-required="false" close-text="Cerrar" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" 
                                                ng-click="ctrl.openInitialDate()">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </button>
                                    </span>                                     
                                </p>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label class="">Hasta:</label>
                                <p class="input-group">                                    
                                    <input type="text" class="form-control" uib-datepicker-popup 
                                           ng-model="ctrl.clientSearch.usr_fecha_registro_final" 
                                           is-open="ctrl.finalDate.opened"
                                           ng-required="false" close-text="Cerrar" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" 
                                                ng-click="ctrl.openFinalDate()">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </button>
                                    </span>
                                </p>
                            </div>
                        </div>                        
                    </div>
                </div>                
                <div class="w3-margin ">
                    <div class="form-group">
                        <button type="button" class="btn btn-raised btn-secondary" value="Buscar" 
                                ng-click="ctrl.searchClientByParameters()"
                        ng-disabled="ctrl.validateClientSearch()">Buscar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="w3-margin" ng-if="ctrl.finishedSearch && !ctrl.wait">                        
        <table id="clientsTable" datatable="ng" 
               dt-options="ctrl.dtOptions" dt-column-defs="ctrl.dtColumnDefs" dt-instance="ctrl.dtInstance"
               class="table-responsive table-bordered w3-tiny">
            <thead>
                <tr class="table-header w3-padding-0">            
                    <th class="w3-text-white w3-padding-2 w3-center">ID</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Login</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Nombres</th>
                    <th class="w3-text-white w3-padding-2 w3-center">apellidos</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Teléfono</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Email</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Fecha registro</th>
                    <th class="w3-text-white w3-padding-2 w3-center">País</th>
                    <th class="w3-text-white w3-padding-2 w3-center">
                        <!--<a href="" class="w3-hover-opacity w3-padding-2 w3-center w3-text-white"
                           uib-tooltip="<spring:message code="client.table.option.add"/>"
                           data-toggle="modal" data-target="#clientModal"
                           ng-click=" ctrl.reset(); ctrl.initClientModalData(ctrl.client,'create')">
                            <i class="material-icons" >                            
                                person_add
                            </i>                            
                        </a>-->
                    </th>                
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="client in ctrl.clients track by client.id_usuario" 
                    class="w3-hover-orange w3-padding-0 w3-center">            
                    <td class="w3-padding-2 w3-center">{{ client.id_usuario }}</td>
                    <td class="w3-padding-2 w3-center">{{ client.usr_login }}</td>
                    <td class="w3-padding-2 w3-center">{{ client.usr_nombre }}</td>
                    <td class="w3-padding-2 w3-center">{{ client.usr_apellido }}</td>
                    <td class="w3-padding-2 w3-center">{{ client.usr_telefono }}</td>
                    <td class="w3-padding-2 w3-center">{{ client.email }}</td>
                    <td class="w3-padding-2 w3-center">{{ client.usr_fecha_registro | date :  "yyyy-MM-dd" }}</td>
                    <td class="w3-padding-2 w3-center">{{ ctrl.countries[client.idpais] }}</td>
                    <td class="w3-padding-2 w3-center">
                        <a href="" class="w3-hover-opacity w3-padding-2 w3-tiny" 
                           uib-tooltip="Detalles" data-toggle="modal" data-target="#clientModal"
                           ng-click="ctrl.initClientModalData(client,'detail')">
                            <i class="material-icons w3-text-blue">                            
                                search
                            </i>                            
                        </a>                        
                        <a href="" class="w3-hover-opacity w3-padding-2 w3-tiny" 
                           uib-tooltip="Editar" data-toggle="modal" data-target="#clientModal"
                           ng-click="ctrl.initClientModalData(client, 'edit')">
                            <i class="material-icons w3-text-green">                            
                                mode_edit
                            </i>                            
                        </a>                                               
                    </td>         
                </tr>
            </tbody>
        </table>                
    </div>
    <div id="clientModal" class="modal fade" role="dialog">
        <div ng-include="'/TelecomCRM/admin/cliente/detail'"></div>
    </div>
    <div class="w3-margin w3-small" ng-if="!ctrl.search && !ctrl.wait && ctrl.action === 'register'">
        <div ng-include="'/TelecomCRM/admin/cliente/detail'"></div>
    </div>
</div>