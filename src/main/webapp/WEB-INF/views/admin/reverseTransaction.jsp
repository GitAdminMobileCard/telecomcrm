<%-- 
    Document   : reverseTransaction
    Created on : 24-mar-2017, 11:03:17
    Author     : wsolano
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div ng-controller="ReverseTransactionController as ctrl">
    <header id="mainHeader" class="w3-center w3-text-white">                
        <h4 class="w3-padding-8" >Módulo Administrador - Reversos</h4>                
    </header>
    <div class="w3-center" ng-show="ctrl.wait">
        <i class="fa fa-spinner w3-xxxlarge w3-spin" aria-hidden="true"></i>
    </div>
    <div class="w3-margin w3-small" ng-if="ctrl.search && !ctrl.wait">        
        <form class="form-group col-lg-12 col-md-12 col-sm-12 test">
            <div>                               
                <!-- busqueda por nombre completo-->
                <div class="w3-margin ">
                    <div class="form-group">
                        <label class="w3-label">Busqueda por nombre y/o apellido</label>                                               
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label class="">Nombre:</label>
                                <input class="form-control" type="text" name="usr_nombre" id="usr_nombre" value=""
                                        placeholder="Nombre" ng-model="ctrl.reverseTransactionSearch.usr_nombre" />                                
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label class="">Apellido:</label>                                
                                <input class="form-control" type="text" name="usr_apellido" id="usr_apellido" value=""
                                        placeholder="Apellido" ng-model="ctrl.reverseTransactionSearch.usr_apellido" />
                            </div>                                                 
                        </div>
                    </div>
                </div>                
                <!-- busqueda por rango de fecha-->
                <div class="w3-margin ">
                    <div class="form-group">
                        <label class="w3-label">Búsqueda por fecha:</label>                                                
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label class="">Desde:</label>
                                <p class="input-group">                                    
                                    <input type="text" class="form-control" uib-datepicker-popup
                                           ng-model="ctrl.reverseTransactionSearch.fechaHora_inicial" 
                                           is-open="ctrl.initialDate.opened"  
                                           ng-required="false" close-text="Cerrar" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" 
                                                ng-click="ctrl.openInitialDate()">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </button>
                                    </span>                                     
                                </p>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <label class="">Hasta:</label>
                                <p class="input-group">                                    
                                    <input type="text" class="form-control" uib-datepicker-popup 
                                           ng-model="ctrl.reverseTransactionSearch.fechaHora_final" 
                                           is-open="ctrl.finalDate.opened"
                                           ng-required="false" close-text="Cerrar" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" 
                                                ng-click="ctrl.openFinalDate()">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </button>
                                    </span>
                                </p>
                            </div>
                        </div>                        
                    </div>
                </div>                
                <div class="w3-margin ">
                    <div class="form-group">
                        <button type="button" class="btn btn-raised btn-secondary" value="Buscar" 
                                ng-click="ctrl.searchReverseTransactionByParameters()"
                        ng-disabled="ctrl.validateReverseTransactionSearch()">Buscar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="w3-margin" ng-if="ctrl.finishedSearch && !ctrl.wait">                        
        <table id="reverseTransactionsTable" datatable="ng" 
               dt-options="ctrl.dtOptions" dt-column-defs="ctrl.dtColumnDefs" dt-instance="ctrl.dtInstance"
               class="table-responsive table-bordered w3-tiny">
            <thead>
                <tr class="table-header w3-padding-0">            
                    <th class="w3-text-white w3-padding-2 w3-center">ID</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Usuario</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Secuencia</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Fecha</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Autorización</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Clave operación</th>
                    <th class="w3-text-white w3-padding-2 w3-center">Clave venta</th>                    
                    <th class="w3-text-white w3-padding-2 w3-center">                        
                    </th>                
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="reverseTransaction in ctrl.reverseTransactions track by reverseTransaction.id_bitacora" 
                    class="w3-hover-orange w3-padding-0 w3-center">            
                    <td class="w3-padding-2 w3-center">{{ reverseTransaction.id_bitacora }}</td>
                    <td class="w3-padding-2 w3-center">{{ reverseTransaction.id_usuario }}</td>
                    <td class="w3-padding-2 w3-center">{{ reverseTransaction.secuencia }}</td>
                    <td class="w3-padding-2 w3-center">{{ reverseTransaction.fechahora }}</td>
                    <td class="w3-padding-2 w3-center">{{ reverseTransaction.autorizacion }}</td>
                    <td class="w3-padding-2 w3-center">{{ reverseTransaction.claveoperacion }}</td>
                    <td class="w3-padding-2 w3-center">{{ reverseTransaction.claveventa }}</td>                    
                    <td class="w3-padding-2 w3-center">
                        <a href="" class="w3-hover-opacity w3-padding-2 w3-tiny" 
                           uib-tooltip="Detalles" data-toggle="modal" data-target="#reverseTransactionModal"
                           ng-click="ctrl.initReverseTransactionModalData(reverseTransaction)">
                            <i class="material-icons w3-text-blue">                            
                                search
                            </i>                            
                        </a>                                               
                    </td>         
                </tr>
            </tbody>
        </table>
    </div>
    <div id="reverseTransactionModal" class="modal fade" role="dialog">
        <div ng-include="'/TelecomCRM/admin/reverso/detail'"></div>
    </div>
</div>