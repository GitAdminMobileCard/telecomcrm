<%-- 
    Document   : providerTemplate
    Created on : 12/04/2017, 04:21:45 PM
    Author     : Lasar-Soporte
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" ng-click="ctrl.reset()">
                &times;
            </button>
            <h4 class="modal-title">Proveedor - {{ctrl.provider.prv_nombre_comercio}}</h4>
        </div>        
        <div class="modal-body">
            <div class="w3-center" ng-if="ctrl.modalWait">
                <i class="fa fa-spinner w3-xxxlarge w3-spin" aria-hidden="true"></i>
            </div>        
            <div ng-if="!ctrl.modalWait">
                <div class="container-fluid">
                    <form class="form-horizontal" role="form">
                        <div class="form-group w3-tiny">                                                                                                                                       
                            <div class="row w3-margin">
                                <div class="col-lg-6 w3-left" ng-if="ctrl.creatingProvider">
                                    <!--<div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            ID Proveedor
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.provider.id_proveedor" ng-disabled="true"/>
                                        </div>           
                                    </div>-->
                                    <div class="row w3-margin" ng-show="!ctrl.detailProvider && !ctrl.editingProvider">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Número cliente
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="number" class="form-control" 
                                                   ng-model="ctrl.provider.prv_num_cliente" 
                                                   ng-disabled="!ctrl.editingProvider && !ctrl.creatingProvider"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin" ng-show="!ctrl.detailProvider && !ctrl.editingProvider">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Pos ID
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="number" class="form-control" 
                                                   ng-model="ctrl.provider.prv_pos_id"
                                                   ng-disabled="!ctrl.editingProvider && !ctrl.creatingProvider"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Nombre de comercio
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.provider.prv_nombre_comercio" 
                                                   ng-disabled="!ctrl.editingProvider && !ctrl.creatingProvider"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin" ng-show="!ctrl.detailProvider && !ctrl.editingProvider">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Domicílio
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.provider.prv_domicilio" 
                                                   ng-disabled="!ctrl.editingProvider && !ctrl.creatingProvider"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin" ng-show="!ctrl.detailProvider && !ctrl.editingProvider"> 
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Dealer login
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.provider.prv_dealer_login" 
                                                   ng-disabled="!ctrl.editingProvider && !ctrl.creatingProvider"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin" ng-show="!ctrl.detailProvider && !ctrl.editingProvider">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Dealer password
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="password" class="form-control" 
                                                   ng-model="ctrl.provider.prv_dealer_password" 
                                                   ng-disabled="!ctrl.editingProvider && !ctrl.creatingProvider"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin" ng-show="!ctrl.detailProvider && !ctrl.editingProvider">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Pos login
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.provider.prv_pos_login" 
                                                   ng-disabled="!ctrl.editingProvider && !ctrl.creatingProvider"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin" ng-show="!ctrl.detailProvider && !ctrl.editingProvider">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Pos password
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="password" class="form-control" 
                                                   ng-model="ctrl.provider.prv_pos_password" 
                                                   ng-disabled="!ctrl.editingProvider && !ctrl.creatingProvider"/>
                                        </div>
                                    </div>                                    
                                </div>
                                <div class="col-lg-6 w3-right" ng-if="ctrl.creatingProvider">
                                    <div class="row w3-margin" ng-show="!ctrl.detailProvider && !ctrl.editingProvider">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Nombre completo
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.provider.prv_nombre_completo" 
                                                   ng-disabled="!ctrl.editingProvider && !ctrl.creatingProvider"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin" ng-show="!ctrl.detailProvider && !ctrl.editingProvider">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Clave personal
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="password" class="form-control" 
                                                   ng-model="ctrl.provider.prv_clave_personal" 
                                                   ng-disabled="!ctrl.editingProvider && !ctrl.creatingProvider"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                for="inputBankCardClient">Status</label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <select class="form-control"                                                   
                                                    ng-model="ctrl.provider.prv_status"
                                                    ng-options="status.id_prv_status as status.desc_prv_status
                                                        for status in ctrl.providerStatuses"
                                                    ng-disabled="true">                                                                                                                                    
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Clave WS
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="password" class="form-control" 
                                                   ng-model="ctrl.provider.prv_claveWS" 
                                                   ng-disabled="!ctrl.editingProvider && !ctrl.creatingProvider"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Path
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.provider.prv_path" 
                                                   ng-disabled="!ctrl.editingProvider && !ctrl.creatingProvider"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Categoria
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.provider.id_categoria" 
                                                   ng-disabled="!ctrl.editingProvider && !ctrl.creatingProvider"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Comisión
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.provider.comision" 
                                                   ng-disabled="!ctrl.editingProvider && !ctrl.creatingProvider"/>
                                        </div>
                                    </div>                                    
                                </div>
                                <div class="col-lg-6 col-lg-offset-3" ng-if="!ctrl.creatingProvider">
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Nombre de comercio
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.provider.prv_nombre_comercio" 
                                                   ng-disabled="!ctrl.editingProvider && !ctrl.creatingProvider"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label"
                                                for="inputBankCardClient">Status</label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <select class="form-control"                                                   
                                                    ng-model="ctrl.provider.prv_status"
                                                    ng-options="status.id_prv_status as status.desc_prv_status
                                                        for status in ctrl.providerStatuses"
                                                    ng-disabled="!ctrl.editingProvider && !ctrl.creatingProvider">                                                                                                                                    
                                            </select>
                                        </div>
                                    </div>                                    
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Clave WS
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="password" class="form-control" 
                                                   ng-model="ctrl.provider.prv_claveWS" 
                                                   ng-disabled="!ctrl.editingProvider && !ctrl.creatingProvider"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Path
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.provider.prv_path" 
                                                   ng-disabled="!ctrl.editingProvider && !ctrl.creatingProvider"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Categoria
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="number" class="form-control" 
                                                   ng-model="ctrl.provider.id_categoria" 
                                                   ng-disabled="!ctrl.editingProvider && !ctrl.creatingProvider"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Comisión
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="number" class="form-control" 
                                                   ng-model="ctrl.provider.comision" 
                                                   ng-disabled="!ctrl.editingProvider && !ctrl.creatingProvider"/>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default w3-left w3-blue" 
                    ng-click="ctrl.editingProvider = true"
                    ng-if="ctrl.detailProvider && !ctrl.editingProvider">
                Editar
            </button>
            <button type="button" class="btn btn-default w3-left w3-green" 
                    ng-click="ctrl.editProvider()"
                    data-dismiss="modal" ng-if="ctrl.editingProvider">
                Guardar
            </button>
            <button type="button" class="btn btn-default w3-left w3-green" 
                    ng-click="ctrl.addProvider()"
                    data-dismiss="modal" ng-if="ctrl.creatingProvider">
                Guardar
            </button>
            <button type="button" class="btn btn-default w3-right"                    
                    data-dismiss="modal" ng-click="ctrl.reset()">
                Cancelar
            </button>            
        </div>
    </div>

</div>
