<%-- 
    Document   : reverseTransactionTemplate
    Created on : 12/04/2017, 12:00:36 PM
    Author     : Lasar-Soporte
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" ng-click="ctrl.reset()">
                &times;
            </button>
            <h4 class="modal-title">Reverso - {{ctrl.reverseTransaction.id_bitacora}}</h4>
        </div>        
        <div class="modal-body">
            <div class="w3-center" ng-if="ctrl.modalWait">
                <i class="fa fa-spinner w3-xxxlarge w3-spin" aria-hidden="true"></i>
            </div>        
            <div ng-if="!ctrl.modalWait">
                <div class="container-fluid">
                    <form class="form-horizontal" role="form">
                        <div class="form-group w3-tiny">                                                                                                                                       
                            <div class="row w3-margin">
                                <div class="col-lg-6 w3-left">
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            ID Transacción
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.reverseTransaction.id_bitacora"
                                                   ng-disabled="true"/>
                                        </div>           
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            ID Usuario
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.reverseTransaction.id_usuario" 
                                                   ng-disabled="true"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Secuencia
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.reverseTransaction.secuencia"
                                                   ng-disabled="true"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Fecha
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.reverseTransaction.fechahora" 
                                                   ng-disabled="true"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Autorización
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.reverseTransaction.autorizacion" 
                                                   ng-disabled="true"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Clave operación
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.reverseTransaction.claveoperacion" 
                                                   ng-disabled="true"/>
                                        </div>
                                    </div>                                                                                                          
                                </div>
                                <div class="col-lg-6 w3-right">                                    
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Clave venta
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.reverseTransaction.claveventa" 
                                                   ng-disabled="true"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Descripción
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.reverseTransaction.descripcion" 
                                                   ng-disabled="true"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Código de error
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.reverseTransaction.errorcode" 
                                                   ng-disabled="true"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Importe
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.reverseTransaction.importe" 
                                                   ng-disabled="true"/>
                                        </div>
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Resultado
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <textarea type="text" class="form-control" 
                                                   ng-model="ctrl.reverseTransaction.resultado" 
                                                   ng-disabled="true">
                                            </textarea>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal-footer">            
            <button type="button" class="btn btn-default w3-right"                    
                    data-dismiss="modal" ng-click="ctrl.reset()">
                Cerrar
            </button>            
        </div>
    </div>

</div>
