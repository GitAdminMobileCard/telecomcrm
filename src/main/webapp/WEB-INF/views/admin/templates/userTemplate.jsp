<%-- 
    Document   : userTemplate
    Created on : 24/04/2017, 11:09:48 AM
    Author     : Lasar-Soporte
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" ng-click="ctrl.reset()">
                &times;
            </button>
            <h4 class="modal-title">Usuario - {{ctrl.user.username}}</h4>
        </div>        
        <div class="modal-body">
            <div class="w3-center" ng-if="ctrl.modalWait">
                <i class="fa fa-spinner w3-xxxlarge w3-spin" aria-hidden="true"></i>
            </div>        
            <div ng-if="!ctrl.modalWait">
                <div class="container-fluid">
                    <form class="form-horizontal" role="form">
                        <div class="form-group w3-tiny">                                                                                                                                       
                            <div class="row w3-margin">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            ID Usuario
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.user.id" ng-disabled="true"/>
                                        </div>           
                                    </div>
                                    <div class="row w3-margin">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Nombre de usuario
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="text" class="form-control" 
                                                   ng-model="ctrl.user.username" 
                                                   ng-disabled="!ctrl.editingUser && !ctrl.creatingUser"/>
                                        </div>
                                    </div>                                                                                                                                               
                                    <div class="row w3-margin" ng-show="!ctrl.detailProvider && !ctrl.editingProvider">
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Contraseña
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input type="password" class="form-control" 
                                                   ng-model="ctrl.user.password" 
                                                   ng-disabled="!ctrl.editingUser && !ctrl.creatingUser"/>
                                        </div>
                                    </div>                                    
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="row w3-margin">                                                                                
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Activo
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input class="w3-check" type="checkbox"
                                                   ng-model="ctrl.user.enabled" 
                                                   ng-disabled="!ctrl.editingUser && !ctrl.creatingUser"/>                                            
                                        </div>
                                    </div>
                                    <div class="row w3-margin">                                                                                
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Cuenta expira
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input class="w3-check" type="checkbox"
                                                   ng-model="ctrl.user.account_expired" 
                                                   ng-disabled="!ctrl.editingUser && !ctrl.creatingUser"/>                                            
                                        </div>
                                    </div>
                                    <div class="row w3-margin">                                                                                
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Cuenta bloqueada
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input class="w3-check" type="checkbox"
                                                   ng-model="ctrl.user.account_locked" 
                                                   ng-disabled="!ctrl.editingUser && !ctrl.creatingUser"/>                                            
                                        </div>
                                    </div>
                                    <div class="row w3-margin">                                                                                
                                        <label  class="col-sm-12 col-md-4 col-lg-4 control-label">
                                            Contraseña expira
                                        </label>
                                        <div class="col-sm-12 col-md-8 col-lg-8">
                                            <input class="w3-check" type="checkbox"
                                                   ng-model="ctrl.user.password_expired" 
                                                   ng-disabled="!ctrl.editingUser && !ctrl.creatingUser"/>                                            
                                        </div>
                                    </div>                                    
                                </div>                                                                                                                                   
                            </div>
                            <div class="row w3-margin">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="row w3-margin">
                                        <h4  class="col-sm-12 col-md-12 col-lg-12">
                                            Roles del usuario
                                        </h4>
                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                            <table id="usersTable"
                                                class="table-responsive table-bordered w3-tiny">
                                                <thead>
                                                    <tr class="table-header w3-padding-0">            
                                                        <th class="w3-text-white w3-padding-2 w3-center">Role</th>                    
                                                        <th class="w3-text-white w3-padding-2 w3-center">                        
                                                        </th>                
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr ng-repeat="role in ctrl.user.roles track by role.id_role" 
                                                        class="w3-padding-0 w3-center">            
                                                        <td class="w3-padding-2 w3-center">{{ role.authority }}</td>                                      
                                                        <td class="w3-padding-2 w3-center">
                                                            <a href="" class="w3-hover-opacity w3-padding-2 w3-tiny" 
                                                               uib-tooltip="Eliminar role"
                                                               ng-click="ctrl.removeUserRole(role)">
                                                                <i class="material-icons w3-text-red">                            
                                                                    delete
                                                                </i>                            
                                                            </a>                                               
                                                        </td>         
                                                    </tr>
                                                </tbody>
                                            </table>                                           
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="row w3-margin">
                                        <h4  class="col-sm-12 col-md-12 col-lg-12">
                                            Roles
                                        </h4>
                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                            <table id="rolesTable"
                                                class="table-responsive table-bordered w3-tiny">
                                                <thead>
                                                    <tr class="table-header w3-padding-0">            
                                                        <th class="w3-text-white w3-padding-2 w3-center">Role</th>                    
                                                        <th class="w3-text-white w3-padding-2 w3-center">                        
                                                        </th>                
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr ng-repeat="role in ctrl.roles track by role.id_role"
                                                        ng-if="ctrl.showRole(role)"
                                                        class="w3-padding-0 w3-center">            
                                                        <td class="w3-padding-2 w3-center">{{role.authority}}</td>                                      
                                                        <td class="w3-padding-2 w3-center">
                                                            <a href="" class="w3-hover-opacity w3-padding-2 w3-tiny" 
                                                               uib-tooltip="Agregar role"
                                                               ng-click="ctrl.addUserRole(role)">
                                                                <i class="material-icons w3-text-blue">                            
                                                                    add_box
                                                                </i>                            
                                                            </a>                                               
                                                        </td>         
                                                    </tr>
                                                </tbody>
                                            </table>                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default w3-left w3-blue" 
                    ng-click="ctrl.editingUser = true; ctrl.detailUser = false"
                    ng-if="ctrl.detailUser && !ctrl.editingUser">
                Editar
            </button>
            <button type="button" class="btn btn-default w3-left w3-green" 
                    ng-click="ctrl.editUser()"
                    data-dismiss="modal" ng-if="ctrl.editingUser">
                Guardar
            </button>
            <button type="button" class="btn btn-default w3-left w3-green" 
                    ng-click="ctrl.addUser()"
                    data-dismiss="modal" ng-if="ctrl.creatingUser">
                Guardar
            </button>
            <button type="button" class="btn btn-default w3-right"                    
                    data-dismiss="modal" ng-click="ctrl.reset()">
                Cancelar
            </button>            
        </div>
    </div>

</div>


