<%-- 
    Document   : index
    Created on : 14/03/2017, 08:36:13 PM
    Author     : Lasar-Soporte
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="refresh" content="${session.maxInactiveInterval}"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><spring:message code="main.title"/></title>
        <link rel="shortcut icon" href="resources/img/LOGOTIPO.svg" />
        
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <link rel="stylesheet" href="resources/css/bootstrap.min.css">
        <link rel="stylesheet" href="resources/css/simple-sidebar.css">
        <link rel="stylesheet" href="resources/css/datatables.bootstrap.min.css">
        <link rel="stylesheet" href="resources/css/angular-material.min.css">
        <link rel="stylesheet" href="resources/css/buttons.dataTables.min.css">
        <link rel="stylesheet" href="resources/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="resources/css/w3.css">
        <link rel="stylesheet" href="resources/css/style.css">   
        
        <script src="resources/js/jQuery/jquery-2.0.3.min.js"></script>        
        <script src="resources/js/Bootstrap/bootstrap.min.js"></script>
        <script src="resources/js/jQuery/jquery.dataTables.min.js"></script>
        <script src="resources/js/Angular/angular.min.js"></script>
        <script src="resources/js/Angular/ui-bootstrap-tpls-2.5.0.min.js"></script>                
        <script src="resources/js/Angular/angular-route.min.js"></script>
        <script src="resources/js/Angular/angular-cookies.min.js"></script>
        <script src="resources/js/Angular/angular-sanitize.js"></script>
        <script src="resources/js/Angular/angular-animate.min.js"></script>
        <script src="resources/js/Angular/angular-aria.min.js"></script>
        <script src="resources/js/Angular/angular-resource.js"></script>
        <script src="resources/js/Angular/angular-material.min.js"></script>
        <script src="resources/js/Angular/angular-datatables.min.js"></script>
        <script src="resources/js/Angular/angular-datatables.buttons.min.js"></script>
        <script src="resources/js/Angular/dataTables.buttons.min.js"></script>
        <script src="resources/js/Angular/dataTables.buttons.min.js"></script>
        <script src="resources/js/Angular/jszip.min.js"></script>
        <script src="resources/js/Angular/pdfmake.min.js"></script>
        <script src="resources/js/Angular/vfs_fonts.js"></script>
        <script src="resources/js/Angular/buttons.html5.min.js"></script>
        <script src="resources/js/Angular/blob.js"></script>
        <script src="resources/js/Angular/FileSaver.js"></script>
        <script src="resources/js/App/app.js"></script>
                        
        <script src="resources/js/Services/CountryService.js"></script>        
        <script src="resources/js/Controller/MenuController.js"></script>       
        
        <script src="resources/js/Services/BankService.js"></script>
        <script src="resources/js/Services/CardService.js"></script>
        <script src="resources/js/Services/ClientStatusService.js"></script>
        <script src="resources/js/Services/FranchiseService.js"></script>
        
        <script src="resources/js/Services/ClientService.js"></script>
        <script src="resources/js/Controller/ClientController.js"></script>
        
        <script src="resources/js/Services/ProductService.js"></script>
        <script src="resources/js/Services/TransactionService.js"></script>
        <script src="resources/js/Controller/TransactionController.js"></script>
        
        <script src="resources/js/Services/ReverseTransactionService.js"></script>
        <script src="resources/js/Controller/ReverseTransactionController.js"></script>
        
        <script src="resources/js/Services/LocksService.js"></script>
        <script src="resources/js/Controller/LocksController.js"></script>
        
        <script src="resources/js/Services/UserTagService.js"></script>
        <script src="resources/js/Controller/UserTagController.js"></script>
        
        <script src="resources/js/Services/ProviderService.js"></script>
        <script src="resources/js/Controller/ProviderController.js"></script>
        
        <script src="resources/js/Services/RoleService.js"></script>
        <script src="resources/js/Services/UserService.js"></script>
        <script src="resources/js/Controller/UserController.js"></script>
        
        <script src="resources/js/Services/ConciliationService.js"></script>
        <script src="resources/js/Controller/ConciliationController.js"></script>       
        
    </head>
    <body ng-app="MyApp">                            
        <div id="wrapper" ng-controller="MenuController as ctrl" ng-class="ctrl.menuClass">
            <!-- Sidebar -->
            <div id="sidebar-wrapper" class="w3-card-8">
                <ul class="sidebar-nav">
                    <li>                         
                        <a href="" id="menu-toggle" ng-if="ctrl.menuClass !== 'toggled'"
                           ng-click="ctrl.menuToggle()">
                            <i class="material-icons w3-left w3-xlarge w3-text-white w3-padding-top">menu</i>
                        </a>
                        <a href="" id="menu-toggle" ng-if="ctrl.menuClass === 'toggled'"
                           ng-click="ctrl.menuToggle()">
                            <i class="material-icons w3-right w3-xlarge w3-text-white w3-padding-top">close</i>
                        </a>
                    </li>
                    <br>
                    <br>
                    <li>
                        <a href="#/home" class="w3-text-white w3-hover-green">
                            <i class="material-icons w3-left w3-xlarge w3-hover-text-white w3-padding-top"
                               data-toggle="tooltip" data-container="body" title="<spring:message code="main.menu.home"/>">
                                home
                            </i>
                            <spring:message code="main.menu.home"/>
                        </a>
                    </li>
                    <li>                        
                        <a href="" ng-click="ctrl.menu.client.active = !ctrl.menu.client.active"
                           class="w3-text-white w3-hover-green">
                            <i class="material-icons w3-left w3-xlarge w3-padding-top"
                               data-toggle="tooltip" data-container="body" title="Clientes">
                                person_outline
                            </i>
                            Clientes                            
                            <i class="material-icons w3-right w3-xlarge w3-padding-top"
                               ng-if="!ctrl.menu.client.active"
                               data-toggle="tooltip" data-container="body" title="Clientes">
                                add
                            </i>
                            <i class="material-icons w3-right w3-xlarge w3-padding-top"
                               ng-if="ctrl.menu.client.active"
                               data-toggle="tooltip" data-container="body" title="Clientes">
                                remove
                            </i>
                        </a>
                        <div uib-collapse="!ctrl.menu.client.active">
                            <div>
                                <ul class="list-group w3-text-white w3-padding-0 w3-margin-0">
                                    <li class="list-group-item w3-padding-0 w3-hover-green w3-small">
                                        <a href="" ng-click="ctrl.showClientView('register')" 
                                            class="w3-text-green w3-hover-green">
                                             <i class="material-icons w3-left w3-xlarge w3-padding-top"
                                                data-toggle="tooltip" data-container="body" title="Registrar cliente">
                                                 add_box
                                             </i>
                                             Registro
                                        </a>
                                    </li>
                                    <li class="list-group-item w3-padding-0 w3-hover-green w3-small">
                                        <a href="" ng-click="ctrl.showClientView('search')" 
                                            class="w3-text-green w3-hover-green">
                                             <i class="material-icons w3-left w3-xlarge w3-padding-top"
                                                data-toggle="tooltip" data-container="body" title="Buscar cliente">
                                                 search
                                             </i>
                                             Busqueda
                                        </a>
                                    </li>                                    
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="" ng-click="ctrl.showTransactionView()" 
                            class="w3-text-white w3-hover-green">
                            <i class="material-icons w3-left w3-xlarge w3-padding-top"
                               data-toggle="tooltip" data-container="body" title="Transacciones">
                                payment
                            </i>
                            Transacciones
                        </a>
                    </li>
                    <!--
                    <li>
                        <a href="" ng-click="ctrl.showReverseView()" class="w3-text-white w3-hover-green">
                            <i class="material-icons w3-left w3-xlarge w3-padding-top"
                               data-toggle="tooltip" data-container="body" title="Reversos">                                
                                undo
                            </i>
                            Reversos
                        </a>
                    </li>
                    -->
                    <li>
                        <a href="" ng-click="ctrl.showLocksView()" class="w3-text-white w3-hover-green">
                            <i class="material-icons w3-left w3-xlarge w3-padding-top"
                               data-toggle="tooltip" data-container="body" title="Bloqueos">                                
                                block
                            </i>
                            Bloqueos
                        </a>
                    </li>
                    <!--
                    <li>
                        <a href="" ng-click="ctrl.showTagsView()" class="w3-text-white w3-hover-green">
                            <i class="material-icons w3-left w3-xlarge w3-padding-top"
                               data-toggle="tooltip" data-container="body" title="Tag">                                
                                bookmark_border
                            </i>
                            Tag
                        </a>
                    </li>
                    -->                    
                    <li>
                        <a href="" ng-click="ctrl.showConciliationView()" class="w3-text-white w3-hover-green">
                            <i class="material-icons w3-left w3-xlarge w3-padding-top"
                               data-toggle="tooltip" data-container="body" title="Conciliación">
                                compare_arrows
                            </i>
                            Conciliación
                        </a>
                    </li>
                    <span class="menu_split"></span>
                    <li class="w3-text-black">                        
                        <a href="" ng-click="ctrl.menu.settings.active = !ctrl.menu.settings.active"
                           class="w3-text-black w3-hover-green">
                            <i class="material-icons w3-left w3-xlarge w3-padding-top"
                               data-toggle="tooltip" data-container="body" title="Configuración">
                                settings
                            </i>
                            Configuración                            
                            <i class="material-icons w3-right w3-xlarge w3-padding-top"
                               ng-if="!ctrl.menu.settings.active"
                               data-toggle="tooltip" data-container="body" title="Configuración">
                                add
                            </i>
                            <i class="material-icons w3-right w3-xlarge w3-padding-top"
                               ng-if="ctrl.menu.settings.active"
                               data-toggle="tooltip" data-container="body" title="Configuración">
                                remove
                            </i>
                        </a>
                        <div uib-collapse="!ctrl.menu.settings.active">
                            <div>
                                <ul class="list-group w3-text-white w3-padding-0 w3-margin-0">
                                    <li class="list-group-item w3-padding-0 w3-hover-green w3-small">
                                        <a href="" ng-click="ctrl.showProvidersView()" 
                                           class="w3-text-green w3-hover-green">
                                            <i class="material-icons w3-left w3-xlarge w3-padding-top"
                                               data-toggle="tooltip" data-container="body" title="Proveedores">
                                                group_work
                                            </i>
                                            Proveedores y Productos
                                        </a>
                                    </li>
                                    <li class="list-group-item w3-padding-0 w3-hover-green w3-small">
                                        <a href="" ng-click="ctrl.showUsersView()" 
                                           class="w3-text-green w3-hover-green">
                                            <i class="material-icons w3-left w3-xlarge w3-padding-top"
                                               data-toggle="tooltip" data-container="body" title="Usuarios">
                                                supervisor_account
                                            </i>
                                            Usuarios
                                        </a>
                                    </li>                                    
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="w3-text-black bottom-menu-item">
                        <a href="<c:url value="/logout" />"  class="w3-text-black w3-hover-green"> 
                            <i class="fa fa-sign-out w3-left w3-xlarge w3-padding-top"></i>
                            <spring:message code="main.menu.logout"/>
                        </a>                                                
                    </li>
                </ul>                
            </div>
            <!-- /#sidebar-wrapper -->
            <!-- Page Content -->
            <div id="page-content-wrapper" class="container">                             
                <div class="row">
                    <div class="col-lg-1 col-md-1 col-sm-1 w3-margin-0 w3-padding-top">
                        <img class="img-responsive" src="resources/img/LOGOTIPO.svg">
                    </div>                                    
                </div>
                <div class="row" ng-view></div>
            </div>            
            <!-- /#page-content-wrapper -->                                                 
        </div>
    </body>
</html>
