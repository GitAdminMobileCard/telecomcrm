<%--
    Document   : locks
    Created on : 24-mar-2017, 13:24:27
    Author     : wsolano
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div ng-controller="LocksController as ctrl">
    <header id="mainHeader" class="w3-center w3-text-white">
        <h4 class="w3-padding-8" >Módulo Administrador - Bloqueos</h4>
    </header>
    <div class="w3-center" ng-show="ctrl.wait">
        <i class="fa fa-spinner w3-xxxlarge w3-spin" aria-hidden="true"></i>
    </div>
    <div class="w3-margin" ng-hide="ctrl.wait">
        <md-content class="md-padding w3-border">
            <md-tabs md-selected="0" md-border-bottom md-dynamic-height>
                <md-tab label="Tarjetas">
                    <div class="w3-margin form-group col-md-6" ng-if="!ctrl.showLockedCardTable">
                        <div class="form-group">
                            <label>Buscar Tarjeta:</label>
                            <input type="text" class="form-control" name="idtarjeta" id="idtarjeta" value="" placeholder="buscar tarjeta" ng-model="ctrl.numcard">
                        </div>
                        <button class="btn btn-raised btn-secondary" type="button" value="Buscar" 
                                ng-click="ctrl.searchLockedCard(ctrl.numcard)"  
                                ng-disabled="ctrl.validateCard()">
                            Buscar
                        </button>                        
                        <button class="btn btn-raised btn-primary w3-right" 
                                type="button" value="Todos" 
                                ng-click="ctrl.getLockedCardList()">
                            Todos
                        </button>
                        <button class="btn btn-raised btn-default w3-right w3-margin-right" 
                                type="button" value="Bloquear tarjeta"
                                data-toggle="modal" data-target="#lockModal"
                                ng-click="ctrl.lockedCard = lockedCard; ctrl.cardDetail = true; ctrl.lockingCard = true">
                            Bloquear tarjeta
                        </button>
                    </div>                    
                    <div  ng-if="ctrl.showLockedCardTable">
                        <div class="w3-margin">
                            <table id="lockedCardsTable" datatable="ng"
                                   dt-options="ctrl.dtLockedCardsOptions"
                                   dt-column-defs="ctrl.dtLockedCardsColumnDefs"
                                   dt-instance="ctrl.dtLockedCardsInstance"
                                   class="table-responsive table-bordered w3-tiny">
                                <thead>
                                    <tr class="table-header w3-padding-0">
                                        <th class="w3-text-white w3-padding-2 w3-center">Tarjeta</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Activo</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">
                                            <!--<a href="" class="w3-hover-opacity w3-padding-2 w3-center w3-text-white" 
                                               uib-tooltip="Bloquear tarjeta" data-toggle="modal" data-target="#lockModal"
                                               ng-click="ctrl.cardDetail = true; ctrl.lockingCard = true"> 
                                                <i
                                                    class="material-icons"> block 
                                                </i>
                                            </a>-->
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr
                                        ng-repeat="lockedCard in ctrl.listLockedCard track by lockedCard.id"
                                        class="w3-hover-orange w3-padding-0 w3-center">
                                        <td class="w3-padding-2 w3-center">{{ lockedCard.tarjeta}}</td>
                                        <td class="w3-padding-2 w3-center">{{ lockedCard.activo | lockStatusFormat}}</td>
                                        <td class="w3-padding-2 w3-center">
                                            <a href="" class="w3-hover-opacity w3-padding-2 w3-tiny" 
                                                uib-tooltip="Editar" data-toggle="modal" data-target="#lockModal"
                                                ng-click="ctrl.lockedCard = lockedCard; ctrl.cardDetail = true;">
                                                 <i class="material-icons w3-text-green">                            
                                                     mode_edit
                                                 </i>                            
                                             </a>
                                            <a href="" class="w3-hover-opacity w3-padding-2 w3-tiny" 
                                               uib-tooltip="Detalles del cliente" 
                                               ng-click="ctrl.getClientByCard(lockedCard)"  
                                               data-toggle="modal" data-target="#clientModal" >
                                                <i class="material-icons w3-text-blue"> search </i>
                                            </a>                                        
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </md-tab> 
                <md-tab label="Dispositivos">
                    <div class="w3-margin form-group col-md-6" ng-if="!ctrl.showLockedImeiTable">
                        <div class="form-group">
                          <label>Buscar Dispositivo:</label>
                            <input class="form-control" type="text" name="idImei" id="idImei" value=""
                                   placeholder="buscar Imei" ng-model="ctrl.numImei" /> 

                        </div>
                        <button type="button" class="btn btn-raised btn-secondary" href="" value="Buscar"
                                       ng-click="ctrl.searchLockedImei(ctrl.numImei)"
                                       ng-disabled="ctrl.validateImei()">Buscar 
                        </button>
                        <button class="btn btn-raised btn-primary w3-right" 
                                type="button" value="Todos" 
                                ng-click="ctrl.getLockedImeiList()">
                            Todos
                        </button>
                        <button class="btn btn-raised btn-default w3-right w3-margin-right" 
                                type="button" value="Bloquear tarjeta"
                                data-toggle="modal" data-target="#lockModal"
                                ng-click="ctrl.lockedImei = lockedImei; ctrl.cardDetail = false; ctrl.lockingImei = true">
                            Bloquear Dispositivo
                        </button>
                    </div>                    
                    <div ng-if="ctrl.showLockedImeiTable">
                        <div class="w3-margin">
                            <table id="lockedImeisTable" datatable="ng" 
                                   dt-options="ctrl.dtLockedImeisOptions" dt-column-defs="ctrl.dtLockedImeisColumnDefs" 
                                   dt-instance="ctrl.dtLockedImeisInstance"
                                   class="table-responsive table-bordered w3-tiny">
                                <thead>
                                    <tr class="table-header w3-padding-0">            
                                        <th class="w3-text-white w3-padding-2 w3-center">Imei</th>
                                        <th class="w3-text-white w3-padding-2 w3-center">Activo</th>                                    
                                        <th class="w3-text-white w3-padding-2 w3-center">
                                            <!--<a href="" class="w3-hover-opacity w3-padding-2 w3-center w3-text-white"
                                               uib-tooltip="Agregar bloqueo"
                                               ng-click="ctrl.openAddLock(ctrl.lockedImei)">
                                                <i class="material-icons" >                            
                                                    block
                                                </i>                            
                                            </a>-->
                                        </th>                
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="lockedImei in ctrl.lockedImeis track by lockedImei.id" 
                                        class="w3-hover-orange w3-padding-0 w3-center">            
                                        <td class="w3-padding-2 w3-center">{{ lockedImei.imei}}</td>
                                        <td class="w3-padding-2 w3-center">{{ lockedImei.activo | lockStatusFormat}}</td>                                    
                                        <td class="w3-padding-2 w3-center">
                                            <a href="" class="w3-hover-opacity w3-padding-2 w3-tiny" 
                                            uib-tooltip="Editar" data-toggle="modal" data-target="#lockModal"
                                            ng-click="ctrl.lockedImei = lockedImei; ctrl.cardDetail = false">
                                                <i class="material-icons w3-text-green">                            
                                                    mode_edit
                                                </i>                            
                                            </a>
                                            <a href="" class="w3-hover-opacity w3-padding-2 w3-tiny" 
                                               uib-tooltip="Detalles del cliente" 
                                               data-toggle="modal" data-target="#clientModal"
                                               ng-click="ctrl.getClientByImei(lockedImei)">
                                                <i class="material-icons w3-text-blue">                            
                                                    search
                                                </i>                            
                                            </a>                                            
                                        </td>         
                                    </tr>
                                </tbody>
                            </table>                            
                        </div>
                    </div>

                </md-tab>
            </md-tabs>
        </md-content>
    </div>
    <div id="clientModal" class="modal fade" role="dialog">
        <div ng-include="'/TelecomCRM/admin/cliente/detail'"></div>
    </div>
    <div id="lockModal" class="modal fade" role="dialog">
        <div ng-include="'/TelecomCRM/admin/bloqueo/detail'"></div>
    </div>
</div>
