<%-- 
    Document   : login
    Created on : May 4, 2016, 1:47:02 PM
    Author     : wsolano
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><spring:message code="main.title"/></title>
        <link rel="shortcut icon" href="resources/img/LOGOTIPO.svg" />
        
        <link rel="stylesheet" href="resources/css/w3.css">
        <link rel="stylesheet" href="resources/css/style.css">
        <link rel="stylesheet" href="resources/css/bootstrap.min.css">
        <link rel="stylesheet" href="resources/css/buttons.dataTables.min.css">
                
        <script src="resources/js/jQuery/jquery-2.0.3.min.js"></script>
        <script src="resources/js/Bootstrap/bootstrap.min.js"></script>
        <script src="resources/js/jQuery/jquery.dataTables.min.js"></script>
        <script src="resources/js/Angular/angular.min.js"></script>
        <script src="resources/js/Angular/ui-bootstrap-tpls-1.3.1.js"></script>
        <script src="resources/js/Angular/angular-animate.min.js"></script>
        <script src="resources/js/Angular/angular-route.min.js"></script>
        <script src="resources/js/Angular/angular-sanitize.js"></script>
        <script src="resources/js/Angular/angular-animate.min.js"></script>
        <script src="resources/js/Angular/angular-aria.min.js"></script>
        <script src="resources/js/Angular/angular-resource.js"></script>
        <script src="resources/js/Angular/angular-material.min.js"></script>
        <script src="resources/js/Angular/angular-cookies.min.js"></script>        
        <script src="resources/js/Angular/angular-datatables.min.js"></script>
        <script src="resources/js/Angular/angular-datatables.buttons.min.js"></script>
        <script src="resources/js/Angular/dataTables.buttons.min.js"></script>
        <script src="resources/js/Angular/blob.js"></script>
        <script src="resources/js/Angular/FileSaver.js"></script>
        <script src="resources/js/App/app.js"></script>
        
        <script src="resources/js/Services/LoginService.js"></script>
        <script src="resources/js/Controller/LoginController.js"></script>
        
    </head>
    <body onload='document.loginForm.username.focus();' ng-app="MyApp">                        
        <div class="container w3-margin-0">            
            <div class="row w3-center">
                <div class="modal show" id="modalShow">
                    <div class="modal-dialog vertical-center">
                        <div class="modal-content">
                            <div class="modal-header w3-padding-0">
                                <c:if test="${not empty loginError}">
                                    <div class="w3-padding w3-red w3-text-white">
                                        ${loginError}
                                    </div>
                                </c:if>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <img src="resources/img/LOGOTIPO.svg" class="img-responsive">
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            <div class="modal-body">
                                <div class="w3-section">                                                                     
                                    <form name='loginForm' action="<c:url value='login' />" method='POST'>
                                        <div class="input-group w3-margin">                                            
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                            <input id="username" type="text" class="form-control" name="username" 
                                                   placeholder="<spring:message code="login.user.name.placeholder"/>">
                                        </div>
                                        <div class="input-group w3-margin">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                            <input id="password" type="password" class="form-control" name="password" 
                                                   placeholder="<spring:message code="login.user.password.placeholder"/>">
                                        </div>
                                        <div class="w3-margin">
                                            <button class="w3-btn-block w3-green w3-section w3-round-medium" type="submit">
                                                <spring:message code="login.submit"/>
                                            </button>
                                        </div>
                                        <!--<input class="w3-check" type="checkbox" checked="checked" 
                                               ng-model="ctrl.rememberUser" name="remember-me"/>
                                        Remember me?-->                                
                                        <input type="hidden" name="${_csrf.parameterName}"
                                        value="${_csrf.token}" />
                                    </form>                            
                                </div>
                            </div>
                            <div class="modal-footer w3-padding-0 w3-small">
                                <div class="w3-padding">
                                    <span class="w3-margin">Powered By</span> 
                                    <img class="img-responsive w3-right" id="poweredImage" src="resources/img/addcel_.png"/>                                                                
                                </div>                                
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>                
            </div>
        </div>                
    </body>
</html>
