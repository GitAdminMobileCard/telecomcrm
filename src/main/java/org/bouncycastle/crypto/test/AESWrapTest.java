package org.bouncycastle.crypto.test;



import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.Wrapper;
import org.bouncycastle.crypto.engines.AESWrapEngine;
import org.bouncycastle.crypto.params.KeyParameter;

public class AESWrapTest {

	byte[] kek;

	public AESWrapTest(byte[] key) {
		kek = key;
	}

	public byte[] encode(byte[] in) {
		Wrapper wrapper = new AESWrapEngine();

		wrapper.init(true, new KeyParameter(kek));

		byte[] cText = wrapper.wrap(in, 0, in.length);

		return cText;
	}

	public byte[] decode(byte[] out) throws InvalidCipherTextException {
		Wrapper wrapper = new AESWrapEngine();

		wrapper.init(false, new KeyParameter(kek));

		byte[] cText = wrapper.unwrap(out, 0, out.length);
		return cText;
	}

	public static byte[] encode(byte[] in, byte[] k) {

//		return in;
		Wrapper wrapper = new AESWrapEngine();
		byte[] cText = null;

		wrapper.init(true, new KeyParameter(k));

		System.out.println("in::" + new String(in));
		System.out.println("in.length::" + in.length);

		if ((in.length % 8) != 0) {
			int newL = (1 + (in.length / 8)) * 8;
			byte[] keyBytes = new byte[newL];
			for (int i = 0; i < newL; i++) {
				if (i < in.length) {
					keyBytes[i] = in[i];
				}else{
					keyBytes[i] = 0;
				}
			}
			cText = wrapper.wrap(keyBytes, 0, keyBytes.length);
		} else {
			cText = wrapper.wrap(in, 0, in.length);
		}

		return cText;
	}

	public static byte[] decode(byte[] out, byte[] k)
			throws InvalidCipherTextException 
	{
		Wrapper wrapper = new AESWrapEngine();

		wrapper.init(false, new KeyParameter(k));

		byte[] cText = wrapper.unwrap(out, 0, out.length);
		return cText;
//		return out;
	}
}