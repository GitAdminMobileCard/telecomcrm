package com.addcel.mx.telecomcrm.model.dao;

import com.addcel.mx.telecomcrm.model.admin.Client;
import com.addcel.mx.telecomcrm.model.admin.Provider;
import com.addcel.mx.telecomcrm.model.admin.Transaction;
import com.addcel.mx.telecomcrm.model.admin.TransactionSearch;
import com.addcel.mx.telecomcrm.model.admin.TransactionStandard;
import com.google.gson.Gson;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author wsolano
 */
@Repository("transactionDao")
@Transactional
public class TransactionDaoImpl implements TransactionDao{
    
    @Autowired
    private SessionFactory sessionFactory;
    
    private Session session;
    private Criteria criteria;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void addTransaction(Transaction transaction) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.persist(transaction);
    }

    @Override
    public void updateTransaction(Transaction transaction) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.update(transaction);
    }

    @Override
    public List<Transaction> listTransactions() {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(Transaction.class);
        cr.setFirstResult(0);
        cr.setMaxResults(500);
        List transactions = cr.list();
        
        return transactions;
    }

    @Override
    public Transaction getTransactionById(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Criteria criteria = session.createCriteria(Transaction.class);
        Criterion id_criterion = Restrictions.eq("id_bitacora", id);
        criteria.add(id_criterion);
        
        Transaction transaction = (Transaction) criteria.list();
        
        return transaction;
    }

    @Override
    public void removeTransaction(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.remove(id);
    }

    @Override
    public List<Transaction> getPageTransactions(int offset, int max) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(Transaction.class);
        cr.setFirstResult(offset);
        cr.setMaxResults(max);
        List transactions = cr.list();
        
        return transactions;
    }        

    @Override
    public Long getNumberRows() {
        
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        
        Criteria criteria = session.createCriteria(Transaction.class);

        criteria.setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();
        
        return count;
    }

    @Override
    public List<TransactionStandard> searchTransactions(TransactionSearch TSearch) {
                
        Boolean validSearch = false;
        Boolean userLoginSearch = false;
        Boolean userIdSearch = false;
        Boolean userSearch = false;       
        List<Client> client = new ArrayList<>();
        List<Transaction> transactions = new ArrayList();
        List<TransactionStandard> transactions_s = new ArrayList();
        
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(Transaction.class);

        Conjunction conjunction = Restrictions.conjunction();
        Conjunction conjunction_user = Restrictions.conjunction();
        Conjunction conjunction_provider = Restrictions.conjunction();

        if (!TSearch.getUsr_fecha_registro_final().isEmpty() && !TSearch.getUsr_fecha_registro_inicial().isEmpty()) {
            //System.out.println("FECHA" );
            try {                              
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date d1 = (Date) format.parse(TSearch.getUsr_fecha_registro_inicial());
                Date d2 = (Date) format.parse(TSearch.getUsr_fecha_registro_final());
                //System.out.println("FECHA"+d1 );
                //System.out.println("FECHA"+d2 );
                Criterion bit_horaCriterion = Restrictions.between("bit_fecha", d1, d2);
                conjunction.add(bit_horaCriterion);
                validSearch = true;
            } catch (ParseException ex) {
                Logger.getLogger(ClientDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
        if (!TSearch.getId_usuario().equals(new Long(-1)) || !TSearch.getUsr_login().isEmpty()){
            //System.out.println("search Transaction by id_usuario " + TSearch.getId_usuario());
            //System.out.println("search Transaction by usr_login " + TSearch.getUsr_login()); 
            Criteria cr_user = session.createCriteria(Client.class);
            if (!TSearch.getId_usuario().equals(new Long(-1))){
                Criterion id_usuarioCriterion = Restrictions.eq("id_usuario",TSearch.getId_usuario());
                conjunction_user.add(id_usuarioCriterion);
                conjunction.add(id_usuarioCriterion);
            }
            if (!TSearch.getUsr_login().isEmpty()){
                Criterion login_usuarioCriterion = Restrictions.eq("usr_login",TSearch.getUsr_login());
                conjunction_user.add(login_usuarioCriterion);
            }
            cr_user.setFirstResult(0);
            cr_user.setMaxResults(1);
            /*
            for (Criterion criterion : conjunction.conditions()){
                System.out.println("Criterion " + criterion.toString());
            }
            */
            cr_user.add(conjunction_user);
            client = cr_user.list();
            
            if (!client.isEmpty()){
                Criterion id_usuarioCriterion = Restrictions.eq("id_usuario", client.get(0).getId_usuario());
                conjunction.add(id_usuarioCriterion);
            }
            validSearch = true;
        }
        
        if(TSearch.getStatus().equals("1")){
            //System.out.println("ESTATus " + TSearch.getStatus());            
            Criterion statusCriterion = Restrictions.eq("bit_status", Integer.parseInt(TSearch.getStatus()));
            conjunction.add(statusCriterion);
            validSearch = true;            
        }else if (TSearch.getStatus().equals("0")){
            Criterion statusCriterion = Restrictions.ne("bit_status", 1);
            conjunction.add(statusCriterion);
            validSearch = true;
        }
        
        if(!TSearch.getBit_no_autorizacion().equals("")){
            //System.out.println("getBit_no_autorizacion " + TSearch.getBit_no_autorizacion());
            Criterion statusCriterion = Restrictions.eq("bit_no_autorizacion",TSearch.getBit_no_autorizacion());
            conjunction.add(statusCriterion);
            validSearch = true;
        }               
        
        //System.out.println("Valid Search" +  validSearch);
        if (validSearch){       
            /*
            for (Criterion criterion : conjunction.conditions()){
                System.out.println("Criterion " + criterion.toString());
            }
            */
            cr.add(conjunction);            
            transactions = cr.list();
            //System.out.println("Valid " + transactions.size());
            Mapper mapper = new DozerBeanMapper();
            for (Transaction tra : transactions){

                TransactionStandard tra_s = new TransactionStandard();
                
                mapper.map(tra,tra_s); // will copy all fields from a to copyA
                //System.out.println(tra_s);
                
                
                //Criterio de usuario
                if (client.isEmpty()){
                    Criteria cr_user = session.createCriteria(Client.class);

                    Criterion id_usuarioCriterion = Restrictions.eq("id_usuario",tra.getId_usuario());
                    conjunction_user.add(id_usuarioCriterion);

                    cr_user.setFirstResult(0);
                    cr_user.setMaxResults(1);
                    cr_user.add(conjunction_user);
                    client = cr_user.list();
                                        
                    if (!client.isEmpty()){
                        //System.out.println("Usuario " + client.get(0).getUsr_login());
                        tra_s.setUsr_nombre(client.get(0).getUsr_nombre());
                        tra_s.setUsr_apellido(client.get(0).getUsr_apellido());            
                        tra_s.setUsr_login(client.get(0).getUsr_login());
                    }
                    client = new ArrayList<>();
                }else{
                    tra_s.setUsr_nombre(client.get(0).getUsr_nombre());
                    tra_s.setUsr_apellido(client.get(0).getUsr_apellido());
                    tra_s.setUsr_login(client.get(0).getUsr_login());
                }                
                
                //Criterio de proveedor
                Criteria cr_provider = session.createCriteria(Provider.class);
                Criterion id_providerCriterion = Restrictions.eq("id_proveedor",tra.getId_proveedor());
                conjunction_provider.add(id_providerCriterion);
                cr_provider.setFirstResult(0);
                cr_provider.setMaxResults(1);
                cr_provider.add(conjunction_provider);
                List<Provider> provider = cr_provider.list();
                if (!provider.isEmpty()){
                    tra_s.setPrv_nombre_comercio(provider.get(0).getPrv_nombre_comercio());       
                }
                
                transactions_s.add(tra_s);
                //System.out.println("size "+transactions_s.size());
            }
        }   
        
        return transactions_s;
    }
    
}
