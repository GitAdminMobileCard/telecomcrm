package com.addcel.mx.telecomcrm.model.dao;

import com.addcel.mx.telecomcrm.model.admin.ProviderStatus;
import java.util.List;

/**
 *
 * @author Lasar-Soporte
 */
public interface ProviderStatusDao {
    
    public void addProviderStatus(ProviderStatus providerStatus);
    public void updateProviderStatus(ProviderStatus providerStatus);
    public List<ProviderStatus> listProviderStatus();    
    public ProviderStatus getProviderStatusById(int id);
    public void removeProviderStatus(int id);    
    public List<ProviderStatus> getPageProviderStatus(int offset, int max);
    public Long getNumberRows (); 
}
