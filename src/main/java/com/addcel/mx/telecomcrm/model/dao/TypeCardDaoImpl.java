package com.addcel.mx.telecomcrm.model.dao;

import com.addcel.mx.telecomcrm.model.admin.TypeCard;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Lasar-Soporte
 */
@Repository("typeCardDao")
@Transactional
public class TypeCardDaoImpl implements TypeCardDao{
    
    @Autowired
    private SessionFactory sessionFactory;
    
    private Session session;
    private Criteria criteria;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void addTypeCard(TypeCard typeCard) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.persist(typeCard);
    }

    @Override
    public void updateTypeCard(TypeCard typeCard) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.update(typeCard);
    }

    @Override
    public List<TypeCard> listTypeCards() {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(TypeCard.class);
        cr.setFirstResult(0);
        cr.setMaxResults(500);
        List typeCards = cr.list();
        
        return typeCards;
    }

    @Override
    public TypeCard getTypeCardById(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Criteria criteria = session.createCriteria(TypeCard.class);
        Criterion id_criterion = Restrictions.eq("id_tipo_tarjeta", id);
        criteria.add(id_criterion);
        
        TypeCard typeCard = (TypeCard) criteria.list();
        
        return typeCard;
    }

    @Override
    public void removeTypeCard(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.remove(id);
    }

    @Override
    public List<TypeCard> getPageTypeCards(int offset, int max) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(TypeCard.class);
        cr.setFirstResult(offset);
        cr.setMaxResults(max);
        List typeCards = cr.list();
        
        return typeCards;
    }        

    @Override
    public Long getNumberRows() {
        
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        
        Criteria criteria = session.createCriteria(TypeCard.class);

        criteria.setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();
        
        return count;
    }
    
}
