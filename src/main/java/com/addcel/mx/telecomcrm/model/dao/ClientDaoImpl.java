package com.addcel.mx.telecomcrm.model.dao;

import com.addcel.mx.telecomcrm.model.admin.Client;
import com.addcel.mx.telecomcrm.model.admin.ClientSearch;
import com.addcel.mx.telecomcrm.model.admin.ClientStatus;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author wsolano
 */
@Repository("clientDao")
@Transactional
public class ClientDaoImpl implements ClientDao {

    @Autowired
    private SessionFactory sessionFactory;
    
    private Session session;
    private Criteria criteria;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void addClient(Client client) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();            
        }
        Transaction tx = session.beginTransaction();
        
        session.persist(client); 
        
        tx.commit();
        
        session.close();
    }

    @Override
    public void updateClient(Client client) {                       
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();            
        }
        Transaction tx = session.beginTransaction();
        
        session.update(client); 
        
        tx.commit();
        
        session.close();
    }

    @Override
    public List<Client> listClients() {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(Client.class);
        cr.setFirstResult(0);
        cr.setMaxResults(500);
        List clients = cr.list();
        
        return clients;
    }

    @Override
    public Client getClientById(Long id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Criteria criteria = session.createCriteria(Client.class);
        Criterion id_criterion = Restrictions.eq("id_usuario", id);
        criteria.add(id_criterion);
        List<Client> clients = criteria.list();
        Client client = null;
        if (clients.size() > 0){
            client = (Client) criteria.list().get(0);
        }
        
        return client;
    }
   
    @Override
    public Client getClientByImei(String imei) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        
        Criteria criteria = session.createCriteria(Client.class);
        criteria.setMaxResults(1);
        Criterion id_criterion = Restrictions.eq("imei", imei);
        criteria.add(id_criterion);
        List<Client> clients = criteria.list();
        Client client = null;
        if (clients.size() > 0){
            client = (Client) criteria.list().get(0);
        }
        
        return client;
    }

    @Override
    public void removeClient(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.remove(id);
    }

    @Override
    public List<Client> getPageClients(int offset, int max) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(Client.class);
        cr.setFirstResult(offset);
        cr.setMaxResults(max);
        List clients = cr.list();
        
        return clients;
    }        

    @Override
    public Long getNumberRows() {
        
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        
        Criteria criteria = session.createCriteria(Client.class);

        criteria.setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();

        return count;
    }

    @Override
    public Client getClientByCard(String tarjeta) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        
        Criteria criteria = session.createCriteria(Client.class);
        criteria.setMaxResults(1);
        Criterion id_criterion = Restrictions.eq("usr_tdc_numero", tarjeta);
        criteria.add(id_criterion);
        List<Client> clients = criteria.list();
        Client client = null;
        if (clients.size() > 0){
            client = (Client) criteria.list().get(0);
        }
        
        return client;

    }

    @Override
    public List<Client> searchClients(ClientSearch clientSearch) {
        
        Boolean validSearch = false;
        List clients = new ArrayList();        
        
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(Client.class);
        Conjunction conjunction = Restrictions.conjunction();

        if (!clientSearch.getUsr_fecha_registro_final().isEmpty() && !clientSearch.getUsr_fecha_registro_inicial().isEmpty()) {
            try {                              
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date d1 = (Date) format.parse(clientSearch.getUsr_fecha_registro_inicial());
                Date d2 = (Date) format.parse(clientSearch.getUsr_fecha_registro_final());
                //System.out.println("Fecha inicial " + d1);
                //System.out.println("Fecha final " + d2);
                Criterion usr_fecha_registroCriterion = Restrictions.between("usr_fecha_registro", d1, d2);
                conjunction.add(usr_fecha_registroCriterion);
                validSearch = true;
            } catch (ParseException ex) {
                Logger.getLogger(ClientDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //System.out.println("Client ID " + clientSearch.getId_usuario());
        //System.out.println("Client ID equals " + clientSearch.getId_usuario().equals(new Long(-1)));
        if (!clientSearch.getId_usuario().equals(new Long(-1))){
            //System.out.println("id " + clientSearch.getId_usuario());
            Criterion id_usuarioCriterion = Restrictions.eq("id_usuario", clientSearch.getId_usuario());
            conjunction.add(id_usuarioCriterion);
            validSearch = true;
        }
        if(!clientSearch.getUsr_apellido().isEmpty()){
            //System.out.println("apellido ");
            Criterion apellidoCriterion = Restrictions.eq("usr_apellido", clientSearch.getUsr_apellido()); 
            conjunction.add(apellidoCriterion);
            validSearch = true;
        }
        if(!clientSearch.getUsr_nombre().isEmpty()){
            //System.out.println("nombre ");
            Criterion nombreCriterion = Restrictions.eq("usr_nombre", clientSearch.getUsr_nombre()); 
            conjunction.add(nombreCriterion);
            validSearch = true;
        }
        if(!clientSearch.getUsr_telefono().isEmpty()){
            //System.out.println("Telefono ");
            Criterion telefonoCriterion = Restrictions.eq("usr_telefono", clientSearch.getUsr_telefono());
            conjunction.add(telefonoCriterion);
            validSearch = true;
        }
        if(!clientSearch.getUsr_login().isEmpty()){
            //System.out.println("Login " + clientSearch.getUsr_login());
            Criterion loginCriterion = Restrictions.eq("usr_login", clientSearch.getUsr_login()); 
            conjunction.add(loginCriterion);
            validSearch = true;
        }                                
        if (validSearch){            
            cr.add(conjunction);
            clients = cr.list();        
        }   
        
        return clients;
    }            

    @Override
    public List<ClientStatus> getClientStatusList() {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(ClientStatus.class);
        cr.setFirstResult(0);
        cr.setMaxResults(500);
        List clientStatus = cr.list();
        
        return clientStatus;
    }
}
