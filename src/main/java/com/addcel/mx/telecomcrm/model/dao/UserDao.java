package com.addcel.mx.telecomcrm.model.dao;

import com.addcel.mx.telecomcrm.model.User;
import java.util.List;

/**
 *
 * @author Lasar-Soporte
 */
public interface UserDao {
    
    public void addUser(User user);
    public void updateUser(User user);
    public List<User> listUsers();
    public User getUserById(int id);
    public void removeUser(int id);
    
    public User authenticateUser(String username, String password);
}
