package com.addcel.mx.telecomcrm.model.admin;

import java.sql.Date;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Lasar-Soporte
 */
@Entity
@Table(name = "tarjetas_usuario")
public class ClientCard {
    
    
    @Id
    @Column(name="idtarjetasusuario")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long idtarjetasusuario; 
    private Long idusuario; 
    private String numerotarjeta; 
    private String vigencia; 
    private int estado; 
    private int idbanco; 
    private int idfranquicia; 
    private int idtarjetas_tipo; 
    private Timestamp fecharegistro; 
    private String ct; 
    private String nombre_tarjeta; 
    private String usrDomAmex; 
    private String usrCpAmex; 
    private int mobilecard;            

    public Long getIdtarjetasusuario() {
        return idtarjetasusuario;
    }

    public void setIdtarjetasusuario(Long idtarjetasusuario) {
        this.idtarjetasusuario = idtarjetasusuario;
    }

    public Long getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Long idusuario) {
        this.idusuario = idusuario;
    }

    public String getNumerotarjeta() {
        return numerotarjeta;
    }

    public void setNumerotarjeta(String numerotarjeta) {
        this.numerotarjeta = numerotarjeta;
    }

    public String getVigencia() {
        return vigencia;
    }

    public void setVigencia(String vigencia) {
        this.vigencia = vigencia;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getIdbanco() {
        return idbanco;
    }

    public void setIdbanco(int idbanco) {
        this.idbanco = idbanco;
    }

    public int getIdfranquicia() {
        return idfranquicia;
    }

    public void setIdfranquicia(int idfranquicia) {
        this.idfranquicia = idfranquicia;
    }

    public int getIdtarjetas_tipo() {
        return idtarjetas_tipo;
    }

    public void setIdtarjetas_tipo(int idtarjetas_tipo) {
        this.idtarjetas_tipo = idtarjetas_tipo;
    }

    public Timestamp getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(Timestamp fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    public String getCt() {
        return ct;
    }

    public void setCt(String ct) {
        this.ct = ct;
    }

    public String getNombre_tarjeta() {
        return nombre_tarjeta;
    }

    public void setNombre_tarjeta(String nombre_tarjeta) {
        this.nombre_tarjeta = nombre_tarjeta;
    }

    public String getUsrDomAmex() {
        return usrDomAmex;
    }

    public void setUsrDomAmex(String usrDomAmex) {
        this.usrDomAmex = usrDomAmex;
    }

    public String getUsrCpAmex() {
        return usrCpAmex;
    }

    public void setUsrCpAmex(String usrCpAmex) {
        this.usrCpAmex = usrCpAmex;
    }

    public int getMobilecard() {
        return mobilecard;
    }

    public void setMobilecard(int mobilecard) {
        this.mobilecard = mobilecard;
    }

    
    
    
}
