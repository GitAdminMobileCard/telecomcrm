/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.mx.telecomcrm.model.admin;

/**
 *
 * @author marcopascale
 */
public class TransactionSearch {
    
    private Long id_usuario;
    private String usr_login;
    private String usr_fecha_registro_inicial;
    private String usr_fecha_registro_final;
    private String bit_no_autorizacion;
    private String status;
    

    public Long getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Long id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getUsr_login() {
        return usr_login;
    }

    public void setUsr_login(String usr_login) {
        this.usr_login = usr_login;
    }

    public String getUsr_fecha_registro_inicial() {
        return usr_fecha_registro_inicial;
    }

    public void setUsr_fecha_registro_inicial(String usr_fecha_registro_inicial) {
        this.usr_fecha_registro_inicial = usr_fecha_registro_inicial;
    }

    public String getUsr_fecha_registro_final() {
        return usr_fecha_registro_final;
    }

    public void setUsr_fecha_registro_final(String usr_fecha_registro_final) {
        this.usr_fecha_registro_final = usr_fecha_registro_final;
    }

    public String getBit_no_autorizacion() {
        return bit_no_autorizacion;
    }

    public void setBit_no_autorizacion(String bit_no_autorizacion) {
        this.bit_no_autorizacion = bit_no_autorizacion;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    
}
   