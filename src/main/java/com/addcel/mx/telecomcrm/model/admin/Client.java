package com.addcel.mx.telecomcrm.model.admin;

import java.sql.Date;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Lasar-Soporte
 */
@Entity
@Table(name = "t_usuarios")
public class Client {
        
    private Long id_usuario;
    private String usr_login;
    private String usr_pwd;
    private Date usr_fecha_nac;
    private String usr_telefono;
    private String operador;
    private Timestamp usr_fecha_registro;
    private String usr_nombre;
    private String usr_apellido;
    private String usr_direccion;
    private String usr_tdc_numero;
    private String usr_tdc_vigencia;
    private Integer id_banco;
    private int id_tipo_tarjeta;
    private int id_proveedor;
    private int id_usr_status;
    private String cedula;
    private Integer tipocedula;
    private Integer recibirSMS;
    private int idpais;
    private Integer gemalto;
    private String email;
    private String imei;
    private String tipo;
    private String software;
    private String modelo;
    private String wkey;
    private String telefono_original;
    private String usr_materno;
    private String usr_sexo;
    private String usr_tel_casa;
    private String usr_tel_oficina;
    private Integer usr_id_estado;
    private String usr_ciudad;
    private String usr_calle;
    private Integer usr_num_ext;
    private String usr_num_interior;
    private String usr_colonia;
    private String usr_cp;
    private String usr_dom_amex;
    private String usr_terminos;
    private String num_ext_STR;
    private String id_ingo;
    private String usr_nss;    
    private String id_sender;    
    
    @Id
    @Column(name="id_usuario")
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Long id_usuario) {
        this.id_usuario = id_usuario;
    }
    
    public String getUsr_login() {
        return usr_login;
    }

    public void setUsr_login(String usr_login) {
        this.usr_login = usr_login;
    }

    public String getUsr_pwd() {
        return usr_pwd;
    }

    public void setUsr_pwd(String usr_pwd) {
        this.usr_pwd = usr_pwd;
    }

    public Date getUsr_fecha_nac() {
        return usr_fecha_nac;
    }

    public void setUsr_fecha_nac(Date usr_fecha_nac) {
        this.usr_fecha_nac = usr_fecha_nac;
    }

    public String getUsr_telefono() {
        return usr_telefono;
    }

    public void setUsr_telefono(String usr_telefono) {
        this.usr_telefono = usr_telefono;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }

    public Timestamp getUsr_fecha_registro() {
        return usr_fecha_registro;
    }

    public void setUsr_fecha_registro(Timestamp usr_fecha_registro) {
        this.usr_fecha_registro = usr_fecha_registro;
    }

    public String getUsr_nombre() {
        return usr_nombre;
    }

    public void setUsr_nombre(String usr_nombre) {
        this.usr_nombre = usr_nombre;
    }

    public String getUsr_apellido() {
        return usr_apellido;
    }

    public void setUsr_apellido(String usr_apellido) {
        this.usr_apellido = usr_apellido;
    }

    public String getUsr_direccion() {
        return usr_direccion;
    }

    public void setUsr_direccion(String usr_direccion) {
        this.usr_direccion = usr_direccion;
    }

    public String getUsr_tdc_numero() {
        return usr_tdc_numero;
    }

    public void setUsr_tdc_numero(String usr_tdc_numero) {
        this.usr_tdc_numero = usr_tdc_numero;
    }

    public String getUsr_tdc_vigencia() {
        return usr_tdc_vigencia;
    }

    public void setUsr_tdc_vigencia(String usr_tdc_vigencia) {
        this.usr_tdc_vigencia = usr_tdc_vigencia;
    }

    public Integer getId_banco() {
        return id_banco;
    }

    public void setId_banco(Integer id_banco) {
        this.id_banco = id_banco;
    }

    public int getId_tipo_tarjeta() {
        return id_tipo_tarjeta;
    }

    public void setId_tipo_tarjeta(int id_tipo_tarjeta) {
        this.id_tipo_tarjeta = id_tipo_tarjeta;
    }

    public int getId_proveedor() {
        return id_proveedor;
    }

    public void setId_proveedor(int id_proveedor) {
        this.id_proveedor = id_proveedor;
    }

    public int getId_usr_status() {
        return id_usr_status;
    }

    public void setId_usr_status(int id_usr_status) {
        this.id_usr_status = id_usr_status;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public Integer getTipocedula() {
        return tipocedula;
    }

    public void setTipocedula(Integer tipocedula) {
        this.tipocedula = tipocedula;
    }

    public Integer getRecibirSMS() {
        return recibirSMS;
    }

    public void setRecibirSMS(Integer recibirSMS) {
        this.recibirSMS = recibirSMS;
    }

    public int getIdpais() {
        return idpais;
    }

    public void setIdpais(int idpais) {
        this.idpais = idpais;
    }

    public Integer getGemalto() {
        return gemalto;
    }

    public void setGemalto(Integer gemalto) {
        this.gemalto = gemalto;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSoftware() {
        return software;
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getWkey() {
        return wkey;
    }

    public void setWkey(String wkey) {
        this.wkey = wkey;
    }

    public String getTelefono_original() {
        return telefono_original;
    }

    public void setTelefono_original(String telefono_original) {
        this.telefono_original = telefono_original;
    }

    public String getUsr_materno() {
        return usr_materno;
    }

    public void setUsr_materno(String usr_materno) {
        this.usr_materno = usr_materno;
    }

    public String getUsr_sexo() {
        return usr_sexo;
    }

    public void setUsr_sexo(String usr_sexo) {
        this.usr_sexo = usr_sexo;
    }

    public String getUsr_tel_casa() {
        return usr_tel_casa;
    }

    public void setUsr_tel_casa(String usr_tel_casa) {
        this.usr_tel_casa = usr_tel_casa;
    }

    public String getUsr_tel_oficina() {
        return usr_tel_oficina;
    }

    public void setUsr_tel_oficina(String usr_tel_oficina) {
        this.usr_tel_oficina = usr_tel_oficina;
    }

    public Integer getUsr_id_estado() {
        return usr_id_estado;
    }

    public void setUsr_id_estado(Integer usr_id_estado) {
        this.usr_id_estado = usr_id_estado;
    }

    public String getUsr_ciudad() {
        return usr_ciudad;
    }

    public void setUsr_ciudad(String usr_ciudad) {
        this.usr_ciudad = usr_ciudad;
    }

    public String getUsr_calle() {
        return usr_calle;
    }

    public void setUsr_calle(String usr_calle) {
        this.usr_calle = usr_calle;
    }

    public Integer getUsr_num_ext() {
        return usr_num_ext;
    }

    public void setUsr_num_ext(Integer usr_num_ext) {
        this.usr_num_ext = usr_num_ext;
    }

    public String getUsr_num_interior() {
        return usr_num_interior;
    }

    public void setUsr_num_interior(String usr_num_interior) {
        this.usr_num_interior = usr_num_interior;
    }

    public String getUsr_Colonia() {
        return usr_colonia;
    }

    public void setUsr_Colonia(String colonia) {
        this.usr_colonia = colonia;
    }

    public String getUsr_cp() {
        return usr_cp;
    }

    public void setUsr_cp(String usr_cp) {
        this.usr_cp = usr_cp;
    }

    public String getUsr_dom_amex() {
        return usr_dom_amex;
    }

    public void setUsr_dom_amex(String usr_dom_amex) {
        this.usr_dom_amex = usr_dom_amex;
    }

    public String getUsr_terminos() {
        return usr_terminos;
    }

    public void setUsr_terminos(String usr_terminos) {
        this.usr_terminos = usr_terminos;
    }

    public String getNum_ext_STR() {
        return num_ext_STR;
    }

    public void setNum_ext_STR(String num_ext_STR) {
        this.num_ext_STR = num_ext_STR;
    }

    public String getId_ingo() {
        return id_ingo;
    }

    public void setId_ingo(String id_ingo) {
        this.id_ingo = id_ingo;
    }

    public String getUsr_nss() {
        return usr_nss;
    }

    public void setUsr_nss(String usr_nss) {
        this.usr_nss = usr_nss;
    }

    public String getUsr_colonia() {
        return usr_colonia;
    }

    public void setUsr_colonia(String usr_colonia) {
        this.usr_colonia = usr_colonia;
    }

    public String getId_sender() {
        return id_sender;
    }

    public void setId_sender(String id_sender) {
        this.id_sender = id_sender;
    }
    
    
}
