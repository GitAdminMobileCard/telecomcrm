package com.addcel.mx.telecomcrm.model.dao;

import com.addcel.mx.telecomcrm.model.admin.Bank;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Lasar-Soporte
 */
@Repository("bankDao")
@Transactional
public class BankDaoImpl implements BankDao{

    @Autowired
    private SessionFactory sessionFactory;
    
    private Session session;
    private Criteria criteria;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void addBank(Bank bank) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.persist(bank);
    }

    @Override
    public void updateBank(Bank bank) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.update(bank);
    }

    @Override
    public List<Bank> listBanks() {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(Bank.class);
        cr.setFirstResult(0);
        cr.setMaxResults(500);
        List banks = cr.list();
        
        return banks;
    }

    @Override
    public Bank getBankById(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Criteria criteria = session.createCriteria(Bank.class);
        Criterion id_criterion = Restrictions.eq("id_banco", id);
        criteria.add(id_criterion);
        
        Bank bank = (Bank) criteria.list();
        
        return bank;
    }

    @Override
    public void removeBank(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.remove(id);
    }

    @Override
    public List<Bank> getPageBanks(int offset, int max) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(Bank.class);
        cr.setFirstResult(offset);
        cr.setMaxResults(max);
        List banks = cr.list();
        
        return banks;
    }        

    @Override
    public Long getNumberRows() {
        
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        
        Criteria criteria = session.createCriteria(Bank.class);

        criteria.setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();
        
        return count;
    }
    
    
}
