package com.addcel.mx.telecomcrm.model.admin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Lasar-Soporte
 */
@Entity
@Table(name = "t_prv_status")
public class ProviderStatus {
    
    @Id
    @Column(name="id_prv_status")   
    private int id_prv_status; 
    private String desc_prv_status;

    public int getId_prv_status() {
        return id_prv_status;
    }

    public void setId_prv_status(int id_prv_status) {
        this.id_prv_status = id_prv_status;
    }

    public String getDesc_prv_status() {
        return desc_prv_status;
    }

    public void setDesc_prv_status(String desc_prv_status) {
        this.desc_prv_status = desc_prv_status;
    }        
    
}
