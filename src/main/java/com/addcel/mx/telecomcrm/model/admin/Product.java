/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.mx.telecomcrm.model.admin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author marcopascale
 */
@Entity
@Table(name = "t_producto")
public class Product{



    private Integer id_producto; 
    private String pro_clave; 
    private Float pro_monto; 
    private String pro_path; 
    private Integer id_proveedor; 
    private String nombre;
    
    
    
    @Id
    @Column(name="id_producto", unique = true) 
    public Integer getId_producto() {
        return id_producto;
    }

    public void setId_producto(Integer id_producto) {
        this.id_producto = id_producto;
    }

    public String getPro_clave() {
        return pro_clave;
    }

    public void setPro_clave(String pro_clave) {
        this.pro_clave = pro_clave;
    }

    public Float getPro_monto() {
        return pro_monto;
    }

    public void setPro_monto(Float pro_monto) {
        this.pro_monto = pro_monto;
    }

    public String getPro_path() {
        return pro_path;
    }

    public void setPro_path(String pro_path) {
        this.pro_path = pro_path;
    }

    public Integer getId_proveedor() {
        return id_proveedor;
    }

    public void setId_proveedor(Integer id_proveedor) {
        this.id_proveedor = id_proveedor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
}
