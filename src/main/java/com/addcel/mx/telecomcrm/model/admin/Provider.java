package com.addcel.mx.telecomcrm.model.admin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Lasar-Soporte
 */
@Entity
@Table(name = "t_proveedor")
public class Provider {

    private Long id_proveedor;
    private Integer prv_num_cliente;
    private Integer prv_pos_id;
    private String prv_nombre_comercio;
    private String prv_domicilio;
    private String prv_dealer_login;
    private String prv_dealer_password;
    private String prv_pos_login;
    private String prv_pos_password;
    private String prv_nombre_completo;
    private String prv_clave_personal;
    private Integer prv_status;
    private String prv_claveWS;
    private String prv_path;
    private Integer id_categoria;
    private Double comision;
    private Integer compatible_Amex;
    private Integer compatible_Visa;
    private Double comision_porcentaje;
    private Double min_comision_porcentaje;
    private Double min_comision_fija;
    private String AFILIACION;
    private Double tipo_cambio;
    private Double MAX_TRANSACCION;

    @Id
    @Column(name="id_proveedor")    
    public Long getId_proveedor() {
        return id_proveedor;
    }

    public void setId_proveedor(Long id_proveedor) {
        this.id_proveedor = id_proveedor;
    }

    public Integer getPrv_num_cliente() {
        return prv_num_cliente;
    }

    public void setPrv_num_cliente(Integer prv_num_cliente) {
        this.prv_num_cliente = prv_num_cliente;
    }

    public Integer getPrv_pos_id() {
        return prv_pos_id;
    }

    public void setPrv_pos_id(Integer prv_pos_id) {
        this.prv_pos_id = prv_pos_id;
    }

    public String getPrv_nombre_comercio() {
        return prv_nombre_comercio;
    }

    public void setPrv_nombre_comercio(String prv_nombre_comercio) {
        this.prv_nombre_comercio = prv_nombre_comercio;
    }

    public String getPrv_domicilio() {
        return prv_domicilio;
    }

    public void setPrv_domicilio(String prv_domicilio) {
        this.prv_domicilio = prv_domicilio;
    }

    public String getPrv_dealer_login() {
        return prv_dealer_login;
    }

    public void setPrv_dealer_login(String prv_dealer_login) {
        this.prv_dealer_login = prv_dealer_login;
    }

    public String getPrv_dealer_password() {
        return prv_dealer_password;
    }

    public void setPrv_dealer_password(String prv_dealer_password) {
        this.prv_dealer_password = prv_dealer_password;
    }

    public String getPrv_pos_login() {
        return prv_pos_login;
    }

    public void setPrv_pos_login(String prv_pos_login) {
        this.prv_pos_login = prv_pos_login;
    }

    public String getPrv_pos_password() {
        return prv_pos_password;
    }

    public void setPrv_pos_password(String prv_pos_password) {
        this.prv_pos_password = prv_pos_password;
    }

    public String getPrv_nombre_completo() {
        return prv_nombre_completo;
    }

    public void setPrv_nombre_completo(String prv_nombre_completo) {
        this.prv_nombre_completo = prv_nombre_completo;
    }

    public String getPrv_clave_personal() {
        return prv_clave_personal;
    }

    public void setPrv_clave_personal(String prv_clave_personal) {
        this.prv_clave_personal = prv_clave_personal;
    }

    public Integer getPrv_status() {
        return prv_status;
    }

    public void setPrv_status(Integer prv_status) {
        this.prv_status = prv_status;
    }

    public String getPrv_claveWS() {
        return prv_claveWS;
    }

    public void setPrv_claveWS(String prv_claveWS) {
        this.prv_claveWS = prv_claveWS;
    }

    public String getPrv_path() {
        return prv_path;
    }

    public void setPrv_path(String prv_path) {
        this.prv_path = prv_path;
    }

    public Integer getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(Integer id_categoria) {
        this.id_categoria = id_categoria;
    }

    public Double getComision() {
        return comision;
    }

    public void setComision(Double comision) {
        this.comision = comision;
    }

    public Integer getCompatible_Amex() {
        return compatible_Amex;
    }

    public void setCompatible_Amex(Integer compatible_Amex) {
        this.compatible_Amex = compatible_Amex;
    }

    public Integer getCompatible_Visa() {
        return compatible_Visa;
    }

    public void setCompatible_Visa(Integer compatible_Visa) {
        this.compatible_Visa = compatible_Visa;
    }

    public Double getComision_porcentaje() {
        return comision_porcentaje;
    }

    public void setComision_porcentaje(Double comision_porcentaje) {
        this.comision_porcentaje = comision_porcentaje;
    }

    public Double getMin_comision_porcentaje() {
        return min_comision_porcentaje;
    }

    public void setMin_comision_porcentaje(Double min_comision_porcentaje) {
        this.min_comision_porcentaje = min_comision_porcentaje;
    }

    public Double getMin_comision_fija() {
        return min_comision_fija;
    }

    public void setMin_comision_fija(Double min_comision_fija) {
        this.min_comision_fija = min_comision_fija;
    }

    public String getAFILIACION() {
        return AFILIACION;
    }

    public void setAFILIACION(String AFILIACION) {
        this.AFILIACION = AFILIACION;
    }

    public Double getTipo_cambio() {
        return tipo_cambio;
    }

    public void setTipo_cambio(Double tipo_cambio) {
        this.tipo_cambio = tipo_cambio;
    }

    public Double getMAX_TRANSACCION() {
        return MAX_TRANSACCION;
    }

    public void setMAX_TRANSACCION(Double MAX_TRANSACCION) {
        this.MAX_TRANSACCION = MAX_TRANSACCION;
    }
    
    
    
}
