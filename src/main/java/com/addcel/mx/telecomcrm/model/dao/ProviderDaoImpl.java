package com.addcel.mx.telecomcrm.model.dao;

import com.addcel.mx.telecomcrm.model.admin.Provider;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Lasar-Soporte
 */
@Repository("providerDao")
@Transactional
public class ProviderDaoImpl implements ProviderDao{

    @Autowired
    private SessionFactory sessionFactory;
    
    private Session session;
    private Criteria criteria;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void addProvider(Provider provider) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Transaction tx = session.beginTransaction();        
        session.persist(provider);
        tx.commit();        
        session.close();        
    }

    @Override
    public void updateProvider(Provider provider) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Transaction tx = session.beginTransaction();        
        session.update(provider);        
        tx.commit();        
        session.close();
    }

    @Override
    public List<Provider> listProviders() {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(Provider.class);
        List providers = cr.list();
        
        return providers;
    }

    @Override
    public List<Long> getProvidersId() {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(Provider.class);
        cr.setProjection(Projections.property("id_proveedor"));
        List providers = cr.list();
        
        return providers;        
    }    
    
    @Override
    public Provider getProviderById(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Criteria criteria = session.createCriteria(Provider.class);
        Criterion id_criterion = Restrictions.eq("idPaises", id);
        criteria.add(id_criterion);
        
        Provider provider = (Provider) criteria.list();
        
        return provider;
    }

    @Override
    public void removeProvider(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.remove(id);
    }

    @Override
    public List<Provider> getPageProviders(int offset, int max) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(Provider.class);
        cr.setFirstResult(offset);
        cr.setMaxResults(max);
        List providers = cr.list();
        
        return providers;
    }        

    @Override
    public Long getNumberRows() {
        
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        
        Criteria criteria = session.createCriteria(Provider.class);

        criteria.setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();
        
        return count;
    }
    
}
