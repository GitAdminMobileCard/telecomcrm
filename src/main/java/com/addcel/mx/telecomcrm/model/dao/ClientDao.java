package com.addcel.mx.telecomcrm.model.dao;

import com.addcel.mx.telecomcrm.model.admin.Client;
import com.addcel.mx.telecomcrm.model.admin.ClientSearch;
import com.addcel.mx.telecomcrm.model.admin.ClientStatus;
import java.util.List;

/**
 *
 * @author wsolano
 */
public interface ClientDao {
    
    public void addClient(Client client);
    public void updateClient(Client client);
    public List<Client> listClients();
    public Client getClientById(Long id);
    public void removeClient(int id);    
    public List<Client> getPageClients(int offset, int max);
    public Long getNumberRows ();
    public Client getClientByImei(String imei);
    public Client getClientByCard(String tarjeta);
    public List<Client> searchClients (ClientSearch clientSearch);
        
    public List<ClientStatus> getClientStatusList();
}
