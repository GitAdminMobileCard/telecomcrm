package com.addcel.mx.telecomcrm.model.dao;

import com.addcel.mx.telecomcrm.model.admin.LockedCard;
import com.addcel.mx.telecomcrm.model.admin.LockedCard;
import com.addcel.mx.telecomcrm.model.admin.LockedImei;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author wsolano
 */
@Repository("lockedDao")
@Transactional
public class LockedDaoImpl implements LockDao{
    
    @Autowired
    private SessionFactory sessionFactory;
    
    private Session session;
    private Criteria criteria;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addLockedCard(LockedCard lockedCard) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }            
        Transaction tx = session.beginTransaction();        
        session.saveOrUpdate(lockedCard);
        tx.commit();        
        session.close();
    }

    @Override
    public void updateLockedCard(LockedCard lockedCard) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }        
        Transaction tx = session.beginTransaction();        
        session.update(lockedCard);
        tx.commit();        
        session.close();
    }

    @Override
    public List<LockedCard> listLockedCards() {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(LockedCard.class);
        List clients = cr.list();
        
        return clients;
    }

    @Override
    public LockedCard getLockedCardById(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Criteria criteria = session.createCriteria(LockedCard.class);
        Criterion id_criterion = Restrictions.eq("tarjeta", id);
        criteria.add(id_criterion);
        
        LockedCard lockedCard = (LockedCard) criteria.list();
        
        return lockedCard;
    }

    @Override
    public void removeLockedCard(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.remove(id);
    }

    @Override
    public List<LockedCard> getLockedCards() {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(LockedCard.class);
        List lockedCards = cr.list();
        
        return lockedCards;
    }        

    @Override
    public Long getNumberLockedCardRows() {
        
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        
        Criteria criteria = session.createCriteria(LockedCard.class);

        criteria.setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();
        
        return count;
    }
    
    @Override
    public void addLockedImei(LockedImei lockedImei) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }        
        Transaction tx = session.beginTransaction();        
        session.saveOrUpdate(lockedImei);
        tx.commit();        
        session.close();
        
    }

    @Override
    public void updateLockedImei(LockedImei lockedImei) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }        
        Transaction tx = session.beginTransaction();        
        session.update(lockedImei);
        tx.commit();        
        session.close();
    }

    @Override
    public List<LockedImei> listLockedImeis() {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(LockedImei.class);
        List lockedImeis = cr.list();
        
        return lockedImeis;
    }

    @Override
    public LockedImei getLockedImeiById(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Criteria criteria = session.createCriteria(LockedImei.class);
        Criterion id_criterion = Restrictions.eq("imei", id);
        criteria.add(id_criterion);
        
        LockedImei lockedImei = (LockedImei) criteria.list();
        
        return lockedImei;
    }

    @Override
    public void removeLockedImei(int id) {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.remove(id);
    }

    @Override
    public List<LockedImei> getLockedImeis() {
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Criteria cr = session.createCriteria(LockedImei.class);        
        List lockedImeis = cr.list();
        
        return lockedImeis;
    }        

    @Override
    public Long getNumberLockedImeiRows() {
        
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        
        Criteria criteria = session.createCriteria(LockedImei.class);

        criteria.setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();
        
        return count;
    }

	@Override
	public List<LockedCard> getSearchLockedCard(String numcard) {
		// TODO Auto-generated method stub
		 try {
	            session = sessionFactory.getCurrentSession();
	        } catch (HibernateException e) {
	            session = sessionFactory.openSession();
	        }
	        Criteria criteria = session.createCriteria(LockedCard.class);
	        Criterion id_criterion = Restrictions.eq("tarjeta", numcard);
	        criteria.add(id_criterion);
	        
	        List<LockedCard> lockedCards = (List<LockedCard>) criteria.list();
	        
	        return lockedCards;
	}

	@Override
	public List<LockedImei> getSearchLockedImei(String numImei) {
		// TODO Auto-generated method stub
		try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Criteria criteria = session.createCriteria(LockedImei.class);
        Criterion id_criterion = Restrictions.eq("imei", numImei);
        criteria.add(id_criterion);
        
        List<LockedImei> lockedImeis = criteria.list();
        
        return lockedImeis;
	}
    
}
