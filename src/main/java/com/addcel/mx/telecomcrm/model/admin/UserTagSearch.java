package com.addcel.mx.telecomcrm.model.admin;

/**
 *
 * @author Lasar-Soporte
 */
public class UserTagSearch {
    
    private Long id_usuario;
    private String tag;

    public Long getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Long id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
    
    
}
