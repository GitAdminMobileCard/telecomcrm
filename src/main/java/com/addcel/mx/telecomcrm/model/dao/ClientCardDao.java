package com.addcel.mx.telecomcrm.model.dao;

import com.addcel.mx.telecomcrm.model.admin.ClientCard;
import java.util.List;

/**
 *
 * @author Lasar-Soporte
 */
public interface ClientCardDao {
    
    public void addClientCard(ClientCard clientCard);
    public void updateClientCard(ClientCard clientCard);
    public List<ClientCard> listClientCards();
    public ClientCard getClientCardById(Long id);
    public List<ClientCard> getClientCardsByClientId(Long clientId);
    public ClientCard getClientCardByNumberCard(String numcard);
    public void removeClientCard(int id);            
    
}
