package com.addcel.mx.telecomcrm.model.dao;

import com.addcel.mx.telecomcrm.model.admin.Country;
import java.util.List;

/**
 *
 * @author Lasar-Soporte
 */
public interface CountryDao {
    
    public void addCountry(Country country);
    public void updateCountry(Country country);
    public List<Country> listCountries();
    public Country getCountryById(int id);
    public void removeCountry(int id);    
    public List<Country> getPageCountries(int offset, int max);
    public Long getNumberRows ();
    
}
