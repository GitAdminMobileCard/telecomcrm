package com.addcel.mx.telecomcrm.model.dao;

import com.addcel.mx.telecomcrm.model.admin.LockedCard;
import com.addcel.mx.telecomcrm.model.admin.LockedImei;

import java.util.List;

/**
 *
 * @author wsolano
 */
public interface LockDao {
    
    public void addLockedCard(LockedCard lockedCard);
    public void updateLockedCard(LockedCard lockedCard);
    public List<LockedCard> listLockedCards();
    public LockedCard getLockedCardById(int id);
    public void removeLockedCard(int id);    
    public List<LockedCard> getLockedCards();
    public List<LockedCard> getSearchLockedCard(String numcard);
    public Long getNumberLockedCardRows ();
    
    public void addLockedImei(LockedImei lockedImei);
    public void updateLockedImei(LockedImei lockedImei);
    public List<LockedImei> listLockedImeis();
    public LockedImei getLockedImeiById(int id);
    public void removeLockedImei(int id);    
    public List<LockedImei> getLockedImeis();
    public List<LockedImei> getSearchLockedImei(String numImei);
    public Long getNumberLockedImeiRows ();
    
}
