package com.addcel.mx.telecomcrm.model.dao;

import com.addcel.mx.telecomcrm.model.admin.Provider;
import java.util.List;

/**
 *
 * @author Lasar-Soporte
 */
public interface ProviderDao {
    
    public void addProvider(Provider providers);
    public void updateProvider(Provider providers);
    public List<Provider> listProviders();
    public List<Long> getProvidersId();
    public Provider getProviderById(int id);
    public void removeProvider(int id);    
    public List<Provider> getPageProviders(int offset, int max);
    public Long getNumberRows ();
    
}
