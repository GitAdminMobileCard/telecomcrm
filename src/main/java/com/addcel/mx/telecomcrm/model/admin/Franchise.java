package com.addcel.mx.telecomcrm.model.admin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Lasar-Soporte
 */
@Entity
@Table(name = "franquicias")
public class Franchise {
    
    @Id
    @Column(name="idfranquicias")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int idfranquicias; 
    private String nombre; 
    private int estado; 
    private int rangoinicio; 
    private int rangofin; 
    private String regexpr;

    public int getIdfranquicias() {
        return idfranquicias;
    }

    public void setIdfranquicias(int idfranquicias) {
        this.idfranquicias = idfranquicias;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getRangoinicio() {
        return rangoinicio;
    }

    public void setRangoinicio(int rangoinicio) {
        this.rangoinicio = rangoinicio;
    }

    public int getRangofin() {
        return rangofin;
    }

    public void setRangofin(int rangofin) {
        this.rangofin = rangofin;
    }

    public String getRegexpr() {
        return regexpr;
    }

    public void setRegexpr(String regexpr) {
        this.regexpr = regexpr;
    }
            
}
