package com.addcel.mx.telecomcrm.security;

import com.addcel.mx.telecomcrm.model.User;
import com.addcel.mx.telecomcrm.model.Role;
import com.addcel.mx.telecomcrm.model.UserForAuth;
import com.addcel.mx.telecomcrm.service.RoleServiceImpl;
import com.addcel.mx.telecomcrm.service.UserServiceImpl;
import java.util.List;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

/**
 *
 * @author wsolano
 */
@Component
public class customAuthenticationProvider implements AuthenticationProvider  {
        
    @Autowired
    private UserServiceImpl userService;    
    
    @Autowired
    private RoleServiceImpl roleService;         
    
    @Autowired
    private HttpServletRequest request;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException{
                
        String username = authentication.getName();
        String password = (String) authentication.getCredentials();        
        UserForAuth userForAuth = new UserForAuth();
        userForAuth.setUsername(username);
        User user = null;       
        List<GrantedAuthority> authorities = null;
        
        user = userService.authenticateUser(username, password);
        
        userForAuth.setId(user.getId());
        authorities = new ArrayList<>();
        List<Role> userRoles = user.getRoles();
        if (userRoles != null){        
            for (Role userRole : userRoles) {            
                System.out.println("Role " + userRole.getAuthority());
                authorities.add(new SimpleGrantedAuthority(userRole.getAuthority()));
            }                    
        }      
        userForAuth.setAuthorities(authorities);
        
        return new MyAuthentication(userForAuth, password, authorities);
    }

    @Override
    public boolean supports(Class<? extends Object> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }  

}
