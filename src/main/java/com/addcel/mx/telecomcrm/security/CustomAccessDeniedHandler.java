package com.addcel.mx.telecomcrm.security;

import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.security.access.AccessDeniedException;

/**
 *
 * @author wsolano
 */
public class CustomAccessDeniedHandler implements org.springframework.security.web.access.AccessDeniedHandler{
    
    private String accessDeniedUrl;
    private ServletContext servletContext;
    
    @Autowired
    private ResourceBundleMessageSource resource;
    
    public CustomAccessDeniedHandler() {
    }

    public CustomAccessDeniedHandler(String accessDeniedUrl) {
        this.accessDeniedUrl = accessDeniedUrl;
    }

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException {
        response.sendRedirect(accessDeniedUrl);
        servletContext = request.getServletContext();
        System.out.println("Cliente " + servletContext.getAttribute("error"));
        request.getSession().setAttribute("errorMessage", servletContext.getAttribute("error").toString());        
    }

    public String getAccessDeniedUrl() {
        return accessDeniedUrl;
    }

    public void setAccessDeniedUrl(String accessDeniedUrl) {
        this.accessDeniedUrl = accessDeniedUrl;
    }
    
}
