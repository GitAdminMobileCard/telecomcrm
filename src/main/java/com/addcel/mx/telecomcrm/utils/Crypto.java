package com.addcel.mx.telecomcrm.utils;

/**
 *
 * @author Lasar-Soporte
 */
public class Crypto {

    private static final String seed = "5525963513";

    public static String getCardNumber(String Telefono) {
        return Crypto.aesDecrypt(Telefono);
    }

    public static String escondeDigitos(String numero) {
        String numeroDeTarjeta = null;
        String numeroEscondido = "";
        int i;        
        
        if (isNumeric(numero)) {
            numeroDeTarjeta = numero;
        } else {            
            numeroDeTarjeta = getCardNumber(numero);
        }

        //System.out.println("Numero de tarjeta " + numeroDeTarjeta);
        
        for (i = 0; i < numeroDeTarjeta.length() - 4; i++) {
            numeroEscondido = numeroEscondido + "*";
        }
        
        if (isNumeric(numeroDeTarjeta)) {
            try {
                return numeroEscondido + numeroDeTarjeta.substring(numeroDeTarjeta.length() - 4, numeroDeTarjeta.length());
            } catch (StringIndexOutOfBoundsException s) {
                return numero;
            }
        } else {
            return numero;
        }
    }

    public static boolean isNumeric(String cadena) {

        try {

            char myArray[] = cadena.toCharArray();
            for (int i = 0; i < myArray.length; i++) {

                String num = String.valueOf(myArray[i]);

                Integer.parseInt(num);

            }
            return true;

        } catch (NumberFormatException nfe) {

            return false;

        }

    }

    public static String parsePass(String pass) {
        int len = pass.length();
        String key = "";

        for (int i = 0; i < 32 / len; i++) {
            key += pass;
        }

        int carry = 0;
        while (key.length() < 32) {
            key += pass.charAt(carry);
            carry++;
        }
        return key;
    }

    public static String aesEncrypt(String cleartext) {
        String encryptedText;

        try {
            return AESBinFormat.encode(cleartext, parsePass(seed));
        } catch (Exception e) {
            encryptedText = "";
        }
        //System.out.println("Texto encriptado " + encryptedText);
        return encryptedText;
    }

    public static String aesDecrypt(String encrypted) {
        String decryptedText;

        try {
            return AESBinFormat.decode(encrypted, parsePass(seed));
        } catch (Exception e) {
            decryptedText = "";
        }

        return decryptedText;
    }

}
