package com.addcel.mx.telecomcrm.service;

import com.addcel.mx.telecomcrm.model.admin.ClientCard;
import com.addcel.mx.telecomcrm.model.dao.ClientCardDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lasar-Soporte
 */
@Service
public class ClientCardServiceImpl implements ClientCardService{

    @Autowired
    ClientCardDao clientCardDao;
    
    @Override
    public void addClientCard(ClientCard clientCard) {
        clientCardDao.addClientCard(clientCard);
    }

    @Override
    public void updateClientCard(ClientCard clientCard) {
        clientCardDao.updateClientCard(clientCard);
    }

    @Override
    public List<ClientCard> listClientCards() {
        return clientCardDao.listClientCards();
    }

    @Override
    public ClientCard getClientCardById(Long id) {
        return clientCardDao.getClientCardById(id);
    }

    @Override
    public List<ClientCard> getClientCardsByClientId(Long clientId) {
        return clientCardDao.getClientCardsByClientId(clientId);
    }

    @Override
    public ClientCard getClientCardByNumberCard(String numcard) {
        return clientCardDao.getClientCardByNumberCard(numcard);
    }

    @Override
    public void removeClientCard(int id) {
        clientCardDao.removeClientCard(id);
    }       
    
}
