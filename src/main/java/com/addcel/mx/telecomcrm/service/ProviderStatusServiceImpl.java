package com.addcel.mx.telecomcrm.service;

import com.addcel.mx.telecomcrm.model.admin.ProviderStatus;
import com.addcel.mx.telecomcrm.model.dao.ProviderStatusDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lasar-Soporte
 */
@Service
public class ProviderStatusServiceImpl implements ProviderStatusService{

    @Autowired
    private ProviderStatusDao providerStatusDao;
    
    @Override
    public void addProviderStatus(ProviderStatus providerStatus) {
        providerStatusDao.addProviderStatus(providerStatus);
    }

    @Override
    public void updateProviderStatus(ProviderStatus providerStatus) {
        providerStatusDao.updateProviderStatus(providerStatus);
    }

    @Override
    public List<ProviderStatus> listProviderStatus() {
        return providerStatusDao.listProviderStatus();
    }

    @Override
    public ProviderStatus getProviderStatusById(int id) {
        return providerStatusDao.getProviderStatusById(id);
    }

    @Override
    public void removeProviderStatus(int id) {
        providerStatusDao.removeProviderStatus(id);
    }

    @Override
    public List<ProviderStatus> getPageProviderStatus(int offset, int max) {
        return providerStatusDao.getPageProviderStatus(offset, max);
    }

    @Override
    public Long getNumberRows() {
        return providerStatusDao.getNumberRows();
    }
}
