/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.mx.telecomcrm.service;

import com.addcel.mx.telecomcrm.model.admin.Product;
import com.addcel.mx.telecomcrm.model.dao.ProductDao;
import com.addcel.mx.telecomcrm.model.dao.ProviderDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author marcopascale
 */
@Service
public class ProductServiceImpl implements ProductService{
    
   @Autowired
   private ProductDao productDao;
    
   @Override
   public List<Product> getListProducts() {
        return productDao.getListProducts();
    }
    
    
    
}
