package com.addcel.mx.telecomcrm.service;

import com.addcel.mx.telecomcrm.model.admin.Provider;
import com.addcel.mx.telecomcrm.model.dao.ProviderDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lasar-Soporte
 */
@Service
public class ProviderServiceImpl implements ProviderService{
    
    @Autowired
    private ProviderDao providerDao;

    @Override
    public void addProvider(Provider provider) {
        providerDao.addProvider(provider);
    }

    @Override
    public void updateProvider(Provider provider) {
        providerDao.updateProvider(provider);
    }

    @Override
    public List<Provider> listProviders() {
        return providerDao.listProviders();
    }

    @Override
    public List<Long> getProvidersId() {
        return providerDao.getProvidersId();
    }        

    @Override
    public Provider getProviderById(int id) {
        return providerDao.getProviderById(id);
    }

    @Override
    public void removeProvider(int id) {
        providerDao.removeProvider(id);
    }

    @Override
    public List<Provider> getPageProviders(int offset, int max) {
        return providerDao.getPageProviders(offset, max);
    }

    @Override
    public Long getNumberRows() {
        return providerDao.getNumberRows();
    }
    
}
