package com.addcel.mx.telecomcrm.service;

import com.addcel.mx.telecomcrm.model.admin.Franchise;
import com.addcel.mx.telecomcrm.model.dao.FranchiseDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lasar-Soporte
 */
@Service
public class FranchiseServiceImpl implements FranchiseService{
    
    @Autowired
    private FranchiseDao franchiseDao;

    @Override
    public void addFranchise(Franchise franchise) {
        franchiseDao.addFranchise(franchise);
    }

    @Override
    public void updateFranchise(Franchise franchise) {
        franchiseDao.updateFranchise(franchise);
    }

    @Override
    public List<Franchise> listFranchises() {
        return franchiseDao.listFranchises();
    }

    @Override
    public Franchise getFranchiseById(int id) {
        return franchiseDao.getFranchiseById(id);
    }

    @Override
    public void removeFranchise(int id) {
        franchiseDao.removeFranchise(id);
    }

    @Override
    public List<Franchise> getPageFranchises(int offset, int max) {
        return franchiseDao.getPageFranchises(offset, max);
    }

    @Override
    public Long getNumberRows() {
        return franchiseDao.getNumberRows();
    }
    
}
