package com.addcel.mx.telecomcrm.service;

import com.addcel.mx.telecomcrm.model.admin.Transaction;
import com.addcel.mx.telecomcrm.model.admin.TransactionSearch;
import com.addcel.mx.telecomcrm.model.admin.TransactionStandard;
import java.util.List;

/**
 *
 * @author wsolano
 */
public interface TransactionService {
    
    public void addTransaction(Transaction transaction);
    public void updateTransaction(Transaction transaction);
    public List<Transaction> listTransactions();
    public Transaction getTransactionById(int id);
    public void removeTransaction(int id);    
    public List<Transaction> getPageTransactions(int offset, int max);
    public Long getNumberRows ();
    public List<TransactionStandard> searchTransactions (TransactionSearch TSearch);
    
}
