package com.addcel.mx.telecomcrm.service;

import com.addcel.mx.telecomcrm.model.admin.Country;
import com.addcel.mx.telecomcrm.model.dao.CountryDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lasar-Soporte
 */
@Service
public class CountryServiceImpl implements CountryService {
    
    @Autowired
    private CountryDao countryDao;

    @Override
    public void addCountry(Country country) {
        countryDao.addCountry(country);
    }

    @Override
    public void updateCountry(Country country) {
        countryDao.updateCountry(country);
    }

    @Override
    public List<Country> listCountries() {
        return countryDao.listCountries();
    }

    @Override
    public Country getCountryById(int id) {
        return countryDao.getCountryById(id);
    }

    @Override
    public void removeCountry(int id) {
        countryDao.removeCountry(id);
    }

    @Override
    public List<Country> getPageCountries(int offset, int max) {
        return countryDao.getPageCountries(offset, max);
    }

    @Override
    public Long getNumberRows() {
        return countryDao.getNumberRows();
    }
    
}
