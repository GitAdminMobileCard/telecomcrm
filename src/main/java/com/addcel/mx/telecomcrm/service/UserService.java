package com.addcel.mx.telecomcrm.service;

import com.addcel.mx.telecomcrm.model.User;
import java.util.List;

/**
 *
 * @author Lasar-Soporte
 */
public interface UserService {

    public void addUser(User user);

    public void updateUser(User user);

    public List<User> listUsers();

    public User getUserById(int id);

    public void removeUser(int id);
    
    public User authenticateUser(String username, String password);

}
