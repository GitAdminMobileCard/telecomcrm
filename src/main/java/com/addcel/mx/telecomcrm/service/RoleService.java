package com.addcel.mx.telecomcrm.service;

import com.addcel.mx.telecomcrm.model.Role;
import java.util.List;

/**
 *
 * @author Lasar-Soporte
 */
public interface RoleService {
    
    public void addRole(Role role);

    public void updateRole(Role role);

    public List<Role> listRoles();

    public Role getRoleById(int id);

    public void removeRole(int id);
    
}
