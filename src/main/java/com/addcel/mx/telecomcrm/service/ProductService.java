package com.addcel.mx.telecomcrm.service;

import com.addcel.mx.telecomcrm.model.admin.Product;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author marcopascale
 */
public interface ProductService {

    public List<Product> getListProducts();
}
