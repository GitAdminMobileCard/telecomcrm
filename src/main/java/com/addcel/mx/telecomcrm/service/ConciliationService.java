package com.addcel.mx.telecomcrm.service;

import com.addcel.mx.telecomcrm.model.admin.AccountStatus;
import com.addcel.mx.telecomcrm.model.admin.AmexTransaction;
import com.addcel.mx.telecomcrm.model.admin.Register;
import java.util.List;

/**
 *
 * @author Lasar-Soporte
 */
public interface ConciliationService {
    
    public List<Register> findAddcelPayments(String fecha);

    public List<Register> findIdPayments(String fecha);

    public List<Register> findLegacyPayments(String fecha);

    public List<AccountStatus> getAccounStatus(String initDate, String endDate, Boolean forAmex);
    
    public List<AmexTransaction> getAmexTransactions(String date);
    
    public void updateAmexTransaction(AmexTransaction amexTransaction);
    
}
