package com.addcel.mx.telecomcrm.service;

import com.addcel.mx.telecomcrm.model.admin.Client;
import com.addcel.mx.telecomcrm.model.admin.ClientSearch;
import com.addcel.mx.telecomcrm.model.admin.ClientStatus;
import com.addcel.mx.telecomcrm.model.dao.ClientDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author wsolano
 */
@Service
public class ClientServiceImpl implements ClientService {
    
    @Autowired
    private ClientDao clientDao;

    public void setClientDao(ClientDao clientDao) {
        this.clientDao = clientDao;
    }        

    @Override
    public void addClient(Client client) {
        clientDao.addClient(client);
    }

    @Override
    public void updateClient(Client client) {
        clientDao.updateClient(client);
    }

    @Override
    public List<Client> listClients() {
        return clientDao.listClients();
    }

    @Override
    public Client getClientById(Long id) {
        return clientDao.getClientById(id);
    }

    @Override
    public void removeClient(int id) {
        clientDao.removeClient(id);
    }   

    @Override
    public List<Client> getPageClients(int offset, int max) {
        return clientDao.getPageClients(offset, max);
    }

    @Override
    public Long getNumberRows() {
        return clientDao.getNumberRows();
    }

	
    @Override
    public Client getClientByImei(String imei) {
    	return clientDao.getClientByImei(imei);
    }
    @Override
    public Client getClientByCard(String tarjeta) {
	return clientDao.getClientByCard(tarjeta);

    }

    @Override
    public List<Client> searchClients(ClientSearch clientSearch) {
        return clientDao.searchClients(clientSearch);
    }
    
    @Override
    public List<ClientStatus> getClientStatusList() {
        return clientDao.getClientStatusList();
    }
    
}
