package com.addcel.mx.telecomcrm.service;

import com.addcel.mx.telecomcrm.model.admin.AccountStatus;
import com.addcel.mx.telecomcrm.model.admin.AmexTransaction;
import com.addcel.mx.telecomcrm.model.admin.Register;
import com.addcel.mx.telecomcrm.model.dao.ConcilationDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lasar-Soporte
 */
@Service
public class ConciliationServiceImpl implements ConciliationService{

    @Autowired
    ConcilationDao concilationDao;
    
    @Override
    public List<Register> findAddcelPayments(String fecha) {
        return concilationDao.findAddcelPayments(fecha);
    }

    @Override
    public List<Register> findIdPayments(String fecha) {
        return concilationDao.findIdPayments(fecha);
    }

    @Override
    public List<Register> findLegacyPayments(String fecha) {
        return concilationDao.findLegacyPayments(fecha);
    }

    @Override
    public List<AccountStatus> getAccounStatus(String initDate, String endDate, Boolean forAmex) {
        return concilationDao.getAccounStatus(initDate, endDate, forAmex);
    }        

    @Override
    public List<AmexTransaction> getAmexTransactions(String date) {
        return concilationDao.getAmexTransactions(date);
    }        

    @Override
    public void updateAmexTransaction(AmexTransaction amexTransaction) {
        concilationDao.updateAmexTransaction(amexTransaction);
    }
    
    
}
