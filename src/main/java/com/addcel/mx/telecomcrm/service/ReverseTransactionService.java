package com.addcel.mx.telecomcrm.service;

import com.addcel.mx.telecomcrm.model.admin.ReverseTransaction;
import com.addcel.mx.telecomcrm.model.admin.ReverseTransactionSearch;
import java.util.List;

/**
 *
 * @author wsolano
 */
public interface ReverseTransactionService {
    
    public void addReverseTransaction(ReverseTransaction client);
    public void updateReverseTransaction(ReverseTransaction client);
    public List<ReverseTransaction> listReverseTransactions();
    public List<ReverseTransaction> getReverseTransactionByIdUsuario(int id_usuario);
    public void removeReverseTransaction(int id);    
    public List<ReverseTransaction> getPageReverseTransactions(int offset, int max);
    public Long getNumberRows ();
    
    public List<ReverseTransaction> searchReverseTransactions (ReverseTransactionSearch reverseTransactionSearch);
    
}
