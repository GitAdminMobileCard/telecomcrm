package com.addcel.mx.telecomcrm.service;

import com.addcel.mx.telecomcrm.model.admin.Bank;
import java.util.List;

/**
 *
 * @author Lasar-Soporte
 */
public interface BankService {
    
    public void addBank(Bank bank);
    public void updateBank(Bank bank);
    public List<Bank> listBanks();
    public Bank getBankById(int id);
    public void removeBank(int id);    
    public List<Bank> getPageBanks(int offset, int max);
    public Long getNumberRows ();
    
}
