package com.addcel.mx.telecomcrm.controller.admin;

import com.addcel.mx.telecomcrm.model.admin.TransactionSearch;
import com.addcel.mx.telecomcrm.model.admin.Product;
import com.addcel.mx.telecomcrm.model.admin.Transaction;
import com.addcel.mx.telecomcrm.model.admin.TransactionStandard;
import com.addcel.mx.telecomcrm.service.ProductService;
import com.addcel.mx.telecomcrm.service.TransactionService;
import com.addcel.mx.telecomcrm.utils.Crypto;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author wsolano
 */
@RestController
@RequestMapping(value="/admin/transaccion")
public class TransactionController {
    
    @Autowired
    private TransactionService transactionService;
    
    @Autowired
    private ProductService productService;
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public ModelAndView homeAdminViewTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/transaction");                         
        return mav;
    }
    
    @RequestMapping(value="/detail", method=RequestMethod.GET)
    public ModelAndView detailTransactionViewTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/templates/transactionTemplate");                         
        return mav;
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<Transaction>> listAllTransactions() throws Exception {                                
                
        List<Transaction> transactions = new ArrayList<>();                                             
        
        transactions = transactionService.listTransactions();                   
        if(transactions.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(transactions, HttpStatus.OK);
    }
    
//@RequestMapping(value = "/list_products", method = RequestMethod.GET)
//    public ResponseEntity<List<Product>> listAllProducts() throws Exception {                                
//                
//        List<Transaction> transactions = new ArrayList<>();                                             
//        
//        transactions = transactionService.listTransactions();                   
//        if(transactions.isEmpty()){
//            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
//        }
//        
//        return new ResponseEntity<>(transactions, HttpStatus.OK);
//    }
    
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResponseEntity<List<Transaction>> getTransactionsByPage(@RequestBody int [] data) throws Exception {                                
                
        List<Transaction> transactions = new ArrayList<>();                                             
        
        System.out.println("page " + data[0]);
        System.out.println("max " + data[1]);
        
        transactions = transactionService.getPageTransactions(data[0], data[1]);                   
        if(transactions.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(transactions, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/searchTransactions", method = RequestMethod.POST)
    public ResponseEntity<List<TransactionStandard>> getTransactions(@RequestBody TransactionSearch TSearch) throws Exception {                                
                
        List<TransactionStandard> transactions = new ArrayList<>();                                             
        
        transactions = transactionService.searchTransactions(TSearch);                   
        if(transactions.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        for (TransactionStandard transactionStandard : transactions ){
            if (transactionStandard.getTarjeta_compra() != null){
                if (!Crypto.isNumeric(transactionStandard.getTarjeta_compra())){
                    String numcard = Crypto.getCardNumber(transactionStandard.getTarjeta_compra());
                    transactionStandard.setTarjeta_compra(Crypto.escondeDigitos(numcard));
                }else{
                    transactionStandard.setTarjeta_compra(Crypto.escondeDigitos(transactionStandard.getTarjeta_compra()));
                }
            }            
        }
        
        return new ResponseEntity<>(transactions, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/total", method = RequestMethod.POST)
    public ResponseEntity<?> getNumberOfTransactions() throws Exception {                                                                                                            
        
        Long totalTransactions = transactionService.getNumberRows();
        return new ResponseEntity<>(totalTransactions, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/productList", method = RequestMethod.GET)
    public ResponseEntity<List<Product>> listAllProducts() throws Exception {                                
                
        List<Product> products = new ArrayList<>();                                             
        
        products = productService.getListProducts();
        if(products.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(products, HttpStatus.OK);
    }
    
}
