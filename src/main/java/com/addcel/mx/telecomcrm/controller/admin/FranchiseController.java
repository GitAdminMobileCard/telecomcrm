package com.addcel.mx.telecomcrm.controller.admin;

import com.addcel.mx.telecomcrm.model.admin.Franchise;
import com.addcel.mx.telecomcrm.service.FranchiseService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Lasar-Soporte
 */
@RestController
@RequestMapping(value="/admin/franquicia")
public class FranchiseController {
    
    @Autowired
    private FranchiseService franchiseService;        
    
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<Franchise>> listAllFranchises() throws Exception {                                
                
        List<Franchise> franchises = new ArrayList<>();                                             
        
        franchises = franchiseService.listFranchises();                   
        if(franchises.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(franchises, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResponseEntity<List<Franchise>> getFranchisesByPage(@RequestBody int [] data) throws Exception {                                
                
        List<Franchise> franchises = new ArrayList<>();                                             
        
        System.out.println("page " + data[0]);
        System.out.println("max " + data[1]);
        
        franchises = franchiseService.getPageFranchises(data[0], data[1]);                   
        if(franchises.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(franchises, HttpStatus.OK);
    }       
    
    @RequestMapping(value = "/total", method = RequestMethod.POST)
    public ResponseEntity<?> getNumberOfFranchises() throws Exception {                                                                                                            
        
        Long totalFranchises = franchiseService.getNumberRows();
        return new ResponseEntity<>(totalFranchises, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ResponseEntity<Franchise> editFranchise(@RequestBody Franchise franchise) throws Exception {                                
        
        franchiseService.updateFranchise(franchise);                               
        
        return new ResponseEntity<>(franchise, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise) throws Exception {                                
        
        franchiseService.addFranchise(franchise);                               
                
        return new ResponseEntity<>(franchise, HttpStatus.OK);
    }
    
}
