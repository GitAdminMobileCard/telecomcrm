package com.addcel.mx.telecomcrm.controller.admin;

import com.addcel.mx.telecomcrm.model.admin.UserTag;
import com.addcel.mx.telecomcrm.model.admin.UserTagSearch;
import com.addcel.mx.telecomcrm.service.UserTagServiceImpl;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Lasar-Soporte
 */
@RestController
@RequestMapping(value="/admin/tag")
public class UserTagController {
    
    @Autowired
    private UserTagServiceImpl userTagService;
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public ModelAndView homeAdminViewTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/userTag");                         
        return mav;
    }
    
    @RequestMapping(value="/detail", method=RequestMethod.GET)
    public ModelAndView tagDetailViewTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/templates/userTagTemplate");                         
        return mav;
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<UserTag>> listAllUserTags() throws Exception {                                
                
        List<UserTag> userTags = new ArrayList<>();                                             
        
        userTags = userTagService.listUserTags();                   
        if(userTags.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(userTags, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResponseEntity<List<UserTag>> getUserTagsByPage(@RequestBody int [] data) throws Exception {                                
                
        List<UserTag> userTags = new ArrayList<>();                                             
        
        System.out.println("page " + data[0]);
        System.out.println("max " + data[1]);
        
        userTags = userTagService.getPageUserTags(data[0], data[1]);                   
        if(userTags.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(userTags, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/total", method = RequestMethod.POST)
    public ResponseEntity<?> getNumberOfUserTags() throws Exception {                                                                                                            
        
        Long totalUserTags = userTagService.getNumberRows();
        return new ResponseEntity<>(totalUserTags, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/searchTags", method = RequestMethod.POST)
    public ResponseEntity<List<UserTag>> searchTags(@RequestBody UserTagSearch userTagSearch) throws Exception {                                
                
        List<UserTag> userTags = new ArrayList<>();                                             
        
        userTags = userTagService.searchTags(userTagSearch);                   
        if(userTags.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(userTags, HttpStatus.OK);
    }
    
}
