package com.addcel.mx.telecomcrm.controller.admin;

import com.addcel.mx.telecomcrm.model.admin.TypeCard;
import com.addcel.mx.telecomcrm.service.TypeCardService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Lasar-Soporte
 */
@RestController
@RequestMapping(value="/admin/tipoTarjeta")
public class TypeCardController {
    
    @Autowired
    private TypeCardService typeCardService;        
    
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<TypeCard>> listAllTypeCards() throws Exception {                                
                
        List<TypeCard> typeCards = new ArrayList<>();                                             
        
        typeCards = typeCardService.listTypeCards();                   
        if(typeCards.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(typeCards, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResponseEntity<List<TypeCard>> getTypeCardsByPage(@RequestBody int [] data) throws Exception {                                
                
        List<TypeCard> typeCards = new ArrayList<>();                                             
        
        System.out.println("page " + data[0]);
        System.out.println("max " + data[1]);
        
        typeCards = typeCardService.getPageTypeCards(data[0], data[1]);                   
        if(typeCards.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(typeCards, HttpStatus.OK);
    }       
    
    @RequestMapping(value = "/total", method = RequestMethod.POST)
    public ResponseEntity<?> getNumberOfTypeCards() throws Exception {                                                                                                            
        
        Long totalTypeCards = typeCardService.getNumberRows();
        return new ResponseEntity<>(totalTypeCards, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ResponseEntity<TypeCard> editTypeCard(@RequestBody TypeCard typeCard) throws Exception {                                
        
        typeCardService.updateTypeCard(typeCard);                               
        
        return new ResponseEntity<>(typeCard, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<TypeCard> addTypeCard(@RequestBody TypeCard typeCard) throws Exception {                                
        
        typeCardService.addTypeCard(typeCard);                               
                
        return new ResponseEntity<>(typeCard, HttpStatus.OK);
    }
    
}
