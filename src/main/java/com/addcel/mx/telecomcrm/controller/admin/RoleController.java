package com.addcel.mx.telecomcrm.controller.admin;

import com.addcel.mx.telecomcrm.model.Role;
import com.addcel.mx.telecomcrm.service.RoleService;
import com.addcel.mx.telecomcrm.utils.PasswordEncrypter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Lasar-Soporte
 */
@RestController
@RequestMapping(value="/admin/role")
public class RoleController {
    @Autowired
    private RoleService roleService;
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public ModelAndView homeAdminViewTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/role");                         
        return mav;
    }
    
    @RequestMapping(value="/detail", method=RequestMethod.GET)
    public ModelAndView detailRoleViewTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {                                 
        ModelAndView mav = new ModelAndView("admin/templates/roleTemplate");                         
        return mav;
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<Role>> listAllRoles() throws Exception {                                
                
        List<Role> roles = new ArrayList<>();                                             
        
        roles = roleService.listRoles();                   
        if(roles.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<>(roles, HttpStatus.OK);
    }           
    
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ResponseEntity<Role> editRole(@RequestBody Role role) throws Exception {                                
                            
        PasswordEncrypter passwordEncrypter = new PasswordEncrypter();                                
        
        roleService.updateRole(role);                               
        
        Role roleToResponse = roleService.getRoleById(role.getId_role());                
        
        return new ResponseEntity<>(roleToResponse, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<Role> addRole(@RequestBody Role role) throws Exception {                                                
        
        PasswordEncrypter passwordEncrypter = new PasswordEncrypter();                
                     
        roleService.addRole(role);                               
                
        Role roleToResponse = roleService.getRoleById(role.getId_role());
        
        return new ResponseEntity<>(roleToResponse, HttpStatus.OK);
    }              
    
}
