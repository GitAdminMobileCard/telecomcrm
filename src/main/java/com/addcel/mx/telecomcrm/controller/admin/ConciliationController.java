package com.addcel.mx.telecomcrm.controller.admin;

import com.addcel.mx.telecomcrm.model.admin.AccountStatus;
import com.addcel.mx.telecomcrm.model.admin.AmexTransaction;
import com.addcel.mx.telecomcrm.model.admin.Register;
import com.addcel.mx.telecomcrm.service.ConciliationServiceImpl;
import com.addcel.mx.telecomcrm.utils.Crypto;
import com.google.common.base.Strings;
import com.google.common.primitives.Longs;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpHeaders.USER_AGENT;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Lasar-Soporte
 */
@RestController
@RequestMapping(value = "/admin/conciliacion")
public class ConciliationController {

    @Autowired
    private ConciliationServiceImpl conciliationService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView homeAdminViewTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView mav = new ModelAndView("admin/conciliation");
        return mav;
    }

    @RequestMapping(value = "/accountStatus", method = RequestMethod.POST)
    public ResponseEntity<List<AccountStatus>> getAccountStatus(@RequestBody String dates[]) throws Exception {

        List<AccountStatus> accountStatuses = new ArrayList<>();

        accountStatuses = conciliationService.getAccounStatus(dates[0], dates[1], false);
        if (accountStatuses.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<>(accountStatuses, HttpStatus.OK);
    }

    @RequestMapping(value = "/amexConciliation", method = RequestMethod.POST)
    @ResponseBody
    public String getAmexTransactions(@RequestBody String date) throws Exception {

        List<AmexTransaction> amexTransactions = new ArrayList<>();
        List<AmexTransaction> amexTransactionsResponse = new ArrayList<>();
        date = date.replace("\"", "");

        long maxDiff = MILLISECONDS.convert(10, SECONDS);

        Date dateAux = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(date);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String initDate = format.format(dateAux);

        System.out.println("Date amex " + initDate);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateAux);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        String endDate = format.format(calendar.getTime());
        
        List<AccountStatus> accountStatuses = new ArrayList<>();

        accountStatuses = conciliationService.getAccounStatus(initDate, endDate, true);

        amexTransactions = conciliationService.getAmexTransactions(initDate);

        for (AccountStatus accountStatus : accountStatuses) {
            for (AmexTransaction amexTransaction : amexTransactions) {
                if (accountStatus.getTarjeta_compra().equals(amexTransaction.getTransaccionTarjeta())) {

                    Date dateAmex = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(amexTransaction.getTransaccionDate());
                    Date dateAccountStatus = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                            .parse(accountStatus.getBit_fecha() + " " + accountStatus.getBit_hora());

                    long datesDiff = dateAmex.getTime() - dateAccountStatus.getTime();
                    if (datesDiff > 0 && datesDiff < maxDiff) {
                        /*System.out.println("Tarjeta de compra " + accountStatus.getTarjeta_compra());
                        System.out.println("Fecha Amex " + amexTransaction.getTransaccionDate());
                        System.out.println("Fecha AS " + accountStatus.getBit_fecha() + " " + accountStatus.getBit_hora());
                        System.out.println("Numero aut. as " + accountStatus.getAutorizacion());
                        System.out.println("Numero aut amex " + amexTransaction.getTransaccionIdn());
                        System.out.println("Status " + accountStatus.getBit_status());
                        System.out.println("Error code " + accountStatus.getBit_codigo_error());
                        System.out.println("Reversal " + amexTransaction.getTransaccionReversal());*/
                        if (accountStatus.getBit_status() == -1 || accountStatus.getBit_status() == 2
                                || accountStatus.getBit_status() == 3 || accountStatus.getBit_codigo_error() != 0) {
                            amexTransaction.setTransaccionReversal("R");
                            conciliationService.updateAmexTransaction(amexTransaction);
                            amexTransactionsResponse.add(amexTransaction);
                        } else {
                            amexTransactionsResponse.add(amexTransaction);
                        }
                    }
                }
            }
        }        
        String[] parts = initDate.split("-");
        /*System.out.println("Year " + parts[0]);
        System.out.println("Month " + parts[1]);
        System.out.println("Day " + parts[2]);*/
        
        
        /*if (amexTransactions.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }*/
        String response = execAmexConciliation(parts[0], parts[1], parts[2]);
        
        //return new ResponseEntity<>(response, HttpStatus.OK);
        return response;
    }

    @RequestMapping(value = "/createFile", method = {RequestMethod.POST, RequestMethod.GET})
    public void createArchivo(@RequestBody String fileData[],
            HttpServletRequest request, HttpServletResponse response) {

        List<Register> registers;
        StringBuffer string = null;
        OutputStream outStream = null;
        String fileName = "";
        String date = fileData[0];
        String fileType = fileData[1];

        switch (fileType) {
            case "ADDCEL":

                registers = conciliationService.findAddcelPayments(date);

                string = buildFileString(registers, date);

                fileName = "ADDCEL_" + date + ".txt";

                try {
                    // set content attributes for the response
                    response.setContentType("text/plain");
                    //	        response.setContentLength(cadena.length());

                    // set headers for the response
                    response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", fileName));

                    // get output stream of the response
                    outStream = response.getOutputStream();

                    // write bytes read from the input stream into the output stream
                    outStream.write(string.toString().getBytes());

                    System.out.println("Archivo tutag construido exitosamente en buildTuTagFile");

                } catch (IOException e) {
                    System.out.println("Ocurrio un error general guardar el archivo: {} " + e.getMessage());
                }

                break;
            case "TUTAG":
                registers = conciliationService.findIdPayments(date);

                string = buildFileString(registers, date);

                fileName = "TUTAG_" + date + ".txt";

                try {
                    // set content attributes for the response
                    response.setContentType("text/plain");
                    //	        response.setContentLength(cadena.length());

                    // set headers for the response
                    response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", fileName));

                    // get output stream of the response
                    outStream = response.getOutputStream();

                    // write bytes read from the input stream into the output stream
                    outStream.write(string.toString().getBytes());

                    System.out.println("Archivo tutag construido exitosamente en buildTuTagFile");

                } catch (IOException e) {
                    System.out.println("Ocurrio un error general guardar el archivo: {} " + e.getMessage());
                }

                break;
            case "TUTAG_CORRECCION":
                registers = conciliationService.findLegacyPayments(date);

                string = buildFileString(registers, date);

                fileName = "TUTAG_" + date + ".txt";

                try {
                    // set content attributes for the response
                    response.setContentType("text/plain");
                    //	        response.setContentLength(cadena.length());

                    // set headers for the response
                    response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", fileName));

                    // get output stream of the response
                    outStream = response.getOutputStream();

                    // write bytes read from the input stream into the output stream
                    outStream.write(string.toString().getBytes());

                    System.out.println("Archivo tutag construido exitosamente en buildTuTagFile");

                } catch (IOException e) {
                    System.out.println("Ocurrio un error general guardar el archivo: {} " + e.getMessage());
                }

                break;
            default:
                break;
        }
    }

    private StringBuffer buildFileString(List<Register> registros, String fecha) {

        StringBuffer cadena = new StringBuffer();

        String[] tmp = fecha.split("-");
        System.out.println(fecha);
        cadena.append("HDR").append(tmp[0]).append(tmp[1]).append(tmp[2].substring(0, 2)).append("\n");
        int consecu = 0;
        double total = 0;

        String[] fec = null;
        String hor = null;
        String tipo = null;
        String tag = null;
        String cargo = null;
        String Cargo = null;
        String[] Aut = null;

        try {

            for (Register registro : registros) {
                consecu += 1;

                fec = registro.getBit_fecha().split("-");
                hor = registro.getBit_hora().replace(":", "").trim();
                tag = registro.getDestino();
                cargo = String.valueOf(registro.getBit_cargo());
                Cargo = Ceros(cargo.replace(".", "0"), 12);
                Aut = registro.getNum_TarjetaIAVE_autorizacion().split("-");

                total = total + registro.getBit_cargo();

                if (registro.getDestino().length() == 11) {
                    tipo = "";
                    tag = tag + " " + " " + " ";
                } else {

                    tag = tag + " " + " ";
                    tag = Strings.padStart(tag, 10, '0');

                    long destinoAsLong = Longs.tryParse(registro.getDestino());

                    //int primerCharEnTagAsInt = Ints.tryParse(registro.getDestino().substring(0,1));
                    //if (primerCharEnTagAsInt <= 1)
                    if (destinoAsLong < 20000000) {
                        tipo = "CPFI";
                    } else {
                        tipo = "IMDM";
                    }

                }

                cadena.append("REG").append(Ceros(String.valueOf(consecu), 6))
                        .append(fec[2].trim()).append(fec[1].trim()).append(fec[0].trim()).append(hor)
                        .append(tipo).append(tag).append(Cargo).append("0000")
                        .append(Ceros(Aut[0], 5)).append(Ceros(Aut[1], 6)).append("\n");
            }

            cadena.append("TRL").append(Ceros(String.valueOf(consecu), 6)).append(Ceros(String.valueOf(total).replace(".", "0"), 12)).append("\n");
        } catch (Exception e) {
            cadena = new StringBuffer("");
        }

        return cadena;
    }

    private String Ceros(String cad, int NumCeros) {
        String tmp = "00000000000000000000000000000";
        return (tmp + cad).substring((tmp + cad).length() - NumCeros, (tmp + cad).length());
    }

    
    private String execAmexConciliation(String year, String month, String day) throws Exception {
        String url = "https://www.mobilecard.mx:8443/AmexSubFile/TestAmex?anio=" + year + "&mes=" + month + "&dia=" + day;        
        Document doc = Jsoup.connect(url).get();
        Elements table = doc.getElementsByTag("table");
        System.out.println("Table " + table.toString()); 
        table.removeAttr("align");
        table.removeAttr("cellpadding");
        table.attr("datatable", "");
        table.attr("dt-options", "ctrl.dtAmexConciliationSummaryOptions");
        table.attr("dt-column-defs", "ctrl.dtAmexConciliationSummaryColumnDefs");
        table.attr("dt-instance", "ctrl.dtAmexConciliationSummaryInstance");
        table.first().children().first().before("<thead>\n" +
"                                    <tr class=\"table-header w3-padding-0\">            \n" +
"                                        <th class=\"w3-text-white w3-padding-2 w3-center\">Comercio</th>\n" +
"                                        <th class=\"w3-text-white w3-padding-2 w3-center\">Afiliación</th>\n" +
"                                        <th class=\"w3-text-white w3-padding-2 w3-center\">Num. Registros</th>\n" +"                                                                                               \n" +
"                                    </tr>\n" +
"                                </thead>");
        table.select("tbody").first().children().first().remove();
        Elements elements = table.select("td");
        for (Element element : elements) {
            element.addClass("w3-bordered");
            element.addClass("w3-border");
        }
        doc.select("h3").remove();
        System.out.println("Table with datatable " + table.toString()); 
        return doc.getElementsByClass("main-content").toString();                        
    }
}
